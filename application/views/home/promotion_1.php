<section class="container-fluid">

<div class="row justify-content-center">
<div class="col-xl-12 col-lg-12">
<div class="fw-section rounded padding-top-4x padding-bottom-4x" style="background-image: url(<?=base_url('library/img/Slider/1.png')?>);">
<span class="overlay rounded" style="opacity: .35;"></span>

<div class="text-center">
<h3 class="display-4 text-normal text-white text-shadow mb-1">Promo 1</h3>
<h2 class="display-2 text-bold text-white text-shadow">...</h2>
<h4 class="d-inline-block h2 text-normal text-white text-shadow border-default border-left-0 border-right-0 mb-4">Texto</h4>
<br><a class="btn btn-primary margin-bottom-none" href="contacts.html">Visitar</a>
</div>

</div>
</div>
</div>

</section>