<div class="modal fade" id="modal_2"  tabindex="-1" role="dialog" >
<div class="modal-dialog modal-xl modal-dialog-scrollable " role="document">
<div class="modal-content">

<!-- Title-->
<div class="modal-header">
<h4 class="modal-title">Conferencias</h4>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<!-- Title-->

<div class="modal-body">

<div class="accordion" id="accordion1" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#collapseTwo" data-toggle="collapse" style="font-size: 18px">Objetivo</a></h6>
</div>

<div class="collapse" id="collapseTwo" data-parent="#accordion1" role="tabpanel">
<div class="card-body">Con nuestras conferencias tratamos de impartir conocimiento sobre distintas áreas; forense, jurídica, psicológica, pedagógicas y terapias alternativas. Para así crear o aumentar su desarrollo personal y que puedan capacitarse aún mejor en temas de su interés.</div>
</div>
</div>
</div>



<!-- Cat 1-->
<?=$this->load->view('Conferencias/Cat_1','',TRUE);?>
<!-- Cat 1-->

<!-- Cat 2-->
<?=$this->load->view('Conferencias/Cat_2','',TRUE);?>
<!-- Cat 2-->

<!-- Cat 3-->
<?=$this->load->view('Conferencias/Cat_3','',TRUE);?>
<!-- Cat 3-->

<!-- Cat 4-->
<?=$this->load->view('Conferencias/Cat_4','',TRUE);?>
<!-- Cat 4-->

<!-- Cat 5-->
<?=$this->load->view('Conferencias/Cat_5','',TRUE);?>
<!-- Cat 5-->



</div>

<div class="modal-footer">
<button class="btn btn-outline-primary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
</div>

</div>
</div>
</div>