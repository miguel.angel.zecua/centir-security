<div class="modal fade" id="modal_3"  tabindex="-1" role="dialog" >
<div class="modal-dialog modal-xl modal-dialog-scrollable " role="document">
<div class="modal-content">

<!-- Title-->
<div class="modal-header">
<h4 class="modal-title">Cursos</h4>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<!-- Title-->

<div class="modal-body">

<!-- Categoria 1-->
<?=$this->load->view('Cursos/Cat1','',TRUE);?>
<!-- Categoria 1-->

<!-- Categoria 2-->
<?=$this->load->view('Cursos/Cat2','',TRUE);?>
<!-- Categoria 2-->

<!-- Categoria 3-->
<?=$this->load->view('Cursos/Cat3','',TRUE);?>
<!-- Categoria 3-->

<!-- Categoria 4-->
<?=$this->load->view('Cursos/Cat4','',TRUE);?>
<!-- Categoria 4-->

<!-- Categoria 5-->
<?=$this->load->view('Cursos/Cat5','',TRUE);?>
<!-- Categoria 5-->
  


</div>

<div class="modal-footer">
<button class="btn btn-outline-primary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
</div>

</div>
</div>
</div>