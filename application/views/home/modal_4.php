<div class="modal fade" id="modal_4"  tabindex="-1" role="dialog" >
<div class="modal-dialog modal-xl modal-dialog-scrollable " role="document">
<div class="modal-content">

<!-- Title-->
<div class="modal-header">
<h4 class="modal-title">Diplomados</h4>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<!-- Title-->

<div class="modal-body">

<div class="accordion" id="accordion2" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#collapseTree" data-toggle="collapse" style="font-size: 16px">Objetivo</a></h6>
</div>

<div class="collapse" id="collapseTree" data-parent="#accordion2" role="tabpanel">
<div class="card-body">Generalmente tienen una duración de entre 80 y 120 horas. Este tipo de información gira en torno a temas muy específicos, que sirven para complementar o ampliar una determinada área del conocimiento. Al culminarlo exitosamente se entrega un diploma <b>impartido por Centi-r, el cual es avalado por la STPS y con opción a certificación SEP-CONOCER.</b> </div>
</div>
</div>
</div>

<div class="row justify-content-center">

<!-- Product 1-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Diplomados/Acupuntura')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Acupuntura-min.png')?>" alt="Diplomado Acupuntura Centi-r"></a>
<h3 class="product-title"><b>Acupuntura</b></h3>
<h4 class="product-price">$1500.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/Acupuntura'">Añadir</button>
</div>

</div>
</div>
</div>
<!-- Product 1-->

</div>



</div>

<div class="modal-footer">
<button class="btn btn-outline-primary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
</div>

</div>
</div>
</div>