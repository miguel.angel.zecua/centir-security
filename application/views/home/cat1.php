
<div class="row justify-content-center d-none" style="margin-left:-3.5rem; margin-right:-3.5rem; background-color:#D4E6F1; border-radius:30px;">

<div id="columnas" class="col-6">

<!-- Related Products Carousel-->


<h3 class="text-center  mt-4 padding-bottom-1x text-center text-dark">Conferencias gratuitas</h3>

<!-- Carousel-->
<div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: true, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;autoplay&quot;: true,  &quot;loop&quot;: true , &quot;autoplayTimeout&quot;: 4000, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }" id="owl-dem">

<!-- CLASE 1-->


<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Perfil_criminal')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Perfil-criminal-min.png')?>" alt="Product"></a>

<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Perfil_criminal'">Unirme</button>
</div>

</div>
</div>
<!-- Product 2-->


<!-- Product 4-->
<div class="grid-item">
<div class="product-card">
<a class="product-thumb" href="<?=base_url('Conferencias/Funcion_criminalista')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Funcion-criminalista-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Funcion_criminalista'">Unirme</button>
</div>

</div>
</div>
<!-- Product 4-->

<!-- Product 5-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Investigacion_forense')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Investigacion-forense-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Investigacion_forense'">Unirme</button>
</div>

</div>
</div>
<!-- Product 5-->


<!-- Product 7-->
<div class="grid-item">
<div class="product-card">    
<a class="product-thumb" href="<?=base_url('Conferencias/Investigacion_criminal')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Investigacion-criminal-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Investigacion_criminal'">Unirme</button>
</div>

</div>
</div>
<!-- Product 7-->

<!-- Product 8-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Trabajo_interdisciplinario')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Trabajo-interdisciplinario-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Trabajo_interdisciplinario'">Unirme</button>
</div>

</div>
</div>
<!-- Product 8-->

<!-- Product 9-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Delincuentes_infancia')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Delincuentes-infancia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
  
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Delincuentes_infancia'">Unirme</button>
</div>

</div>
</div>
<!-- Product 9-->

<!-- Product 10-->
<div class="grid-item">
<div class="product-card"> 
<a class="product-thumb" href="<?=base_url('Conferencias/Apoyo_balistica')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Apoyo-balistica-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Apoyo_balistica'">Unirme</button>
</div>

</div>
</div>
<!-- Product 10-->
<!-- CLASE 1-->

<!-- CLASE 2-->
<!-- Product 1 -->
<div class="grid-item">
<div class="product-card"> 
<a class="product-thumb" href="<?=base_url('Conferencias/Abogado_Perito')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Abogado-Perito-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Abogado_Perito'">Unirme</button>
</div>

</div>
</div>
<!-- Product 1-->

<!-- CLASE 2-->

<!-- CLASE 3-->

<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Defensa_dolor')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Defensa-dolor-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
  
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Defensa_dolor'">Unirme</button>
</div>

</div>
</div>
<!-- Product 3 -->

<!-- Product 4-->
<div class="grid-item">
<div class="product-card">     
<a class="product-thumb" href="<?=base_url('Conferencias/Emociones_vida')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Emociones-vida-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Emociones_vida'">Unirme</button>
</div>

</div>
</div>
<!-- Product 4-->


<!-- Product 8-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Liderazgo_aplicado')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Liderazgo-aplicado-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
  
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Liderazgo_aplicado'">Unirme</button>
</div>

</div>
</div>
<!-- Product 8-->
<!-- CLASE 3-->

<!-- CLASE 4-->
<!-- Product 1 -->
<div class="grid-item">
<div class="product-card">   
<a class="product-thumb" href="<?=base_url('Conferencias/Errores_lectoescritura')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Pedagogia/Errores-lectoescritura-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Errores_lectoescritura'">Unirme</button>
</div>

</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Retos_educacion')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Pedagogia/Retos-educacion-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Retos_educacion'">Unirme</button>
</div>

</div>
</div>
<!-- Product 2-->

<!-- CLASE 4-->

<!-- CLASE 5-->
<!-- Product 1 -->
<div class="grid-item">
<div class="product-card">   
<a class="product-thumb" href="<?=base_url('Conferencias/Influencia_emociones')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Influencia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Terapia_Imanes'">Unirme</button>
</div>

</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Terapia_dolor')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Terapia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Medicina_Cannabica'">Unirme</button>
</div>

</div>
</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Alimentos_funcionales_enfermedades')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Alimentos-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
  
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Auriculoterapia'">Unirme</button>
</div>

</div>
</div>


<!-- Product 3 -->
<!-- CLASE 5-->


</div>
<div class="text-center mt-3 ">
<a class="btn btn-secondary margin-top-none" href="<?=base_url('Conferencias')?>">TODOS</a>
</div>
</div>
</div>
<br>


