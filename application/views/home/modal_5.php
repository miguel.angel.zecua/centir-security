<div class="modal fade" id="modal_5"  tabindex="-1" role="dialog" >
<div class="modal-dialog modal-xl modal-dialog-scrollable " role="document">
<div class="modal-content">

<!-- Title-->
<div class="modal-header">
<h4 class="modal-title">Terapias alternativas</h4>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<!-- Title-->

<div class="modal-body">


<div class="row justify-content-center mt-2">

<!-- Product 1-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Acupuntura')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Acupuntura-min.png')?>" alt="Terapia Alternativa Acupuntura Centi-r"></a>
<h3 class="product-title"><b>Acupuntura</b></h3>
<h4 class="product-price">$250.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Acupuntura'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Ventosas')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Ventosas-min.png')?>" alt="Terapias Alternativas Ventosas Centi-r"></a>
<h3 class="product-title"><b>Ventosas</b></h3>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Ventosas'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Auriculoterapia')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Auriculoterapia-min.png')?>" alt="Terapias Alternativas Auriculoterapia Centi-r"></a>
<h3 class="product-title"><b>Auriculoterapia</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Auriculoterapia'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 3-->

</div>

<div class="row justify-content-center mt-2">

<!-- Product 4-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Conoterapia')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Conoterapia-min.png')?>" alt="Terapias Alternativas Conoterapia Centi-r"></a>
<h3 class="product-title"><b>Conoterapia</b></h3>
<h4 class="product-price">$250.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Conoterapia'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 4-->

<!-- Product 5-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Terapia_imanes')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Imanes-min.png')?>" alt="Terapias Alternativas Imanes Centi-r"></a>
<h3 class="product-title"><b>Terapia de imanes</b></h3>
<h4 class="product-price">$250.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Terapia_imanes'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 5-->

<!-- Product 6-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Flores_mexicanas')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Flores-min.png')?>" alt="Terapias Alternativas Flores Mexicanas Centi-r"></a>
<h3 class="product-title"><b>Flores mexicanas</b></h3>
<h4 class="product-price">$150.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Flores_mexicanas'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 6-->

</div>

<div class="row justify-content-center mt-2">

<!-- Product 7-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Terapia_neural')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Neural-min.png')?>" alt="Terapias Alternativas Neural Centi-r"></a>
<h3 class="product-title"><b>Terapia neural</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Terapia_neural'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 7-->

<!-- Product 8-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Alineacion_chakras')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Chakras-min.png')?>" alt="Terapias Alternativas Chakras Centi-r"></a>
<h3 class="product-title"><b>Alineación de chakras</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Alineacion_chakras'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 8-->

<!-- Product 9-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Masaje_terapeutico')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Terapeutico-min.png')?>" alt="Terapias Alternativas Terapeutico Centi-r"></a>
<h3 class="product-title"><b>Masaje terapéutico</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Masaje_terapeutico'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 9-->

</div>

<div class="row justify-content-center mt-2">

<!-- Product 10-->
<div class="col-4 mt-2" id="fila1">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Masaje_reductivo')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Reductivo-min.png')?>" alt="Terapias Alternativas Masaje Terapeutico Centi-r"></a>
<h3 class="product-title"><b>Masaje reductivo</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapias_alternativas/Masaje_reductivo'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 10-->
	
</div>



</div>

<div class="modal-footer">
<button class="btn btn-outline-primary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
</div>

</div>
</div>
</div>