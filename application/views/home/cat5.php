
<div class="row justify-content-center" style="margin-left:-3.5rem; margin-right:-3.5rem; background-color:#F2D7D5; border-radius:30px; margin-top: -21px !important;">

<div id="columnas" class="col-6">

<!-- Related Products Carousel-->


<h3 class="text-center  mt-4 padding-bottom-1x text-center text-dark">Diplomados</h3>

<!-- Carousel-->
<div class=" row justify-content-center" >

<!-- CLASE 1-->
<!-- Product 1 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/Acupuntura')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Acupuntura-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Acupuntura</b></h3>
<h4 class="product-price invisible">$1500.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/Acupuntura'">Añadir</button>
</div>

</div>
</div>
<!-- Product 1-->


</div>
<!-- CLASE 1-->

<!-- CLASE 2-->
<!-- Product 2 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/S_penal_acusatorio')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/S_Acusatorio_penal.png')?>" alt="Product"></a>
<h3 class="product-title" style="margin-bottom: -10px;"><b>Sistema penal acusatorio y sus etapas procesales</b></h3>
<h4 class="product-price invisible">$000.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/S_penal_acusatorio'">Añadir</button>
</div>

</div>
</div>
<!-- Product 2-->
</div>
<!-- CLASE 2-->


<!-- CLASE 3-->
<!-- Product 3 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/Biomagnetismo')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Biomagnetismo.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Biomagnetismo</b></h3>
<h4 class="product-price invisible">$1300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/Biomagnetismo'">Añadir</button>
</div>

</div>
</div>
<!-- Product 3-->
</div>
<!-- CLASE 3-->




</div>
<div class="text-center mt-4 ">
<a class="btn btn-secondary margin-top-none" href="<?=base_url('Diplomados')?>">TODOS</a>
</div>
</div>
</div>
<br>