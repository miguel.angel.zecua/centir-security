<div class="modal fade" id="modal_1"  tabindex="-1" role="dialog" >
<div class="modal-dialog modal-xl modal-dialog-scrollable " role="document">
<div class="modal-content">

<!-- Title-->
<div class="modal-header text-center">
<h4 class="modal-title text-center">Consultas</h4>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<!-- Title-->

<div class="modal-body">

<div class="row justify-content-center mt-2 mb-2">
  
<!-- Product 1-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Individual')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Individual-min.png')?>" alt="Consulta Individual Centi-r"></a>
<h3 class="product-title"><b>Individual</b></h3>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Individual'">Agendar
</button>


</div>

</div>
</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Familiar')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Familiar-min.png')?>" alt="Consulta Familiar Centi-r"></a>
<h3 class="product-title"><b>Familiar</b></h3>
<h4 class="product-price">$350.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Familiar'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Pareja')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Pareja-min.png')?>" alt="Consulta Pareja Centi-r"></a>
<h3 class="product-title"><b>De pareja</b></h3>
<h4 class="product-price">$350.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Pareja'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 3-->

</div>


<div class="row justify-content-center mt-2">

<!-- Product 4-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Infantil')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Infantil-min.png')?>" alt="Consulta Infantil Centi-r"></a>
<h3 class="product-title"><b>Infantil</b></h3>
<h4 class="product-price">$250.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Infantil'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 4-->

<!-- Product 5-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Adicciones')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Adicciones-min.png')?>" alt="Consultas Adicciones Centi-r"></a>
<h3 class="product-title"><b>Adicciones</b></h3>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm"onclick="location.href='Consultas/Adicciones'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 5-->

<!-- Product 6-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Tanatologia')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Tanatologia-min.png')?>" alt="Consulta Tanatologia Centi-r"></a>
<h3 class="product-title"><b>Tanatología</b></h3>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Tanatologia'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 6-->
	
</div>


<div class="row justify-content-center mt-2">

<!-- Product 7-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Especial')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Especial-min.png')?>" alt="Consulta Especial Centi-r"></a>
<h3 class="product-title"><b>Educación especial</b></h3>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm"  onclick="location.href='Consultas/Especial'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 7-->

<!-- Product 8-->
<div class="col-md-4 col-sm-4 mt-2">
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Lenguaje')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Lenguaje-min.png')?>" alt="Consulta Lenguaje Centi-r"></a>
<h3 class="product-title"><b>De lenguaje</b></h3>
<h4 class="product-price">$500.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Lenguaje'">Agendar</button>
</div>

</div>
</div>
</div>
<!-- Product 8-->

</div>

</div>

<div class="modal-footer">
<button class="btn btn-outline-primary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
</div>

</div>
</div>
</div>