<section class="container mt-3">
<h3 class="text-center mt-0">- Nuestros Servicios -</h3>

<!-- CAT 1-->
<?=$this->load->view('home/cat1','',TRUE);?>
<!-- CAT 1-->

<!-- CAT 2-->
<?=$this->load->view('home/cat2','',TRUE);?>
<!-- CAT 2-->

<!-- CAT 3-->
<?=$this->load->view('home/cat3','',TRUE);?>
<!-- CAT 3-->

<!-- CAT 4-->
<?=$this->load->view('home/cat4','',TRUE);?>
<!-- CAT 4-->

<!-- CAT 6-->
<?=$this->load->view('home/cat6','',TRUE);?>
<!-- CAT 6-->

<!-- CAT 7-->
<?=$this->load->view('home/cat7','',TRUE);?>
<!-- CAT 7-->

<div class="row justify-content-center d-none">



<!-- CATEGORIA 2-->
<div class="col-md-4 col-sm-6">
<div class="card mb-30"><a class="card-img-tiles" href="<?=base_url('Conferencias')?>">
<div class="inner">

<div class="main-img">
<img src="<?=base_url('library/img/Categorias/General/Conferencias-min.png')?>" alt="Conferencias Centi-r"></div>
</div></a>

<div class="card-body text-center">
<h4 class="card-title">Conferencias</h4>
<button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modal_2">Ver</button>
</div>
</div>
</div>
<!-- CATEGORIA 2-->

<!-- CATEGORIA 1-->
<div class="col-md-4 col-sm-6">
<div class="card mb-30"><a class="card-img-tiles" href="<?=base_url('Consultas')?>">
<div class="inner">

<div class="main-img">
<img src="<?=base_url('library/img/Categorias/General/Consultas-min.png')?>" alt="Consultas Centi-r"></div>
</div></a>

<div class="card-body text-center">
<h4 class="card-title">Consultas</h4>
<button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modal_1">Ver</button>
</div>
</div>
</div>
<!-- CATEGORIA 1-->

<!-- CATEGORIA 5-->
<div class="col-md-4 col-sm-6">
<div class="card mb-30"><a class="card-img-tiles" href="<?=base_url('Terapias_alternativas')?>">
<div class="inner">

<div class="main-img">
<img src="<?=base_url('library/img/Categorias/General/Alternativas-min.png')?>" alt="Terapias Alternativas Centi-r"></div>
</div></a>

<div class="card-body text-center">
<h4 class="card-title">Terapias alternativas</h4>
<button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modal_5">Ver</button>
</div>
</div>
</div>
<!-- CATEGORIA 5-->

</div>

<div class="row justify-content-center d-none">

<!-- CATEGORIA 3-->
<div class="col-md-4 col-sm-6">
<div class="card mb-30"><a class="card-img-tiles" href="<?=base_url('Cursos')?>">
<div class="inner">

<div class="main-img"><img src="<?=base_url('library/img/Categorias/General/Cursos-min.png')?>" alt="Cursos Centi-r"></div>
</div></a>

<div class="card-body text-center">
<h4 class="card-title">Cursos</h4>
<button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modal_3">Ver</button>
</div>
</div>
</div>
<!-- CATEGORIA 3-->

<!-- CATEGORIA 4-->       
<div class="col-md-4 col-sm-6">
<div class="card mb-30"><a class="card-img-tiles" href="<?=base_url('Diplomados')?>">
<div class="inner">

<div class="main-img">
<img src="<?=base_url('library/img/Categorias/General/Diplomados-min.png')?>" alt="Diplomados Centi-r"></div>
</div></a>

<div class="card-body text-center">
<h4 class="card-title">Diplomados</h4>
<button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modal_4">Ver</button>
</div>
</div>
</div>
<!-- CATEGORIA 4-->


<!-- CATEGORIA 6-->
<div class="col-md-4 col-sm-6">
<div class="card mb-30"><a class="card-img-tiles" href="<?=base_url('Tienda')?>">
<div class="inner">

<div class="main-img">
<img src="<?=base_url('library/img/Categorias/General/Tienda-min.png')?>" alt="Tienda Centi-r"></div>
</div></a>

<div class="card-body text-center">
<h4 class="card-title">Tienda</h4>
<button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modal_6">Ver</button>
</div>
</div>
</div>
<!-- CATEGORIA 6-->

</div>


<div class="text-center mt-0 mb-3 d-none">
<a class="btn btn-outline-secondary margin-top-none" href="shop-categories.html">TODAS LAS CATEGORIAS</a>
</div>

</section>