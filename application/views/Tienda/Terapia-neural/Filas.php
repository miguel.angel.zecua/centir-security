<div class="row justify-content-center mt-3">

<!-- Product 1 -->
<div class="col-md-3 col-sm-4 mt-2">     

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Terapia-Neural/1.png')?>" style="border-radius: 25px;">
<hr class="mt-2">
<h4 class="product-price">$100.00</h4>
</div>
</div>

</div>
<!-- Product 1 -->

<!-- Product 2 -->
<div class="col-md-3 col-sm-4 mt-2">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Terapia-Neural/2.png')?>" style="border-radius: 25px;">
<hr class="mt-2">
<h4 class="product-price">$350.00</h4>
</div>
</div>

</div>
<!-- Product 2 -->

</div>