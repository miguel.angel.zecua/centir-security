<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #B32291;">
<div class="container">
<div class="column">

<h1 style="color: white;">Tienda</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="index.html" style="color: white !important;">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li style="color: white !important;">Tienda</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view active" href="<?=base_url('Tienda')?>"><span></span><span></span><span></span></a>
</div>
</div>

</div>


<!-- CUADRO 1-->
<div class="col-md-12 col-sm-12 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">CATÁLOGO DE TIENDA </b></a></h6>
</div>

<div class="collapse show" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->


<object data="<?=base_url('Temarios/Tienda.pdf')?>" type="application/pdf" width="100%" height="800px"> 
  <p>It appears you don't have a PDF plugin for this browser.
   No biggie... you can <a href="resume.pdf">click here to
  download the PDF file.</a></p>  
</object>


<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->


<div class="accordion d-none" role="tablist">



<!-- Contenido 1-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseOne" data-toggle="collapse" class="collapsed" aria-expanded="false">Maderoterapia</a></h6>
</div>
<div class="collapse" id="collapseOne" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Maderoterapia/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 1-->


<!-- Contenido 2-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseTwo" data-toggle="collapse" class="collapsed" aria-expanded="false">Acupuntura</a></h6>
</div>
<div class="collapse" id="collapseTwo" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Acupuntura/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 2-->


<!-- Contenido 3-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseTree" data-toggle="collapse" class="collapsed" aria-expanded="false">Auriculoterapia</a></h6>
</div>
<div class="collapse" id="collapseTree" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Auriculoterapia/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 3-->


<!-- Contenido 4-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseFour" data-toggle="collapse" class="collapsed" aria-expanded="false">Electroestimulación
</a></h6>
</div>
<div class="collapse" id="collapseFour" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Electroestimulacion/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 4-->


<!-- Contenido 5-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseFive" data-toggle="collapse" class="collapsed" aria-expanded="false">Terapia neural</a></h6>
</div>
<div class="collapse" id="collapseFive" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Terapia-neural/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 5-->


<!-- Contenido 6-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseSix" data-toggle="collapse" class="collapsed" aria-expanded="false">Moxibustión</a></h6>
</div>
<div class="collapse" id="collapseSix" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Moxibustion/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 6-->


<!-- Contenido 7-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseSeven" data-toggle="collapse" class="collapsed" aria-expanded="false">Imanes</a></h6>
</div>
<div class="collapse" id="collapseSeven" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Imanes/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 7-->


<!-- Contenido 8-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseEight" data-toggle="collapse" class="collapsed" aria-expanded="false">Relajación</a></h6>
</div>
<div class="collapse" id="collapseEight" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Relajacion/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 8-->


<!-- Contenido 9-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseNine" data-toggle="collapse" class="collapsed" aria-expanded="false">Ventosas</a></h6>
</div>
<div class="collapse" id="collapseNine" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Ventosas/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 9-->


<!-- Contenido 10-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseTen" data-toggle="collapse" class="collapsed" aria-expanded="false">Conoterapia</a></h6>
</div>
<div class="collapse" id="collapseTen" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Conoterapia/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 10-->


<!-- Contenido 11-->
<div class="card">
<div class="card-header" role="tab">
<h6>
<a href="#collapseEleven" data-toggle="collapse" class="collapsed" aria-expanded="false">Otros</a></h6>
</div>
<div class="collapse" id="collapseEleven" role="tabpanel" style="">
<div class="card-body">

<?=$this->load->view('Tienda/Otros/Filas','',TRUE);?>

</div>
</div>

</div>
<!-- Contenido 11-->






</div>
</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

