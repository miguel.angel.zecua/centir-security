<div class="row justify-content-center mt-3">

<!-- Product 1 -->
<div class="col-md-3 col-sm-4 mt-2">     

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/1.png')?>" style="border-radius: 25px;"></a>
<hr class="mt-2">
<h4 class="product-price">$325.00</h4>
</div>
</div>

</div>
<!-- Product 1 -->

<!-- Product 2 -->
<div class="col-md-3 col-sm-4 mt-2">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/2.png')?>" style="border-radius: 25px;"></a>
<hr class="mt-2">
<h4 class="product-price">$350.00</h4>
</div>
</div>

</div>
<!-- Product 2 -->

<!-- Product 3 -->
<div class="col-md-3 col-sm-4 mt-2 d-none">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/3.png')?>" style="border-radius: 25px;"></a>
<hr class="mt-2">
<h4 class="product-price">$250.00</h4>
</div>
</div>

</div>
<!-- Product 3 -->

<!-- Product 4 -->
<div class="col-md-3 col-sm-4 mt-2">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/4.png')?>" style="border-radius: 25px;"></a>
<hr class="mt-2">
<h4 class="product-price">$150.00</h4>
</div>
</div>

</div>
<!-- Product 4 -->

<!-- Product 5 -->
<div class="col-md-3 col-sm-4 mt-2">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/5.png')?>" style="border-radius: 25px;"></a>
<hr class="mt-2">
<h4 class="product-price">$400.00</h4>
</div>
</div>

</div>
<!-- Product 5 -->

<!-- Product 6 -->
<div class="col-md-3 col-sm-4 mt-2">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/6.png')?>" style="border-radius: 25px;"></a>
<hr class="mt-2">
<h4 class="product-price">$2.50</h4>
</div>
</div>

</div>
<!-- Product 6 -->

<!-- Product 7 -->
<div class="col-md-3 col-sm-4 mt-2">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/7.png')?>" style="border-radius: 25px;"></a>
<hr class="mt-2">
<h4 class="product-price">$20.00</h4>
</div>
</div>

</div>
<!-- Product 7 -->

<!-- Product 8 -->
<div class="col-md-3 col-sm-4 mt-2 d-none">  

<div class="grid-item" >
<div class="product-card">
<img src="<?=base_url('library/img/Tienda/Moxibustion/8.png')?>" style="border-radius: 25px;"></a>
<h3 class="product-title"><b>Mini moxa para aguja caja 200 piezas
</b></h3>
<h4 class="product-price">$350.00</h4>
</div>
</div>

</div>
<!-- Product 8 -->

</div>