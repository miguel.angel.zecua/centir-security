<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search " method="get">
<input type="text" name="site_search"  placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column text-center">

<h1 class="text-dark">CONTACTO</h1>
</div>

</div>
</div>

<div class="container padding-bottom-2x mb-2">
<div class="row">

<div class="col-md-7">
<div class="display-4 text-muted opacity-75 mb-30">REDES SOCIALES</div>
</div>

<div class="col-md-5">
<div class="contenedor-redes-sociales">
<a class="facebook" href="https://www.facebook.com/TecnicasIntegrativasYDeRenovacion/
" target="_blank">
<span class="circulo"><i class="socicon-facebook"></i></span>
<span class="titulo">Facebook</span>
<span class="titulo-hover">Seguir</span>
</a>

<a class="twitter" href="https://www.instagram.com/centi_r_/
" target="_blank">
<span class="circulo"><i class="socicon-instagram"></i></span>
<span class="titulo">Instagram</span>
<span class="titulo-hover">Seguir</span>
</a>

<a class="gplus" href="mailto:centir2020@gmail.com" target="_blank">
<span class="circulo"><i class="socicon-googleplus"></i></span>
<span class="titulo">Gmail</span>
<span class="titulo-hover">Seguir</span>
</a>
</div>
</div>

</div>

<hr class="margin-top-2x">
<div class="row margin-top-2x">

<div class="col-md-7">
<div class="display-4 text-muted opacity-75 mb-30">CONTACTO</div>
</div>

<div class="col-md-5 justify-content-center">

<h4 class="margin-top-1x text-center">Calle: Privada de los Pinos #2, San Buenaventura Atempan CP 90010
</h4>

<br>

<h4 class="margin-top-1x text-center">Tel: 246 248 6999
</h4>

<h4 class="margin-top-1x text-center">Correo: Centir2020@gmail.com
</h4>

</div>

</div>

<hr class="margin-top-2x">

<h3 class="margin-top-3x text-center mb-30">DIRECCIÓN</h3>

<div class="row justify-content-center rounded">

  
<div class="col-md-11 col-sm-11 mb-3 d-none">
<img src="<?=base_url('library/img/Contact/Direccion.png')?>" class="rounded">
<h3 class="margin-top-1x text-center">Leonarda Gómez Blanco 34, Acxotla del Río, Totolac, Tlaxcala. CP 90160
</h3>
</div>

<div class="col-md-11 col-sm-11 mt-2">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1026.4539053706076!2d-98.21520872079805!3d19.32365373839564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfdece02bcea51%3A0x551d9fd2f09efe3c!2sPrivada%20de%20los%20Pinos%20No.%202%20San%20Buenaventura%20Atempan%2CTlaxcala%20C.P.%2090010%2C%20C.%20Pinos%202%2C%20San%20Buenaventura%20Atempa%2C%2090010%20Tlaxcala%20de%20Xicoht%C3%A9ncatl%2C%20Tlaxcala!5e0!3m2!1ses!2smx!4v1658503566825!5m2!1ses!2smx" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</h3>
</div>

</div>
</div>
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>
<style type="text/css">

.contenedor-redes-sociales {
    margin: 20px 0;
    padding: 20px;
    text-align: center;
}

.contenedor-redes-sociales a {
    position: relative;
    display: inline-block;
    height: 40px;
    width: 160px;
    line-height: 35px;
    padding: 0;
    color: #fff;
    border-radius: 50px;
    background: #fff;
    margin: 5px;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.1);
    box-shadow: 0 0 2px rgba(0, 0, 0, 0.12), 0 2px 4px rgba(0, 0, 0, 0.24);
}

.contenedor-redes-sociales a:hover span.circulo {
    left: 100%;
    margin-left: -35px;
    background: #fff;
}

.contenedor-redes-sociales a:hover span.titulo {
    opacity: 0;
}

.contenedor-redes-sociales a:hover span.titulo-hover {
    opacity: 1;
    color: #fff;
}

.contenedor-redes-sociales a span.titulo-hover {
    opacity: 0;
}

.contenedor-redes-sociales a span.circulo {
    display: block;
    color: #fff;
    position: absolute;
    float: left;
    margin: 3px;
    line-height: 30px;
    height: 30px;
    width: 30px;
    top: 0;
    left: 0;
    transition: all 0.5s;
    border-radius: 50%;
}

.contenedor-redes-sociales a span.circulo i {
    width: 100%;
    text-align: center;
    font-size: 16px;
    line-height: 30px;
}

.contenedor-redes-sociales a span.titulo, .contenedor-redes-sociales a span.titulo-hover {
    position: absolute;
    text-align: center;
    margin: 0 auto;
    font-size: 16px;
    font-weight: 400;
    transition: .5s;
}

.contenedor-redes-sociales a span.titulo {
    right: 15px
}

.contenedor-redes-sociales a span.titulo-hover {
    left: 15px
}


/*----------Colores de los botones----------*/

.contenedor-redes-sociales .facebook {
    border: 2px solid #3b5998;
}

.contenedor-redes-sociales .facebook:hover, .contenedor-redes-sociales .facebook span.circulo {
    background: #3b5998;
}

.contenedor-redes-sociales .facebook:hover span.circulo, .contenedor-redes-sociales .facebook span.titulo {
    color: #3b5998;
}

.contenedor-redes-sociales .twitter {
    border: 2px solid #E1306C;
}

.contenedor-redes-sociales .twitter:hover, .contenedor-redes-sociales .twitter span.circulo {
    background: #E1306C;
}

.contenedor-redes-sociales .twitter:hover span.circulo, .contenedor-redes-sociales .twitter span.titulo {
    color: #E1306C;
}

.contenedor-redes-sociales .gplus {
    border: 2px solid #dd4b39;
}

.contenedor-redes-sociales .gplus:hover, .contenedor-redes-sociales .gplus span.circulo {
    background: #dd4b39;
}

.contenedor-redes-sociales .gplus:hover span.circulo, .contenedor-redes-sociales .gplus span.titulo {
    color: #dd4b39;
}


/*--------------Mediaqueries--------------*/

@media screen and (max-width: 480px) {
    .contenedor-redes-sociales a {
        width: 100%;
        margin: 0;
        margin-bottom: 10px;
    }
}
</style>
</body>
</html>

