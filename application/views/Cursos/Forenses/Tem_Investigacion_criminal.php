<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 LINEAMIENTOS BÁSICOS EN LA INVESTIGACIÓN</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">1.1 Fundamentos de la investigación.</b>
<br><br>
<b class="ml-2">1.2 Metodología de la investigación.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 FUENTES DE INFORMACIÓN EN LA INVESTIGACIÓN</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Lugar de los hechos.</b>
<br><br>
<b class="ml-2">2.2 Personas.</b>
<br><br>
<b class="ml-2">2.3 Víctimas.</b>
<br><br>
<b class="ml-2">2.4 Testigos.</b>
<br><br>
<b class="ml-2">2.5 Archivos y registros.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 MODUS OPERANDI EN LA INVESTIGACIÓN</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">3.1 Tipología del delincuente.</b>
<br><br>
<b class="ml-2">3.2 Diversas modalidades del M.O.</b>
<br><br>
<b class="ml-2">3.3 Época actual.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

<!-- CUADRO 4-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">4.0 ENTREVISTA E INTERROGATORIO</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">4.1 Criterios generales de entrevista.</b>
<br><br>
<b class="ml-5">4.1.1 Sondeo inicial (Búsqueda de información).</b>
<br><br>
<b class="ml-2">4.2 Criterios generales en el interrogatorio.</b>
<br><br>
<b class="ml-5">4.2.1 Técnicas de interrogatorio.</b>
<br><br>
<b class="ml-5">4.2.2 Sala de interrogatorio.</b>
<br><br>
<b class="ml-2">4.3 Sintomatología de la mentira.</b>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 4-->

<!-- CUADRO 5-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">5.0 CASO PRÁCTICO</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 5-->