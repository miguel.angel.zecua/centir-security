<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MÓDULO 1</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">1.1 Introducción a la Balística.</b>
<br><br>
<b class="ml-2">1.2 Balística y Balística Forense.</b>
<br><br>
<b class="ml-2">1.3 Clasificación de la Balística Forense.</b>
<br><br>
<b class="ml-2">1.4 Introducción a la Historia y Evolución de la Armas de Fuego.</b>
<br><br>
<b class="ml-2">1.5 Concepto de Arma de Fuego.</b>
<br><br>
<b class="ml-2">1.6 Clasificación de las Armas de Fuego.</b>
<br><br>
<b class="ml-2">1.7 Medidas de Seguridad al Manipular Armas de Fuego.</b>
<br><br>
<b class="ml-2">1.8 Nomenclatura Interna y Externa de las Armas de Fuego.</b>
<br><br>
<b class="ml-2">1.9 Marca de Fabricantes de Armas de Fuego.</b>
<br><br>
<b class="ml-2">1.10 Calibre de un arma de fuego. Nominal y Real.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MÓDULO 2</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Clasificación de las Puntas en Balas.</b>
<br><br>
<b class="ml-2">2.2 Pólvora y sus Tipos.</b>
<br><br>
<b class="ml-2">2.3 Identificación de Armas de Fuego y Cartuchos.</b>
<br><br>
<b class="ml-2">2.4 Teoría AFTE Micro comparativa.</b>
<br><br>
<b class="ml-2">2.5 Características Generales del Rayado Código GRC.</b>
<br><br>
<b class="ml-2">2.6 Balística de Efectos.</b>
<br><br>
<b class="ml-2">2.7 Lesiones por Armas de Fuego.</b>
<br><br>
<b class="ml-2">2.8 Trayecto y Trayectoria Balística.</b>
<br><br>
<b class="ml-2">2.9 Física Aplicada a la Balística Forense.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">PRÁCTICA DE TRAYECTORIAS BALÍSTICAS Y PROCESAMIENTO DE INDICIOS BALÍSTICOS 
</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body text-center">
<!-- Body-->

<b class="text-center">20 de Marzo del 2021</b>
<br><br>
<b class="text-success">Todos los sábados de 4:00 a 6:00 pm</b>
<br><br>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

