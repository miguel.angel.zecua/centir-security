<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 ORÍGENES DE LA ESCRITURA.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">1.1 El lenguaje y su relación con la escritura.</b>
<br><br>
<b class="ml-2">1.2 Vocabulario grafoscópico.</b>
<br><br>
<b class="ml-2">1.3 Conceptos iniciales.</b>
<br><br>
<b class="ml-2">1.4 Grafoscopía.</b>
<br><br>
<b class="ml-2">1.5 Ciencias que apoyan su estudio.</b>
<br><br>
<b class="ml-2">1.6 Órganos que intervienen.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 CARACTERÍSTICAS DE ORDEN PARTICULAR (GESTOS GRÁFICOS Y MORFOLOGÍA).</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Grafismo.</b>
<br><br>
<b class="ml-2">2.2 Rasgos.</b>
<br><br>
<b class="ml-2">2.3 Trazos.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 CARACTERÍSTICAS DE ORDEN GENERAL 
</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">3.1 Alineamiento.</b>
<br><br>
<b class="ml-2">3.2 Dirección.</b>
<br><br>
<b class="ml-2">3.3 Espacios interliterales.</b>
<br><br>
<b class="ml-2">3.4 Espacio entre palabras.</b>
<br><br>
<b class="ml-2">3.5 Espontaneidad.</b>
<br><br>
<b class="ml-2">3.6 Habilidad escritural.</b>
<br><br>
<b class="ml-2">3.7 Inclinación.</b>
<br><br>
<b class="ml-2">3.8 Presión muscular.</b>
<br><br>
<b class="ml-2">3.9 Proporción dimensional.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

<!-- CUADRO 4-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">4.0 PRÁCTICA 
</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">4.1 Características de orden particular (gestos gráficos y morfología).</b>
<br><br>
<b class="ml-2">4.2 Señas particulares de la escritura.</b>
<br><br>
<b class="ml-2">4.3 Constantes o gestos gráficos.</b>
<br><br>
<b class="ml-2">4.4 Elementos respectivos en la escritura.</b>
<br><br>
<b class="ml-2">4.5 Gestos gráficos y morfología escritural (como identificarlos).</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 4-->

<!-- CUADRO 5-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">5.0 DOCUMENTOSCOPÍA 
</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">5.1 Documentoscopía y documentología.</b>
<br><br>
<b class="ml-2">5.2 Conceptos.</b>
<br><br>
<b class="ml-2">5.3 Aplicaciones.</b>
<br><br>
<b class="ml-2">5.4 Útiles inscriptores.</b>
<br><br>
<b class="ml-2">5.5 Tipos.</b>
<br><br>
<b class="ml-2">5.6 Contenidos.</b>
<br><br>
<b class="ml-2">5.7 Soportes.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 5-->

