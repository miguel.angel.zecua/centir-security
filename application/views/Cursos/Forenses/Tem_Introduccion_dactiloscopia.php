<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 ANTECEDENTES Y CONCEPTOS.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">1.1 Sistemas dactilares actuales.</b>
<br><br>
<b class="ml-2">1.2 Sistema juan vucetich.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 ARCO.</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Presilla interna.</b>
<br><br>
<b class="ml-2">2.2 Presilla externa.</b>
<br><br>
<b class="ml-2">2.3 Verticilo.</b>
<br><br>
<b class="ml-2">2.4 Partes de los dactilogramas.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 PARTES DE LOS DACTILOGRAMAS 
</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">3.1 Sistemas crestales.</b>
<br><br>
<b class="ml-2">3.2 Líneas directrices.</b>
<br><br>
<b class="ml-2">3.3 Deltas.</b>
<br><br>
<b class="ml-2">3.4 Centro nuclear.</b>
<br><br>
<b class="ml-2">3.5 Punto déltico.</b>
<br><br>
<b class="ml-2">3.6 Clasificación y subclasificación.</b>
<br><br>
<b class="ml-2">3.7 Puntos característicos.</b>


<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

<!-- CUADRO 4-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">4.0 PRÁCTICA DE CAMPO 
</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">4.1 Toma de ficha decadactilar.</b>
<br><br>
<b class="ml-2">4.2 Rastreo búsqueda y fijación de fragmento dactilares.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 4-->

<!-- CUADRO 5-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">5.0 PRÁCTICA FORENSE DE INTERVENCIÓN DACTILOSCÓPICA
</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">5.1 Fijación en el lugar de los hechos.</b>
<br><br>
<b class="ml-2">5.2 Búsqueda de fragmentos dactilares.</b>
<br><br>
<b class="ml-2">5.3 AFijación de los fragmentos dactilares.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 5-->

<!-- CUADRO 6-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_6" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">6.0 ENTREGA DEL DICTAMEN Y/O INFORME
</b></a></h6>
</div>

<div class="collapse" id="temario_6" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 6-->

