<div class="accordion" id="div6" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#collapseSeven" data-toggle="collapse" style="font-size: 16px"><b>Otros</b></a></h6>
</div>

<div class="collapse" id="collapseSeven" data-parent="#div6" role="tabpanel">
<div class="card-body">



<div class="row justify-content-center">

<div class="col-md-3 col-sm-4">

<!-- Product 9-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Mercados_financieros')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Mercados_financieros.png')?>" alt="Product"></a>
<h3 class="product-title"><b>¿Cómo invertir en los mercados financieros?</b></h3>
<h4 class="product-price invisible">$1000.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Mercados_financieros'">INFO</button>
</div>

</div>
</div>
<!-- Product 9-->

</div>
</div>
<!-- Body-->
</div>
</div>
</div>
</div>