<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0. TEORÍA DE DESARROLLO COGINITIVO DE JEAN PIAGET (ETAPA SENSORIO MOTORA0-2 AÑOS).</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 ESTIMULACIÓN TEMPRANA</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Definición.</b>
<br><br>
<b class="ml-2">2.2 Áreas de Desarrollo en Estimulación Temprana.</b>
<br><br>
<b class="ml-5">2.2.1 Área Sensorio-Motora.</b>
<br><br>
<b class="ml-5">2.2.2 Área de Lenguaje.</b>
<br><br>
<b class="ml-5">2.2.3 Área Intelectual o Cognitiva.</b>
<br><br>
<b class="ml-5">2.2.4 Área Socio-Emocional.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 BENEFICIOS Y ACTIVIDADES DE ESTIMULACIÓN TEMPRANA.
</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">0-4 Meses</b>
<br><br>
<b class="ml-2">4-8 Meses</b>
<br><br>
<b class="ml-2">8-12 Meses</b>
<br><br>
<b class="ml-2">12-18 Meses</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

