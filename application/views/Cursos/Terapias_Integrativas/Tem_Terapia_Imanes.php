<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 INTRODUCCIÓN.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">1.1 Antecedentes históricos.</b>
<br><br>
<b class="ml-2">1.2 Fundamentos científicos.</b>
<br><br>
<b class="ml-2">1.3 Efectos de los campos magnéticos en los seres vivos.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 BENEFICIOS.</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Tipos de imanes.</b>
<br><br>
<b class="ml-2">2.2 Contra indicaciones y riesgos en la terapia de imanes.</b>
<br><br>
<b class="ml-2">2.3 Diferentes polaridades.</b>
<br><br>
<b class="ml-2">2.4 ¿Para qué sirve el polo negativo?</b>
<br><br>
<b class="ml-2">2.5 ¿Para qué sirve el polo positivo?</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 EQUILIBRIO DEL PH 
</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">3.1 Polarización del agua y sus beneficios.</b>
<br><br>
<b class="ml-2">3.2 Algunos tratamientos con imanes.</b>
<br><br>
<b class="ml-2">3.3 Zona de dolor.</b>


<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

<!-- CUADRO 4-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">4.0 TÉCNICA DE APLICACIÓN DE IMANES 
</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">4.1 Herramienta de diagnóstico.</b>
<br><br>
<b class="ml-2">4.2 Mapa clásico Su Jok.</b>
<br><br>
<b class="ml-2">4.3 Testeo.</b>
<br><br>
<b class="ml-2">4.4 Tratamiento de síntomas con imanes.</b>
<br><br>
<b class="ml-2">4.5 Equilibrio biomagnético.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 4-->

<!-- CUADRO 5-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">5.0 EMOCIONES
</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 5-->


