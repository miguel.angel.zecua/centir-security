<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 INTRODUCCIÓN.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: Entenderá por qué somos energía y cómo, Influye el campo magnético en nuestros chakras y para qué es importante la integración de la mente cuerpo y alma para llegar a una cura integral.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 MAGNETOTERAPIA.</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<p>Objetivo:  Se aprenderá el uso de los campos magnéticos para regular la energía del cuerpo y así erradicar los síntomas que el cuerpo siente cuando hay un descontrol energético.</p>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 QUÉ SON LOS CHAKRAS.</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: Se aprenderá que son los chakras y la importancia de los 7 principales vórtices energéticos en nuestro cuerpo.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

<!-- CUADRO 4-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">4.0 ALINEACIÓN DE CHACKRAS.</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: Aprenderás la importancia y los beneficios de un equilibrio de magnético de chakra los beneficios.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 4-->

<!-- CUADRO 5-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">5.0 MULADHARA.</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 5-->

<!-- CUADRO 6-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_6" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">6.0 SVADHISTHANA.</b></a></h6>
</div>

<div class="collapse" id="temario_6" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>
Objetivo: aprenderás la relación  que tiene este chakra con cada elemento y órgano del cuerpo humano. Así como la relación con su sonido, la acción, el color y función.
</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 6-->

<!-- CUADRO 7-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_7" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">7.0 MANIPURA.</b></a></h6>
</div>

<div class="collapse" id="temario_7" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: aprenderás la relación que tiene este chakra con cada elemento y órgano del cuerpo humano. Así como la relación con su sonido, la acción, el color y función.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 7-->

<!-- CUADRO 8-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_8" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">8.0 ANAHATA.</b></a></h6>
</div>

<div class="collapse" id="temario_8" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo:  aprenderás la relación que tiene este chakra con cada elemento y órgano del cuerpo humano. Así como la relación con su sonido, la acción, el color y función.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 8-->

<!-- CUADRO 9-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_9" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">9.0 VISHUDDHAHA.</b></a></h6>
</div>

<div class="collapse" id="temario_9" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: aprenderás la relación  que tiene este chakra con cada elemento y órgano del cuerpo humano. Así como la relación con su sonido, la acción, el color y función.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 9-->

<!-- CUADRO 10-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_10" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">10.0 SAHASRARA.</b></a></h6>
</div>

<div class="collapse" id="temario_10" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: aprenderás la relación que tiene este chakra con cada elemento y órgano del cuerpo humano. Así como la relación con su sonido, la acción, el color y función.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 10-->

<!-- CUADRO 11-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_11" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">11.0 AJNA.</b></a></h6>
</div>

<div class="collapse" id="temario_11" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: aprenderás la relación que tiene este chakra con cada elemento y órgano del cuerpo humano. Así como la relación con su sonido, la acción, el color y función.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 11-->

<!-- CUADRO 12-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_12" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">12.0 LOS CUARZOS Y LOS CHAKRAS.</b></a></h6>
</div>

<div class="collapse" id="temario_12" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: aprenderás a conocer las propiedades curativas de los cuarzos por medio de su energía, y reconocerás el color que le corresponde a cada chakra y por qué.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 12-->

<!-- CUADRO 13-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_13" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">13.0 AROMATERAPIA Y LOS CHAKRAS.</b></a></h6>
</div>

<div class="collapse" id="temario_13" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 13-->

<!-- CUADRO 14-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_14" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">14.0 USO DEL PENDULO EN LOS CHAKRAS.</b></a></h6>
</div>

<div class="collapse" id="temario_14" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: aprenderás el uso del pendulocomo una herramienta para identificar la zona exacta de los chackras asi mismo una técnica para equilíbralos energéticamente.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 14-->

<!-- CUADRO 15-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_15" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">15.0 LA IMPORTANCIA DEL MAGNETISMO PARA EL EQUILIBRIO DE CHAKRAS.</b></a></h6>
</div>

<div class="collapse" id="temario_15" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo:  aprenderás como interactúa el magnetismo con los chakras y cuál es la forma correcta de colocarlos imanes para equilibrar y armonizar todo el ser.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 15-->

<!-- CUADRO 16-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_16" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">16.0 CIERRE DEL CURSO CON PROPIOS TESTIMONIOS.</b></a></h6>
</div>

<div class="collapse" id="temario_16" role="tabpanel">
<div class="card-body">
<!-- Body-->
<p>Objetivo: experimentaremos y comentaremos nuestras propias experiencias para llevarnos un testimonio y aprender a asentar lo aprendido.</p>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 16-->














