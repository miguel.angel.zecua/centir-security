<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 ALIMENTOS FUNCIONALES.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 PLANTAS MEDICINALES.</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 ALIMENTOS PREBIÓTICOS Y PROBIÓTICOS.</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

<!-- CUADRO 4-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">4.0 FIBRA.</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 4-->

<!-- CUADRO 5-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">5.0 ASPECTOS FUNCIONALES DE LOS PRODUCTOS LÁCTEOS.</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 5-->

<!-- CUADRO 6-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_6" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">6.0. COMPONENTES BIOACTIVOS DE LOS ALIMENTOS.</b></a></h6>
</div>

<div class="collapse" id="temario_6" role="tabpanel">
<div class="card-body">
<!-- Body-->
<b class="ml-2">6.1 Vitaminas.</b>
<br><br>
<b class="ml-2">6.2 Minerales.</b>
<br><br>
<b class="ml-2">6.3 Carotenoides.</b>
<br><br>
<b class="ml-2">6.4 Esteroles vegetales.</b>
<br><br>
<b class="ml-2">6.5 Componentes fenólicos.</b>
<br><br>
<b class="ml-2">6.6 Omega 3.</b>
<br><br>
<b class="ml-2">6.7 Resveratrol.</b>
<br><br>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 6-->

<!-- CUADRO 7-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_7" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">7.0. LISTA DE COMPRAS Y PLANEACIÓN.</b></a></h6>
</div>

<div class="collapse" id="temario_7" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 7-->

<!-- CUADRO 8-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_8" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">8.0. JUGOS.</b></a></h6>
</div>

<div class="collapse" id="temario_8" role="tabpanel">
<div class="card-body">
<!-- Body-->
<b class="ml-2">8.1 Jugos Verdes.</b>
<br><br>
<b class="ml-2">8.2 Granola.</b>
<br><br>
<b class="ml-2">8.3 Salsa de semillas.</b>
<br><br>
<b class="ml-2">8.4 Germinados, sazonadores y aderezos, 8.5  chocolate, chiles en vinagre, platillos, ensaladas, salsas multiusos.</b>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 8-->

<!-- CUADRO 9-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_9" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">8.0. ¿CÓMO COMER PARA GANAR SALUD? .</b></a></h6>
</div>

<div class="collapse" id="temario_9" role="tabpanel">
<div class="card-body">
<!-- Body-->
<b class="ml-2">9.1 Al despertar.</b>
<br><br>
<b class="ml-2">9.2 Desayuno.</b>
<br><br>
<b class="ml-2">9.3 Comida.</b>
<br><br>
<b class="ml-2">9.4 Cena.</b>
<br><br>
<b class="ml-2">9.5 Ayunos.</b>
<br><br>
<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 9-->



