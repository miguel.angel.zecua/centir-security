<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 INTRODUCCIÓN A LA AURICULOTERAPIA.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 SOBREPESO.</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Causas.</b>
<br><br>
<b class="ml-5">2.1.1 Físicas.</b>
<br><br>
<b class="ml-5">2.1.2 Emocionales.</b>
<br><br>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 BASES DEL TRATAMIENTO.</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

<!-- CUADRO 4-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">4.0 ANATOMÍA AURICULAR.</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 4-->

<!-- CUADRO 5-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">5.0 PROCEDIMIENTO PASO A PASO.</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 5-->



