<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #EAFF4A;">
<div class="container">
<div class="column">

<h1>Cursos y Diplomados</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li>Cursos y Diplomados</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view " href="<?=base_url('Cursos')?>"><span></span><span></span><span></span></a>
<a class="list-view active" href="<?=base_url('Cursos/List')?>"><span></span><span></span><span></span></a></div>
</div>

</div>

<!-- Fila-->
<div class="row justify-content-center">

<div class="col-lg-11">

<div class="p-3 mb-2 bg-info text-dark text-center rounded mb-3"><b style="font-size: 18px">Terapias Integrativas</b></div>

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Terapia_Imanes')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Terapia_de_Imanes-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Terapia de Imanes para las Emociones</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Su objetivo consiste en entender que la fuerza magnética puede favorecer el estado energético de las personas para poder llegar a conseguir la salud física, emocional y mental deseada.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapia_Imanes'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Medicina_Cannabica')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Medicina_Cannabica-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Medicina Cannábica</h3>

<h4 class="product-price invisible">$1500.00</h4>
<p class="hidden-xs-down">Conocer los principios básicos de la planta del cannabis, sus características y sus aplicaciones medicinales; utilizar el conocimiento para analizar problemas de salud específicos donde pueda ser aplicada la medicina cannábica, así como las posibles interacciones medicamentosas, la dosificación adecuada y los posibles riesgos.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Medicina_Cannabica'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Auriculoterapia')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Auriculoterapia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Auriculoterapia para control de peso</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Conocer los fundamentos y aplicación de la Auriculoterapia para Control de Peso para lograr la talla adecuado en cada persona, y así conseguir beneficios como sentirse y verse mejor por dentro y por fuera.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Auriculoterapia'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Dejar_amar')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Dejar_amar-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Dejar ir es amar</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Conocer los fundamentos y aplicación de la Auriculoterapia para Control de Peso para lograr la talla adecuado en cada persona, y así conseguir beneficios como sentirse y verse mejor por dentro y por fuera.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Dejar_amar'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Terapia_neural')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Terapia_neural-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Terapia neural</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Conocer los fundamentos y aplicación de la Auriculoterapia para Control de Peso para lograr la talla adecuado en cada persona, y así conseguir beneficios como sentirse y verse mejor por dentro y por fuera.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapia_neural'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Alineacion_chakras')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Alineacion_chakras-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Alineación de chakras con imanes</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Conocer los fundamentos y aplicación de la Auriculoterapia para Control de Peso para lograr la talla adecuado en cada persona, y así conseguir beneficios como sentirse y verse mejor por dentro y por fuera.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Alineacion_chakras'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Alimentos_funcionales')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Alimentos_funcionales-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Alimentos funcionales</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Conocer los fundamentos y aplicación de la Auriculoterapia para Control de Peso para lograr la talla adecuado en cada persona, y así conseguir beneficios como sentirse y verse mejor por dentro y por fuera.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Alimentos_funcionales'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<div class="p-3 mb-2 bg-info text-dark text-center rounded mb-3"><b style="font-size: 18px">Psicología</b></div>

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Dinero_espiritu')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Dinero_espiritu-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Dinero y espíritu</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Descubre como los hábitos aprendidos en la familia y nuestra percepción sobre el dinero influyen en la forma en la que buscamos, ganamos y gastamos el dinero y conoce nuevas formas de hacer que el dinero venga a ti.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Dinero_espiritu'">INFO</button>
</div>
</div>
</div>
<!-- Product-->


<div class="p-3 mb-2 bg-info text-dark text-center rounded mb-3"><b style="font-size: 18px">Pedagogía</b></div>

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Estimulacion_temprana')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Estimulacion_temprana-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Estimulación temprana</h3>

<h4 class="product-price invisible">$300.00</h4>
<p class="hidden-xs-down">Busca estimular al niño de manera oportuna, ofrecerle una gama de experiencias que le permiten formar las bases para la adquisición de futuros aprendizajes y desarrollar sus capacidades.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Estimulacion_temprana'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Cursos/Lectoescritura_sensorial')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Lectoescritura_sensorial-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Lectoescritura sensorial</h3>

<h4 class="product-price invisible">$1250.00</h4>
<p class="hidden-xs-down">Descubre las bondades de los alimentos funcionales y aprende a incluirlos en tu dieta diaria para obtener beneficios adicionales mientras comes. Aprende a seleccionar alimentos por su valor y aportaciones nutricionales sin sacrificar el buen sabor de los alimentos.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Lectoescritura_sensorial'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<div class="p-3 mb-2 bg-info text-dark text-center rounded mb-3"><b style="font-size: 18px">Forenses</b></div>

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Balistica_Forense')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Balistica_Forense-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Balística Forense e identificación de Armas de Fuego</h3>

<h4 class="product-price invisible">$700.00</h4>
<p class="hidden-xs-down">Proporcionar al discente las herramientas técnicas y científicas para la investigación balística forense en apoyo a las instancias de administración y procuración de justicia.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Balistica_Forense'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Introduccion_Estudio')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Grafoscopia_Documentoscopia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Introducción Al Estudio De La Grafoscopía Y Documentoscopía</h3>

<h4 class="product-price invisible">$1800.00</h4>
<p class="hidden-xs-down">Conocer y entender las características y estructuración de los grafismos, sus modificaciones, alteraciones y correcciones, así como las técnicas para analizar los diversos documentos, e identificar el origen gráfico.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Introduccion_Estudio'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Cursos/Investigacion_criminal')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Criminal-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Investigación criminal</h3>

<h4 class="product-price invisible">$1800.00</h4>
<p class="hidden-xs-down">Descubrir, analizar y comprobar científicamente la participación en un hecho delictuoso y los factores que influyeron en el comportamiento delictivo.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Investigacion_criminal'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Introduccion_dactiloscopia')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Dactiloscopia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Introducción a la dactiloscopía</h3>

<h4 class="product-price invisible">$00.00</h4>
<p class="hidden-xs-down">Conocer las generalidades de todo dactilograma, así como las características propias de toda huella dactilar aprender a clasificar los diferentes tipos de dactilogramas que existen, lo anterior con la finalidad de que la persona maneje la forma más adecuada de llevar a cabo tomas de huellas dactilar, así como de los métodos más adecuados para hacer la clasificación de cada dactilograma.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Introduccion_dactiloscopia'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<div class="p-3 mb-2 bg-info text-dark text-center rounded mb-3"><b style="font-size: 18px">Jurídicos</b></div>

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Cursos/Lectura_redaccion')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Lectura_redaccion-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Lectura, redacción y argumentación jurídica</h3>

<h4 class="product-price invisible">$1800.00</h4>
<p class="hidden-xs-down">Retomar las bases aprendidas desde nuestra educación básica con la finalidad de generar habilidades que permitan a los alumnos una ortografía y redacción impecables para así aplicarlos dentro del ámbito jurídico.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Lectura_redaccion'">INFO</button>
</div>
</div>
</div>
<!-- Product-->

<div class="p-3 mb-2 bg-info text-dark text-center rounded mb-3"><b style="font-size: 18px">Otros</b></div>

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Cursos/Mercados_financieros')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Mercados_financieros.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">¿Cómo invertir en los mercados financieros?</h3>

<h4 class="product-price invisible">$1000.00</h4>
<p class="hidden-xs-down">El alumno conozca y comprenda los mercados
financieros así como sus instrumentos de inversión además
del sistema financiero Mexicano y su normativa..</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Mercados_financieros'">INFO</button>
</div>
</div>
</div>
<!-- Product-->


</div>
       
</div>
<!-- Fila-->

</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

