<div class="accordion" id="div1" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#collapseTwo" data-toggle="collapse" style="font-size: 16px"><b>Terapias Integrativas</b></a></h6>
</div>

<div class="collapse" id="collapseTwo" data-parent="#div1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- CELDA 1-->
<div class="row justify-content-center d-none">

<div class="col-md-3 col-sm-4 mt-3">
	<!-- Product 1-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Terapia_Imanes')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Terapia_de_Imanes-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Terapia de Imanes para las Emociones</b></h3>
<h4 class="product-price invisible">$1250.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Terapia_Imanes'">INFO</button>
</div>

</div>
</div>
<!-- Product 1-->
</div>

<div class="col-md-3 col-sm-4 mt-3">
<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Medicina_Cannabica')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Medicina_Cannabica-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Medicina Cannábica</b><br><br></h3>
<h4 class="product-price invisible">$1500.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Medicina_Cannabica'">INFO</button>
</div>

</div>
</div>
<!-- Product 2-->
</div>

<div class="col-md-3 col-sm-4 mt-3">
<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Auriculoterapia')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Auriculoterapia-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Auriculoterapia para control de peso</b></h3>
<h4 class="product-price invisible">$1250.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Auriculoterapia'">INFO</button>
</div>

</div>
</div>
<!-- Product 3-->
</div>

	
</div>

<!-- CELDA 1-->

<!-- CELDA 2-->

<div class="row justify-content-center d-none">

<div class="col-md-3 col-sm-4 mt-3">
	<!-- Product 1-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Dejar_amar')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Dejar_amar-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Dejar ir es amar</b></h3>
<br>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Dejar_amar'">INFO</button>
</div>

</div>
</div>
<!-- Product 1-->
</div>

<div class="col-md-3 col-sm-4 mt-3">
<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Terapia_neural')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Terapia_neural-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Terapia neural</b><br><br></h3>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Terapia_neural'">INFO</button>
</div>

</div>
</div>
<!-- Product 2-->
</div>

<div class="col-md-3 col-sm-4 mt-3">
<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Alineacion_chakras')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Alineacion_chakras-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Alineación de chakras con imanes</b></h3>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Alineacion_chakras'">INFO</button>
</div>

</div>
</div>
<!-- Product 3-->
</div>

	
</div>
<!-- CELDA 2-->

<!-- CELDA 3-->
<div class="row justify-content-center d-none">

<div class="col-md-3 col-sm-4 mt-3">
	<!-- Product 1-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Alimentos_funcionales')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Alimentos_funcionales-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Alimentos funcionales</b></h3>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Alimentos_funcionales'">INFO</button>
</div>

</div>
</div>
<!-- Product 1-->
</div>


</div>
<!-- CELDA 3-->

<!-- Body-->
</div>
</div>
</div>
</div>