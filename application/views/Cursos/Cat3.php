<div class="accordion" id="div3" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#collapseFour" data-toggle="collapse" style="font-size: 16px"><b>Pedagogía</b></a></h6>
</div>

<div class="collapse" id="collapseFour" data-parent="#div3" role="tabpanel">
<div class="card-body">

<!-- Body-->

<div class="row justify-content-center">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-3 d-none">

<div class="grid-item">
<div class="product-card">
          
<a class="product-thumb" href="<?=base_url('Cursos/Estimulacion_temprana')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Estimulacion_temprana-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Estimulación temprana</b><br><br></h3>
<h4 class="product-price invisible">$300.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Estimulacion_temprana'">INFO</button>
</div>

</div>
</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-3">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Lectoescritura_sensorial')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Lectoescritura_sensorial-min.png')?>" alt="Product"></a>

<h4 class="product-price invisible">$00.00</h4>

          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Lectoescritura_sensorial'">INFO</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

</div>
<!-- Body-->
</div>
</div>
</div>
</div>