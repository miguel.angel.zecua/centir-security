<div class="accordion" id="div5" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#collapseSix" data-toggle="collapse" style="font-size: 16px"><b>Jurídicos</b></a></h6>
</div>

<div class="collapse" id="collapseSix" data-parent="#div5" role="tabpanel">
<div class="card-body">



<div class="row justify-content-center d-none">

<div class="col-md-3 col-sm-4">

<!-- Product 9-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Lectura_redaccion')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Lectura_redaccion-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Lectura, redacción y argumentación jurídica</b></h3>
<h4 class="product-price invisible">$1800.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Lectura_redaccion'">INFO</button>
</div>

</div>
</div>
<!-- Product 9-->

</div>
</div>
<!-- Body-->
</div>
</div>
</div>
</div>