<!-- CUADRO 1-->
<div class="col-md-10 col-sm-10 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 INTRODUCCIÓN A LA LECTURA Y ESCRITURA.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">1.1 Aprender a pensar leyendo bien.</b>
<br><br>
<b class="ml-2">1.2 La escritura y el uso de la caligrafía para mejorar hábitos de escritura.</b>
<br><br>
<b class="ml-2">1.3 Lectura y escritura aplicadas.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 1-->

<!-- CUADRO 2-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">2.0 ORTOGRAFÍA Y REDACCIÓN</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">2.1 Nociones generales de ortografía.</b>
<br><br>
<b class="ml-2">2.2 Generalidades de redacción y la importancia de su aplicación en el ámbito jurídico.</b>
<br><br>
<b class="ml-2">2.3 Ortografía y redacción aplicadas.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 2-->

<!-- CUADRO 3-->
<div class="col-md-10 col-sm-10 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">3.0 ARGUMENTACIÓN JURÍDICA 
</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<b class="ml-2">3.1 Argumentación y deontología jurídica.</b>
<br><br>
<b class="ml-2">3.2 Enseñanza y análisis del derecho.</b>
<br><br>
<b class="ml-2">3.3 Investigación y análisis jurídico.</b>
<br><br>
<b class="ml-2">3.4 Aprendizaje de lenguaje y criterio jurídico.</b>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- CUADRO 3-->

