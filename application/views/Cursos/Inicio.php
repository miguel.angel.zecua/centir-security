<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #EAFF4A;">
<div class="container">
<div class="column">

<h1>Cursos y Diplomados</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li>Cursos y Diplomados</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view active" href="<?=base_url('Cursos')?>"><span></span><span></span><span></span></a>
<a class="list-view" href="<?=base_url('Cursos/List')?>"><span></span><span></span><span></span></a></div>
</div>

</div>


<!-- Categoria 1-->



<div class="row justify-content-center" style="margin-left:-3.5rem; margin-right:-3.5rem; background-color:#FCF3CF ; border-radius:30px; margin-top: -21px !important; margin-bottom: 15px !important; ">

<div id="columnas" class="col-6">

<!-- Related Products Carousel-->


<h3 class="text-center  mt-4 padding-bottom-1x text-center text-dark">Cursos</h3>

<!-- Carousel-->
<div class=" row justify-content-center" >

<!-- CLASE 1-->

<div class="col-sm-3 mt-2">
	
<!-- Product 10-->
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Cursos/Lectoescritura_sensorial')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Lectoescritura_sensorial-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Lectoescritura_sensorial'">INFO</button>
</div>

</div>
</div>
<!-- Product 10-->	
</div>


<div class="col-sm-3 mt-2">
<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Investigacion_criminal')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Criminal-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$1800.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Investigacion_criminal'">INFO</button>
</div>

</div>
</div>
<!-- Product 3 -->	

</div>


<!-- CLASE 1-->
<div class="col-sm-3 mt-2">
<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Mercados_financieros')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Mercados_financieros.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$1800.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Mercados_financieros'">INFO</button>
</div>

</div>
</div>
<!-- Product 3 -->	

</div>


<!-- CLASE 1-->
<div class="col-sm-3 mt-2">
<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Plantas_medicinales')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Plantas_medicinales.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$0</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Plantas_medicinales'">INFO</button>
</div>

</div>
</div>
<!-- Product 3 -->	

</div>


<!-- CLASE 2-->


<!-- CLASE 2-->

<!-- CLASE 3-->

<!-- CLASE 3-->

</div>
<div class="text-center mt-4 ">

</div>
</div>
</div>
<br>
<!-- Categoria 1-->

<!-- Categoria 2-->

<div class="row justify-content-center" style="margin-left:-3.5rem; margin-right:-3.5rem; background-color:#F2D7D5; border-radius:30px; margin-top: -21px !important;">

<div id="columnas" class="col-6">

<!-- Related Products Carousel-->


<h3 class="text-center  mt-4 padding-bottom-1x text-center text-dark">Diplomados</h3>

<!-- Carousel-->
<div class=" row justify-content-center" >

<!-- CLASE 1-->
<!-- Product 1 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2 d-none">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/Acupuntura')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Acupuntura-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Acupuntura</b></h3>
<h4 class="product-price invisible">$1500.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/Acupuntura'">Añadir</button>
</div>

</div>
</div>
<!-- Product 1-->


</div>
<!-- CLASE 1-->

<!-- CLASE 2-->
<!-- Product 2 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/S_penal_acusatorio')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/S_Acusatorio_penal.png')?>" alt="Product"></a>
<h3 class="product-title" style="margin-bottom: -10px;"><b>Sistema penal acusatorio y sus etapas procesales</b></h3>
<h4 class="product-price invisible">$000.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/S_penal_acusatorio'">Añadir</button>
</div>

</div>
</div>
<!-- Product 2-->
</div>
<!-- CLASE 2-->


<!-- CLASE 3-->
<!-- Product 3 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/Biomagnetismo')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Biomagnetismo.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Biomagnetismo</b></h3>
<h4 class="product-price invisible">$1300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/Biomagnetismo'">Añadir</button>
</div>

</div>
</div>
<!-- Product 3-->
</div>
<!-- CLASE 3-->


<!-- CLASE 4-->
<!-- Product 4 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/Masajes_Terapeuticos')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Masajes_Terapeuticos.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Masajes Terapeúticos</b></h3>
<h4 class="product-price invisible">$1300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/Masajes_Terapeuticos'">Añadir</button>
</div>

</div>
</div>
<!-- Product 4-->
</div>
<!-- CLASE 4-->


<!-- CLASE 5-->
<!-- Product 5 -->
<div class="col-sm-4 col-md-3 col-lg-3 col-xs-3 mt-2">
<div class="grid-item">
<div class="product-card" >

<a class="product-thumb" href="<?=base_url('Diplomados/Acupuntura_Terapias_Alternativas')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Acupuntura.png')?>" alt="Product"></a>
<h3 class="product-title" style="margin-bottom: -10px;"><b>Acupuntura y Terapias Alternativas</b></h3>
<h4 class="product-price invisible">$1300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Diplomados/Acupuntura_Terapias_Alternativas'">Añadir</button>
</div>

</div>
</div>
<!-- Product 5-->
</div>
<!-- CLASE 5-->




</div>
<div class="text-center mt-4 ">

</div>
</div>
</div>
<br>
<!-- Categoria 2-->



</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

