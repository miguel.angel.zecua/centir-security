<div class="accordion" id="div4" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#collapseFive" data-toggle="collapse" style="font-size: 16px"><b>Forenses</b></a></h6>
</div>

<div class="collapse" id="collapseFive" data-parent="#div4" role="tabpanel">
<div class="card-body">

<!-- Body-->

<div class="row justify-content-center">

<div class="col-md-3 col-sm-4 d-none">
<!-- Product 5-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Balistica_Forense')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Balistica_Forense-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Balística Forense e identificación de Armas de Fuego</b></h3>
<h4 class="product-price invisible">$700.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Balistica_Forense'">INFO</button>
</div>

</div>
</div>
<!-- Product 5-->
</div>

<div class="col-md-3 col-sm-4 d-none">
<!-- Product 6-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Introduccion_Estudio')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Grafoscopia_Documentoscopia-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Introducción Al Estudio De La Grafoscopía Y Documentoscopía</b></h3>
<h4 class="product-price invisible">$1800.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Introduccion_Estudio'">INFO</button>
</div>

</div>
</div>
<!-- Product 6-->
</div>

<div class="col-md-3 col-sm-4">
<!-- Product 7-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Investigacion_criminal')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Criminal-min.png')?>" alt="Product"></a>

<h4 class="product-price invisible">$1800.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Investigacion_criminal'">INFO</button>
</div>

</div>
</div>
<!-- Product 7-->
</div>

<div class="col-md-3 col-sm-4 d-none">
<!-- Product 8-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Cursos/Introduccion_dactiloscopia')?>">
<img src="<?=base_url('library/img/Categorias/Cursos/Dactiloscopia-min.png')?>" alt="Product"></a>
<h3 class="product-title"><b>Introducción a la dactiloscopía</b><br></h3>
<br>
<h4 class="product-price invisible">$1800.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Introduccion_dactiloscopia'">INFO</button>
</div>

</div>
</div>
<!-- Product 8-->
</div>


</div>
<!-- Body-->
</div>
</div>
</div>
</div>