<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">

<div class="column">
<h1>Mi Perfil</h1>
</div>


</div>
</div>

<!-- Contenedor Principal-->
<div class="container padding-bottom-3x mb-2">

<div class="row">
<div class="col-lg-4">
<aside class="user-info-wrapper">
<div class="user-cover" style="background-image: url(<?=base_url('library/img/Logo/Principal2.png')?>);">
</div>

<div class="user-info">
<div class="user-avatar"><a class="edit-avatar" href="#"></a>
<img src="<?=base_url('library/img/Logo/1.png')?>" alt="User"></div>
<div class="user-data">
<h4><?php echo $row->Nombre_Usuario; ?></h4>
</div>
</div>
</aside>

<nav class="list-group">

<a class="list-group-item active" href="<?=base_url('User/Perfil')?>">
<i class="icon-head"></i>Perfil</a>	

<a class="list-group-item with-badge" href="<?=base_url('User/Cursos')?>">
<i class="icon-map"></i>Cursos<span class="badge badge-primary badge-pill">0</span></a>
<a class="list-group-item with-badge" href="<?=base_url('User/Diplomados')?>">
<i class="icon-map"></i>Diplomados<span class="badge badge-primary badge-pill">0</span></a>

<!-- Contenedor Principal
<a class="list-group-item with-badge" href="account-orders.html">
<i class="icon-bag"></i>Ordenes<span class="badge badge-primary badge-pill">0</span></a>


<a class="list-group-item with-badge" href="account-wishlist.html">
<i class="icon-heart"></i>Lista de deseos<span class="badge badge-primary badge-pill">0</span></a>
-->
</nav>

</div>

<div class="col-lg-8">
<div class="padding-top-2x mt-2 hidden-lg-up"></div>

<form class="row">

<div class="col-md-12 text-center">
<div class="alert alert-primary" role="alert" style="padding: 10px;">Id del Usuario: <strong>#<?php echo $row->Id_Usuario; ?></strong>

</div>
</div>

<div class="col-md-6 mt-4">
<div class="form-group">
<label for="account-fn">Nombre</label>
<input class="form-control" type="text" id="account-fn" value="<?php echo $row->Nombre_Usuario; ?>" required="">
</div>
</div>

<div class="col-md-6 mt-4">
<div class="form-group">
<label for="account-ln">Apellidos</label>
<input class="form-control" type="text" id="account-ln" value="<?php echo $row->Apellidos; ?>" required="">
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<label for="account-email">Correo Electrónico</label>
<input class="form-control" type="email" id="account-email" value="<?php echo $row->Correo; ?>" disabled="">
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<label for="account-phone">Número de Teléfono </label>
<input class="form-control" type="text" id="account-phone" value="<?php echo $row->Telefono; ?>" required="">
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<label for="account-phone">Fecha de Nacimiento</label>
<input class="form-control" type="date" id="account-phone" value="+7(805) 348 95 72" required="">
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<label for="account-phone">Edad</label>
<input class="form-control" type="number" id="account-phone" value="+7(805) 348 95 72" required="">
</div>
</div>

<div class="col-md-10">
<div class="form-group">
<label for="account-pass">Contraseña Actual</label>
<input class="form-control" type="password" id="account-pass" value="<?php echo $row->Contraseña; ?>">
</div>
</div>

<div class="col-md-2">
<div class="form-group">
<button id="show_password" class="btn btn-secondary margin-top-1x" type="button" onclick="mostrarPassword()" style="margin-top:30px !important; height:42px;"> 
Mostrar</a> 
</button>
</div>
</div>




<div class="col-12">
<hr class="mt-2 mb-3">

<div class="d-flex flex-wrap justify-content-between align-items-center">
<div class="custom-control custom-checkbox d-block">
</div>

<button class="btn btn-primary margin-right-none" type="button" data-toast="" data-toast-position="topRight" data-toast-type="success" data-toast-icon="icon-circle-check" data-toast-title="Success!" data-toast-message="Your profile updated successfuly.">Actualizar Perfil</button>

</div>
</div>

</form>
</div>
</div>
</div>

<!-- Contenedor Principal-->

<!-- Script-->
<script type="text/javascript">
function mostrarPassword(){
		var cambio = document.getElementById("account-pass");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
	$(document).ready(function () {
	//CheckBox mostrar contraseña
	$('#ShowPassword').click(function () {
		$('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
		console.log($('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password'));
	});
});
</script>

<!-- Script-->

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

