<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">

<div class="column">
<h1>Mi Perfil</h1>
</div>



</div>
</div>

<!-- Contenedor Principal-->
<div class="container padding-bottom-3x mb-2">

<div class="row">
<div class="col-lg-12">
<aside class="user-info-wrapper">
<div class="user-cover" style="background-image: url(<?=base_url('library/img/Logo/Principal2.png')?>);">
</div>

<div class="user-info d-flex justify-content-center">
<div class="user-avatar "><a class="edit-avatar" href="#"></a>
<img src="<?=base_url('library/img/Logo/1.png')?>" alt="User"></div>
<div class="user-data">
<h4></h4><span></span>
</div>
</div>
</aside>

<nav class="list-group">

<a class="list-group-item " href="<?=base_url('User/Perfil')?>">
<i class="icon-head"></i>Perfil</a>	

<a class="list-group-item with-badge active" href="<?=base_url('User/Cursos')?>">
<i class="icon-map"></i>Diplomados<span class="badge badge-primary badge-pill">0</span></a>

<!-- Contenedor Principal
<a class="list-group-item with-badge" href="account-orders.html">
<i class="icon-bag"></i>Ordenes<span class="badge badge-primary badge-pill">0</span></a>


<a class="list-group-item with-badge" href="account-wishlist.html">
<i class="icon-heart"></i>Lista de deseos<span class="badge badge-primary badge-pill">0</span></a>
-->
</nav>

</div>

</div>

<div class="row margin-top-2x">


  <?php foreach($tests as $test) { ?>



<!-- Podcast 6 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?php echo $test->imagen;?>" alt="Card image">

<div class="card-body text-center">
<h4 class="card-title text-center"><?php echo $test->nombre;?></h4>

<a class="btn btn-primary btn-sm align-items-center" href="<?php echo $test->Url;?>">Ver</a>
</div>

</div>
</div>
<!-- Podcast 6 -->


 <?php 


} ?>

</div>

</div>
</div>

<!-- Contenedor Principal-->

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>
<script src="<?=base_url('library/js/Especials/Logo.js')?>"></script>
</body>
</html>

