
<div class="row justify-content-center">

<div class="col-lg-12 col-md-12 order-md- mt-2">
<hr class="margin-top-1x">
<h6 class=" text-normal text-uppercase text-center mt-3 mb-3">Nuestro Equipo</h6>
<hr class="margin-bottom-1x">

<div class="row">

<div class="col-sm-4 text-center mb-4">
<img class="d-block w-150 mx-auto img-thumbnail rounded-circle mb-2" src="<?=base_url('library/img/Icons-Item/Alternativas.png')?>" alt="Team">
<h6>Nombre 1</h6>
<p class="text-muted mb-2">CEO</p>
</div>

<div class="col-sm-4 text-center mb-4">
<img class="d-block w-150 mx-auto img-thumbnail rounded-circle mb-2" src="<?=base_url('library/img/Icons-Item/Biomagnetismo.png')?>" alt="Team">
<h6>Nombre 2</h6>
<p class="text-muted mb-2">CEO</p>
</div>

<div class="col-sm-4 text-center mb-4">
<img class="d-block w-150 mx-auto img-thumbnail rounded-circle mb-2" src="<?=base_url('library/img/Icons-Item/Psicologicas.png')?>" alt="Team">
<h6>Nombre 3</h6>
<p class="text-muted mb-2">Ceo</p>
</div>

</div>
</div>

</div>