<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column">

<h1>Conferencias</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li>Consultas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view " href="<?=base_url('Consultas')?>"><span></span><span></span><span></span></a>
<a class="list-view active" href="<?=base_url('Consultas/List')?>"><span></span><span></span><span></span></a></div>
</div>

</div>

<!-- Fila-->
<div class="row justify-content-center">

<div class="col-lg-11">


<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Consultas/Individual')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Individual-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Individual</h3>

<h4 class="product-price">$300.00</h4>

<p class="hidden-xs-down">Es el tratamiento en donde una persona (el paciente) establece una «Relación profesional» con otra persona (el terapeuta) el cual le ayudará a diagnosticar trastornos psicológicos, emocionales, de conducta, promover el crecimiento personal, remover, modificar y prevenir síntomas de malestar ya existentes. Y con base en esto, desarrollar tratamientos y planes terapéuticos, tomando en cuenta las observaciones realizadas y las necesidades del paciente.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Individual'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Consultas/Familiar')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Familiar-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Familiar</h3>

<h4 class="product-price">$450.00</h4>
<p class="hidden-xs-down">Es un tratamiento que se brinda a todos los miembros de una familia cuando el objetivo de mejorar la comunicación y resolver conflictos. Posibilitar que los miembros usen sus recursos de apoyo puede ser vital para ayudarles a gestionar las fases del desarrollo familiar o los acontecimientos vitales estresantes tales como una enfermedad grave o el fallecimiento de uno de sus miembros.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Familiar'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Consultas/Pareja')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Pareja-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">De pareja</h3>

<h4 class="product-price">$350.00</h4>
<p class="hidden-xs-down">Es una forma de terapia que busca mejorar la comunicación y resolver problemas dentro de una relación íntima. En contraste con el asesoramiento de los problemas de relación, que pueden ser aplicadas exclusivamente a través de sesiones individuales, terapia de pareja es un término aplicado a la psicoterapia para dos personas dentro de una relación. </p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Pareja'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Consultas/Infantil')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Infantil-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Infantil</h3>

<h4 class="product-price">$250.00</h4>
<p class="hidden-xs-down">Es un conjunto de técnicas y métodos usados para poder ayudar a niños y niñas que tienen problemas con sus emociones o conductas, y uno de los muchos elementos que la diferencian de la terapia para adultos es que utiliza el juego como elemento clave en la terapia.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Infantil'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Consultas/Adicciones')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Adicciones-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Adicciones</h3>

<h4 class="product-price">$300.00</h4>
<p class="hidden-xs-down">Este tratamiento consiste en buscar el equilibrio y la recuperación completa del paciente tratando el problema desde el origen. Tiene como finalidad ayudar al adicto a dejar la búsqueda y el consumo compulsivos de la droga.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Adicciones'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Consultas/Tanatologia')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Tanatologia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Tanatología</h3>

<h4 class="product-price">$300.00</h4>
<p class="hidden-xs-down">Es una disciplina que se encarga de proporcionar un acompañamiento emocional para trabajar el proceso de duelo, principalmente de una muerte. También se encarga de los duelos derivados de pérdidas significativas que no tengan que ver con la muerte física o enfermos terminales.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Tanatologia'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

</div>
       
</div>
<!-- Fila-->

</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

