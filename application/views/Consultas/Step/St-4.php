<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
<script src="<?=base_url('library/js/Consultas/sweetalert.js')?>"></script>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #18A2FF;">
<div class="container">
<div class="column">

<h1 style="color: white;">Consultas Psicológicas</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?= base_url('Home') ?>" style="color: white;">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li style="color: white;">Consultas Psicológicas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->


<!-- CUADRO 1-->
<div class="row">
<!-- Checkout Adress-->
<div class="col-xl-12 col-lg-12">

<!-- Direcciones-->
<div class="checkout-steps"><a class="active" href="checkout-review.html">4. Resumen</a><a class="completed" href="<?=base_url('Consultas/St3')?>"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>3. Datos de la consulta</a><a class="completed" href="<?=base_url('Consultas/St2')?>"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>2. Datos Personales</a><a class="completed" href="<?=base_url('Consultas/Inicio')?>"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>1. Consulta</a></div>         
<!-- Direcciones-->


<h4>Revise los datos para su consulta</h4>
<hr class="padding-bottom-1x">
<form id="formulario_principal">

<div class="row" id="fila_1">

<div class="col-sm-4 d-none">
<div class="form-group">

<label for="size" style="font-size: 17px; font-weight: bold;">Consulta</label>
<input type="text" class="form-control" id="consulta" name="consulta" style="font-size: 18px; color: red;" readonly>

</div>
</div>

<div class="col-sm-4">
<div class="form-group">

<label for="size" style="font-size: 17px; font-weight: bold;">Consulta</label>
<input type="text" class="form-control" id="consulta_name" name="consulta_name" style="font-size: 18px; color: red;" readonly>

</div>
</div>

<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Nombre</label>
<input type="text" class="form-control" id="nombre" style="font-size: 18px; color: red;" name="nombre" readonly>
</div>
</div>

<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Edad</label>
<input type="text" class="form-control" name="edad" style="font-size: 18px; color: red;" id="edad" readonly>
</div>
</div>

</div>


<hr>


<div class="row">

<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Correo</label>
<input type="text" class="form-control" name="correo" id="correo" style="font-size: 18px; color: red;" readonly>
</div>
</div>


<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Telefono</label>
<input type="text" class="form-control" name="telefono" id="telefono" style="font-size: 18px; color: red;" readonly>
</div>
</div>

<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Modalidad</label>
<input type="text" class="form-control" id="modalidad" name="modalidad" style="font-size: 18px; color: red;" readonly>
</div>
</div>

</div>


<hr>

<div class="row">

<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Sexo del Psicólogo</label>
<input type="text" class="form-control" id="sexo" name="sexo" style="font-size: 18px; color: red;" readonly>
</div>
</div>


<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Fecha</label>
<input type="text" class="form-control" id="fecha" name="fecha" style="font-size: 18px; color: red;" readonly>
</div>
</div>

<div class="col-sm-4">
<div class="form-group">
<label for="color" style="font-size: 17px; font-weight: bold;">Horario</label>
<input type="text" class="form-control" name="horario" id="horario" style="font-size: 18px; color: red;" readonly>
</div>
</div>


</div>

<hr>


<div class="checkout-footer margin-top-1x">

<div class="column"><a class="btn btn-outline-secondary" href="<?=base_url('Consultas/St3')?>"><i class="icon-arrow-left"></i><span class="hidden-xs-down">&nbsp;Regresar</span></a></div>

<div class="column"><a class="btn btn-primary disable_link" id="enviar"><span class="hidden-xs-down">Siguiente&nbsp;</span><i class="icon-arrow-right"></i></a></div>
</form>

</div>
</div>

<!-- Sidebar-->

</div>
<!-- CUADRO 1-->



</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>



<script type="text/javascript">
	
$("#nombre").val(sessionStorage.getItem('Nombre_main'));
$("#edad").val(sessionStorage.getItem('Edad_main'));
$("#correo").val(sessionStorage.getItem('Correo_main'));

$("#telefono").val(sessionStorage.getItem('Telefono_main'));
$("#modalidad").val(sessionStorage.getItem('Modalidad_main'));
$("#sexo").val(sessionStorage.getItem('Sexo_main'));

$("#fecha").val(sessionStorage.getItem('Fecha_main'));
$("#horario").val(sessionStorage.getItem('Horario_main'));
$("#consulta_name").val(sessionStorage.getItem('Consulta'));


switch(sessionStorage.getItem('Consulta'))
{
case "Individual":
$("#consulta").val(1);
break;
case "Familiar":
$("#consulta").val(2);
break;
case "De pareja":
$("#consulta").val(3);
break;
case "Infantil":
$("#consulta").val(4);
break;
case "Adicciones":
$("#consulta").val(5);
break;
case "Tanatologia":
$("#consulta").val(6);
break;
default:
$("#consulta").val(1);
}


$("#enviar").on("click", function(){


Swal.fire({
title: '¿Desea guardar su cita?',
text: "",
icon: 'question',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'SI'
}).then((result) => {
if (result.isConfirmed) {

$.ajax({
type: "POST", 
url: "<?php echo site_url('Consultas/create')?>",
data: $('#formulario_principal').serialize(),
success: function (data) 
{
sessionStorage.clear()	
Swal.fire(
'Hecho!',
'Cita guardada!',
'success'
)
window.location.href = 'Inicio';
} 
});


}
})


});

</script>


<script type="text/javascript">
$( document ).ready(function() 
{

if($("#nombre").val() =='' || $("#edad").val() =='' || $("#correo").val() =='' || $("#telefono").val() =='' || $("#modalidad").val() == '' || $("#sexo").val() == '' || $("#fecha").val() =='' || $("#horario").val() =='' || $("#consulta_name").val()=='')
{

}

else
{
$("#enviar").removeClass("disable_link");	
}
});


</script>

<style type="text/css">
.disable_link
{
pointer-events: none; 
display: inline-block;
}	
</style>

</body>
</html>
