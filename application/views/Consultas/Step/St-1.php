<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
<script src="<?=base_url('library/js/jQuery.js')?>"></script>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #18A2FF;">
<div class="container">
<div class="column">

<h1 style="color: white;">Terapias Psicológicas</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?= base_url('Home') ?>" style="color: white;">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li style="color: white;">Terapias Psicológicas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->

<!-- CUADRO 1-->
<div class="row">
<!-- Checkout Adress-->
<div class="col-xl-12 col-lg-12">

<!-- Direcciones-->
<div class="checkout-steps">
<a class="disable_link" id="resumen" href="<?=base_url('Consultas/St4')?>">4. Resumen</a>
<a class="disable_link" id="datos_consulta" href="<?=base_url('Consultas/St3')?>"><span class="angle"></span>3. Datos de la consulta</a>

<a class="disable_link" id="datos_personales" href="<?=base_url('Consultas/St2')?>"><span class="angle disable-links"></span>2. Datos Personales</a>

<a class="active" href="<?=base_url('Consultas/Inicio')?>"><span class="angle"></span>1. Consulta</a>
</div>
<!-- Direcciones-->

<h4>Tipo de consulta:</h4>

<hr class="padding-bottom-1x">


<div class="row mt-2">
<div class="col-12">
<form>
<div class="form-group row">
<div class="col-12">
<div class="card-deck">

<!-- ///////////////////////////////////////////// -->
<div id="elephant-card" class="card mb-4">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="elephant" name="consulta" type="radio" value="Individual" class="d-none">
<label for="elephant" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Individual</span></label>
</h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$350 X Sesión</span>
</div>
<p class="card-text text-center">Ayudará a diagnosticar trastornos psicológicos, emocionales, de conducta, promover el crecimiento personal, remover, modificar y prevenir síntomas de malestar ya existentes.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////// -->
<div id="lion-card" class="card mb-4">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="lion" name="consulta" type="radio" value="Familiar" class="d-none"> 
<label for="lion" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Familiar</span></label>
</h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; font-weight: 400;">$450 X Sesión
</div>
<p class="card-text text-center">Tratamiento que se brinda a todos los miembros de una familia cuando el objetivo de mejorar la comunicación y resolver conflictos.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->


<!-- ///////////////////////////////////////////// -->
<div id="zebra-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="zebra" name="consulta" type="radio" value="De pareja" class="d-none"> 
<label for="zebra" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">De pareja</span></label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$350 X Sesión</span>
</div>
<p class="card-text text-center">Terapia que busca mejorar la comunicación y resolver problemas dentro de una relación íntima.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->
<div class="w-100"></div>


<!-- ///////////////////////////////////////////// -->
<div id="giraffe-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="giraffe" name="consulta" type="radio" value="Infantil" class="d-none"> 
<label for="giraffe" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Infantil</span></label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$250 X Sesión</span>
</div>
<p class="card-text text-center">Conjunto de técnicas y métodos usados para poder ayudar a niños y niñas que tienen problemas con sus emociones o conductas, que utiliza el juego como elemento clave en la terapia.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->


<!-- ///////////////////////////////////////////// -->
<div id="hyena-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="hyena" name="consulta" type="radio" value="Adicciones" class="d-none"> 
<label for="hyena" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Adicciones</span></label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$300 X Sesión</span>
</div>
<p class="card-text text-center">Tratamiento que consiste en buscar el equilibrio y la recuperación completa del paciente tratando el problema desde el origen. Tiene como finalidad ayudar al adicto a dejar la búsqueda y el consumo compulsivos de la droga.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////// -->
<div id="meerkat-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="meerkat"name="consulta"  type="radio" value="Tanatologia" class="d-none"> 
<label for="meerkat" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Tanatología</span>
</label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$300 X Sesión</span>
</div>
<p class="card-text">Disciplina que se encarga de proporcionar un acompañamiento emocional para trabajar el proceso de duelo, principalmente de una muerte. También se encarga de los duelos derivados de pérdidas significativas</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->



</div>
</div>
</div>
</form>
</div>
</div>


<div class="checkout-footer">
<div class="column"><a class="btn btn-primary invisible" id="boton_siguiente" href="<?=base_url('Consultas/St2')?>"><span class="hidden-xs-down">Siguiente&nbsp;</span><i class="icon-arrow-right"></i></a></div>
</div>
</div>
<!-- Sidebar          -->

</div>
<!-- CUADRO 1-->



</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>


<style type="text/css">

.active {
border-color: #18A2FF !important;
border-width: 4px;
background-color: white;
box-shadow: 0 3px 6px rgba(0, 0, 0, 0.18), 0 3px 6px rgba(0, 0, 0, 0.23);
}

.card {
box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15), 0 2px 5px rgba(0, 0, 0, 0.2);
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
}


</style>

<script type="text/javascript">

$(document).ready(function () {
$("#Tipo_consulta").text(sessionStorage.getItem('Consulta'));
$('input:radio').change(function () {//Clicking input radio
var radioClicked = $(this).attr('id');
unclickRadio();
removeActive();
clickRadio(radioClicked);
makeActive(radioClicked);
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
});
$(".card").click(function () {//Clicking the card
var inputElement = $(this).find('input[type=radio]').attr('id');
unclickRadio();
removeActive();
makeActive(inputElement);
clickRadio(inputElement);
var consulta = $("input[name='consulta']:checked").val();
sessionStorage.setItem('Consulta',consulta);
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
});
});


function unclickRadio() {
$("input:radio").attr("checked", false);
}

function clickRadio(inputElement) {
$("#" + inputElement).attr("checked", true);
}

function removeActive() {
$(".card").removeClass("active");
}

function makeActive(element) {
$("#" + element + "-card").addClass("active");
}

</script>


<style type="text/css">

.card:hover {
border-color: transparent;
transition: 0.5s; 
border-width: 5px;
}
</style>


<script type="text/javascript">
  
$(document).ready(function () 
{
var valor = sessionStorage.getItem('Consulta');
if(valor != '')
{

switch(valor)
{
case 'Individual':
$("#elephant-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Familiar':
$("#lion-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'De pareja':
$("#zebra-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Infantil':
$("#giraffe-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Adicciones':
$("#hyena-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("invisible");
break;
case 'Tanatologia':
$("#meerkat-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
}

}

});

</script>

<style type="text/css">
	
.disable_link
{
pointer-events: none; 
display: inline-block;
}	
</style>



</body>
</html>

