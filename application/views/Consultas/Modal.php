<div class="modal fade" id="modal" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">

<div class="modal-header">
<h4 class="modal-title"> Consulta Individual</h4>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
</div>

<div class="modal-body">
<!-- BODY-->
<div class="alert alert-danger rounded text-center text-dark" role="alert">
Revise que sus datos sean correctos  
</div>
<form method="post" action="<?php echo site_url('Consultas/create')?>">
<div class="form-group row">

<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Nombre</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="nombre_2" id="nombre_2" required>
</div>

<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Edad</label>
<div class="col-sm-10 mt-3">
<input type="number" class="form-control" name="edad_2" id="edad_2" required>
</div>


<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Modalidad</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="modalidad_2" id="modalidad_2" required>
</div>


<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Correo</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="correo_2" id="correo_2" required>
</div>


<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Telefono</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="telefono_2" id="telefono_2" required>
</div>


<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Sexo Psicologo</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="sexo_2" id="sexo_2" required>
</div>


<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Fecha</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="fecha_2" id="fecha_2" required>
</div>


<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Horario</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="horario_2" id="horario_2" required>
</div>

<input type="text" class="form-control d-none" name="id_consulta" value="1">


</div>
<!-- BODY-->

</div>
<div class="modal-footer">
<button class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Editar</button>
<button class="btn btn-primary btn-sm " data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" type="submit" value="save">Confirmar</button>
</div>
</form>
</div>
</div>
</div>
</div>