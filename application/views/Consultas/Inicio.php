<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>



<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column">


<h1>Consultas</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li>Consultas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view active" href="<?=base_url('Consultas')?>"><span></span><span></span><span></span></a>
<a class="list-view" href="<?=base_url('Consultas/List')?>"><span></span><span></span><span></span></a></div>
</div>

</div>

<!-- Products Grid 1-->

<div class="isotope-grid cols-4 mb-0">
<div class="gutter-sizer"></div>
<div class="grid-sizer"></div>

<!-- Product 1-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Individual')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Individual-min.png')?>" alt="Product"></a>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Individual'">Agendar</button>
</div>

</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Familiar')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Familiar-min.png')?>" alt="Product"></a>
<h4 class="product-price">$450.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Familiar'">Agendar</button>
</div>

</div>
</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Pareja')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Pareja-min.png')?>" alt="Product"></a>
<h4 class="product-price">$350.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Pareja'">Agendar</button>
</div>

</div>
</div>
<!-- Product 3-->

<!-- Product 4-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Infantil')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Infantil-min.png')?>" alt="Product"></a>
<h4 class="product-price">$250.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Infantil'">Agendar</button>
</div>

</div>
</div>
<!-- Product 4-->

<!-- Product 5-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Adicciones')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Adicciones-min.png')?>" alt="Product"></a>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Adicciones'">Agendar</button>
</div>

</div>
</div>
<!-- Product 5-->

<!-- Product 6-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Consultas/Tanatologia')?>">
<img src="<?=base_url('library/img/Categorias/Consultas/Tanatologia-min.png')?>" alt="Product"></a>
<h4 class="product-price">$300.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Consultas/Tanatologia'">Agendar</button>
</div>

</div>
</div>
<!-- Product 6-->

</div>
<!-- Products Grid 1-->

</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

<div class="iziToast-wrapper iziToast-wrapper-topRight"></div>

</body>
</html>

