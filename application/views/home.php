<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
<meta name="description" content="En CenTI-R somos un centro especializado en diversas terapias alternativas y psicológicas dónde trabajamos en conjunto la parte física, emocional, y mental, para llegar a la sanación integral de tu ser.">


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css"/>

</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Modal Inicial-->
<div class="modal fade" tabindex="-1" id="myModal">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header" style="border-bottom: none; margin-bottom: -25px;">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>

<div class="modal-body">
<div class="row justify-content-center">
	
 <div class="col-12 col-sm-12 mb-3 mt-0">
<span class="badge badge-pill badge-info" style="width: 100%; color: black; font-size: 19px;">¡Próximamente!</span>
 </div>	


<div class="col-6 col-sm-6 mb-5">
<a href="<?=base_url('Diplomados/Acupuntura_Terapias_Alternativas')?>" data-size="1000x667">
<img class="new" style="border-radius: 25px;" src="<?=base_url('library/img/Categorias/Diplomados/Acupuntura.png')?>" alt="Diplomado Acupuntura Centi-r"></a>
</div>

<div class="col-6 col-sm-6 mb-5">
<a href="<?=base_url('Diplomados/Masajes_Terapeuticos')?>" data-size="1000x667">
<img class="new" style="border-radius: 25px;" src="<?=base_url('library/img/Categorias/Diplomados/Masajes_Terapeuticos.png')?>" alt="Diplomado Acupuntura Centi-r"></a>
</div>


</div>
</div>

</div>
</div>
</div>
<!-- Modal Inicial-->





<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->

<!-- Main Slider-->
<?=$this->load->view('home/slider','',TRUE);?>
<!-- Main Slider-->


<!-- Top Categories-->
<?=$this->load->view('home/categories','',TRUE);?>
<!-- Top Categories-->



<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<a class="bottom-whatts"  id="botonwhatss" href="https://api.whatsapp.com/send?phone=+522462486999" class="btn-wsp" target="_blank"> <i class="fab fa-whatsapp fa-lg" style="color: green; vertical-align: middle;" id="logowhatts"></i> 

</a>
<!-- Back To Top Button-->
<script src="<?=base_url('library/js/jQuery.js')?>"></script>
<?=$this->load->view('include/js','',TRUE);?>
<script src="<?=base_url('library/js/events_home.js')?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

<script type="text/javascript">
setTimeout(function(){
//$('#myModal').modal('show');
}, 2000);
</script>


<style type="text/css">
	
.new {

  border:2px solid #fff;
  -moz-box-shadow: 10px 10px 5px #ccc;
  -webkit-box-shadow: 10px 10px 5px #ccc;
  box-shadow: 10px 10px 5px #ccc;
  -moz-border-radius:25px;
  -webkit-border-radius:25px;
  border-radius:25px;
}


  .carousel-item {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  
  .carousel-item img {
    width: 100%;
    height: auto;
    max-height: 300px;
  }
</style>
  <script>
    $(document).ready(function() {
      $('.carousel').slick({
        autoplay: true,
        autoplaySpeed: 1000,
        dots: true,
        arrows: true
      });
    });
  </script>


</body>
</html>

