<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column">

<h1>Diplomados</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li>Diplomados</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view " href="<?=base_url('Diplomados')?>"><span></span><span></span><span></span></a>
<a class="list-view active" href="<?=base_url('Consultas/List')?>"><span></span><span></span><span></span></a></div>
</div>

</div>

<!-- Fila-->
<div class="row justify-content-center">

<div class="col-lg-11">


<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Diplomados/Acupuntura')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Acupuntura-min.png')?>" alt="Product"></a>
<div class="product-info">Acupuntura</h3>

<h4 class="product-price">$1500.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Acupuntura'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Diplomados/Biomagnetismo')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/Biomagnetismo.png')?>" alt="Product"></a>
<div class="product-info">Biomagnetismo</h3>

<h4 class="product-price">$1300.00</h4>

<p class="hidden-xs-down">El alumno aprendera las tecnicas del uso de campos 
magneticos para equilibrar la energia a nivel fisico, emocional y mental, asi 
como llegar al homeostasis del cuerpo regulando el PH.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Biomagnetismo'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Diplomados/S_penal_acusatorio')?>">
<img src="<?=base_url('library/img/Categorias/Diplomados/S_Acusatorio_penal.png')?>" alt="Product"></a>
<div class="product-info">Sistema penal acusatorio y sus etapas procesales</h3>

<h4 class="product-price">$1375.00</h4>

<p class="hidden-xs-down">Cubrir las necesidades básicas de conocimiento técnico y científico en las ciencias forenses y su aplicación en la justicia.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='S_penal_acusatorio'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->





</div>
       
</div>
<!-- Fila-->

</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

