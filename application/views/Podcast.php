<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column text-center">

<h1 class="text-dark">Podcast</h1>
</div>

</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->


<div class="row">

<!-- Podcast 1 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_1.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Terapia de Imanes</h4>
<p class="card-text">
¿Sabías que el cuerpo es un conjunto de energía y que a veces de desequilibra y por eso podemos enfermarnos? 🤯. La Terapia de Imanes consiste en el reconocimiento de puntos de energía alterados en nuestro organismo que en conjunto están dando origen a una enfermedad o malestar de curso agudo o crónico 🤕.</p>

<br>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/0cncqm1tOrPmUkTKMJRIVf?si=iyL3eMeHQjOApM2G2gyvbQ&utm_source=whatsapp">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 1 -->

<!-- Podcast 2 -->
<div class="col-sm-4  margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_2.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Pensamiento lógico matemático</h4>
<p class="card-text">
El pensamiento lógico matemático es aquel que surge a partir de las experiencias directas y desarrolla la capacidad de comprender los conceptos abstractos a través de los números, formas gráficas, ecuaciones, fórmulas matemáticas y físicas, etc.</p>
<br><br>
<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/6hc4D1u8xaUOeOdd45jQqC?si=Af-1nazXSyWN2ZjYufjnRg&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 2 -->

<!-- Podcast 3 -->
<div class="col-sm-4  margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_3.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">El círculo vicioso del Amor Inmaduro</h4>
<p class="card-text">
Amor Inmaduro: Quienes lo sienten, se ven a ellos mismos como seres incompletos, que necesitan de otra persona para completarse. El amor inmaduro dice: "Te amo porque te necesito."</p>
<br><br>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/0deNHbzn44ZHqxfhTxMhZq?si=4Dsu3M9sTKKNf9jgt68i9w&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 3 -->

</div>

<div class="row">

<!-- Podcast 4 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_4.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Acupuntura</h4>
<p class="card-text">
¿Quieres conocer más acerca de la acupuntura y de sus beneficios? ☯️ en donde la Lic. Mónica Carol Bugarin nos explica acerca de porqué deberíamos tomar esta terapia alternativa 🧡</p>
<br><br><br><br>
<br><br>
<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/7DsztN6CxzWEQXunBzviOC?si=NioXo5T9RPCiV5JjePe9AQ&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 4 -->

<!-- Podcast 5 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_5.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Autoestima</h4>
<p class="card-text">
La Autoestima es la percepción de la persona respecto a ella misma 🧡 constituyendo el componente evaluativo del auto-valoración global que una persona realiza sobre sí. Por otro lado, en la auto-estima se involucran los sentimientos de respeto y de valor que una persona siente sobre ella, es decir, es amor propio 🧍🏻‍♀️🧍🏻🧍🏻‍♂️🧡
</p>
<br><br>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/2xyEXiCB9mPYT1STevb7bX?si=3UFaw8juTLScelxNZdagRw&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 5 -->


<!-- Podcast 6 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_6.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">El proceso de Duelo</h4>
<p class="card-text">
Duelo: Incluye todo el proceso emocional de enfrentarse a una pérdida y su duración puede ser prolongada ⏳Este proceso involucra emociones, acciones, expresiones diferentes y todas ellas ayudan a la persona a aceptar la pérdida de un ser amado 🪦, de una casa 🏡, un trabajo 💼, una pareja 💔, etc.</p>
<br><br><br>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/1H9UbF5qtB8EHK8W7nX6q1?si=IOinsgoxS6239sM7soKgJw&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 6 -->
</div>

<div class="row">

<!-- Podcast 7 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_7.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Terapia Neural</h4>
<p class="card-text">
La Terapia Neural 💉 es una forma de medicina alternativa con la que se estímulan el cuerpo posee para auto curarse, es decir, el cuerpo de cada persona tiene la capacidad de auto defenderse, auto curarse y mantenerse sano 💪🏼

Descubre más sobre la Terapia Neural y sus beneficios en el tratamiento de dolores y enfermedades con nuestra invitada la terapeuta Mónica Espíndola Buendía </p>

<br><br>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/4vUxvqatKUGHDVAJz3BHQL?si=eOgxMEroRZSux5Z6CSExdA&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 7 -->

<!-- Podcast 8 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_8.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Alimentos Funcionales</h4>
<p class="card-text">
Los alimentos funcionales 🥑 son aquellos que tienen un efecto potencialmente positivo en la salud 💪🏼más allá de la nutrición básica, debido a que promueven una salud óptima y ayudan a reducir el riesgo de padecer enfermedades 🥴

Descubre en que consisten y cuáles son los Alimentos Funcionales, cómo podemos integrarlos a nuestra alimentación diaria y sus beneficios en la salud con nuestra invitada la Nutrióloga Libia Darina Dosamantes Carrasco.</p>

<br>
<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/1LAqog9Zx6c8tSRLRHRdB4?si=14QaJIkhTMedJY6lGwZgtQ&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 8 -->

<!-- Podcast 9 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_9.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Dinero y Espíritu</h4>
<p class="card-text">
¿Sabías qué los problemas que tenemos con el dinero podrían tener su origen en nuestra historia familiar? 👨‍👩‍👧 Aquellos hábitos que aprendemos en casa sobre cómo ganar, guardar y gastar dinero quedan presentes e influyen en nuestra relación con el dinero 💸, y pueden ser la causa de cómo percibimos el dinero a nuestro alrededor 🙌🏼

Descubre más del Tena "Dinero y Espiritualidad" 🙏🏼 y como trabajar la forma en la que percibimos el dinero 💸 con nuestra invitada la Psicóloga y Terapeuta Integrativa Carol Bugarin</p>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/6Bj8vFcrjUdkQlLGQgwtG8?si=2yJjdc4CRjeG2WzxpSSrKg&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 9 -->

</div>

<div class="row">

<!-- Podcast 10 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_10.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">Técnicas de enseñanza de Lecto-Escritura</h4>
<p class="card-text">
Si el aprendizaje del habla 👅 se realiza imitando a las personas a nuestro alrededor, el aprendizaje de la lectura 📚 y escritura ✍🏼 se debería establecer de igual manera. En el sistema educativo se han utilizado diferentes métodos de lectoescritura, que como su nombre indica, son modelos para trabajar a la par la lectura y escritura 🤓

Descubre más de cuales son las Técnicas de Enseñanza de la Lecto-Escritura que podemos aprovechar para enseñar a leer y escribir a personas de toda edad con nuestra invitada la licenciada en Educación Especial, Mariel Flores González 😃 </p>

<br>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/75HJ88T6PQtfeo7Umd1HG9?si=GLmKBb2wTtis7b-Ll7vqBg&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 10 -->

<!-- Podcast 11 -->
<div class="col-sm-4 margin-bottom-2x pb-2">

<div class="card">
<img class="card-img-top rounded" src="<?=base_url('library/img/Podcast/Pot_11.png')?>" alt="Card image">

<div class="card-body">
<h4 class="card-title text-center">¿Cómo encontrar el origen de la ansiedad para sanar?</h4>
<p class="card-text">
Las emociones ☺️😔😣 que experimentamos día a día tienen un gran impacto sobre nuestro estado de salud 🫀e influyen tanto en la manera en enfermedades 🤧 cómo en el proceso de sanación 🙏🏼, por lo que es importante saber identificar dichas emociones para lograr sanar de forma más rápida y eficaz ☺️

Descubre Cómo encontrar el origen de la ansiedad para sanar con nuestro invitado, el Psicólogo y Especialista en Técnicas de Terapias Alternativas Víctor Hugo Murgo Tiscareño 😁</p>
<br><br><br>

<a class="btn btn-primary btn-sm" href="https://open.spotify.com/episode/69bchMSi7RaLyBHTNMKlLf?si=XDawt-0dTfek3p_j95nMrA&utm_source=copy-link">Escúchanos</a>
</div>

</div>
</div>
<!-- Podcast 11 -->


</div>

</div>
<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

