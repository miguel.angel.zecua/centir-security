<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column text-center">

<h1 class="text-dark">Acerca de nosotros </h1>
</div>

</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->

<div class="row align-items-center padding-bottom-2x">
<div class="col-md-5">
<img class="d-block w-270 m-auto" src="<?=base_url('library/img/About/Logo.png')?>" alt="Online Shopping"></div>

<div class="col-md-7 text-md-left text-center">
<div class="mt-30 hidden-md-up"></div>
<h2 class="text-center"><b>CenTI-R</b><h2>
<h2 class="text-center">Centro de Técnicas Integrativas para la Renovación</h2>
<p class="text-center">“En CenTI-R somos un centro especializado en diversas terapias alternativas y psicológicas dónde trabajamos en conjunto la parte física, emocional, y mental, para llegar a la sanación integral de tu ser.”
</p>
</div>
</div>

<hr>

<div class="row align-items-center padding-top-2x padding-bottom-2x">
<div class="col-md-5 order-md-2">
<img class="d-block w-270 m-auto" src="<?=base_url('library/img/About/Objetivo.png')?>" alt="Delivery"></div>

<div class="col-md-7 order-md-1 text-md-left text-center">
<div class="mt-30 hidden-md-up"></div>
<h2 class="text-center">OBJETIVO</h2>
<p class="text-center">Atender y capacitar gente en el arte de sanar, enseñándoles y aplicándoles varias técnicas para el tratamiento físico, mental y emocional del ser, logrando así un desarrollo e integración de la mente y el alma.
</p>
</div>
</div>

<hr>

<div class="row align-items-center padding-top-2x padding-bottom-2x">
<div class="col-md-5">
<img class="d-block w-270 m-auto" src="<?=base_url('library/img/About/Mision.png')?>" alt="Online Shopping"></div>

<div class="col-md-7 text-md-left text-center">
<div class="mt-30 hidden-md-up"></div>
<h2 class="text-center">MISIÓN</h2>
<p class="text-center">Mejorar la calidad de vida de las personas mediante e uso de terapias alternativas y psicológicas.</p>

<h2 class="text-center padding-top-1x">VISIÓN</h2>
<p class="text-center">Ser dentro de 5 años, la clínica líder en terapias alternativas y enseñanza en Tlaxcala, para las personas que busquen mejorar su estilo de vida e impartir conocimiento de dichas técnicas.
</p>

</div>
</div>


<hr>

<div class="text-center padding-top-3x mb-30">
<h2>PRÁCTICAS</h2>
</div>


<div class="row">


<div class="owl-carousel">



<div class="item">
<img src="<?=base_url('library/img/About/Value/1.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/3.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/4.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/5.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/6.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/7.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/8.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/9.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/10.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/11.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/12.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/13.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/14.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/15.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/16.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/17.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/18.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/19.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/20.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/22.jpeg'); ?>" style="border-radius: 13px;"></div>
<div class="item">
<img src="<?=base_url('library/img/About/Value/23.jpeg'); ?>" style="border-radius: 13px;"></div>
</div>


</div>


</div>
<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

<style type="text/css">
	
.owl-carousel {
padding:30px; 
border-radius: 13px;
}

.item {

background: #4DC7A0;
text-align: center;
color:#fff;
border-radius: 13px;

}
</style>


<script type="text/javascript">

$(document).ready(function() {
var car = $(".owl-carousel");
var carItems = 3; //items to show per page
car.owlCarousel({
items: carItems,
margin: 10,
nav: true,
navText: [],
loop: true,
center: true,
autoplay: true,
autoplayTimeout: 1750,
autoplayHoverPause: false,
responsiveClass: false,
callbacks: true,
onInitialized: function(e) {
if (carItems >= e.item.count) {
car.trigger("stop.owl.autoplay");
} else {}
}
});
});

</script>

</body>
</html>

