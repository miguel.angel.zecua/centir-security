
<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<div class="site-branding">
<div class="inner">
<!-- Off-Canvas Toggle (#shop-categories)-->
<a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>
<!-- Off-Canvas Toggle (#mobile-menu)-->
<a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
<!-- Site Logo-->
<a class="site-logo" href="index.html">
<img src="<?=base_url('library/img/Logo/Principal.png')?>" id="logo2" alt="Unishop"></a>
</div>
</div>

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>

<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->

<!-- Page Title-->
<div class="page-title">
<div class="container">

<div class="column">
<h1>A1</h1>
</div>

<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li><a href="<?=base_url('A0')?>">A0</a>
</li>

<li class="separator">&nbsp;</li>
<li>A1</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<div class="row">

<!-- Poduct Gallery-->
<div class="col-md-6">
<div class="product-gallery">

<div class="gallery-wrapper">
<div class="gallery-item video-btn text-center">
<a href="#" data-toggle="tooltip" data-type="video" data-video="&lt;div class=&quot;wrapper&quot;&gt;&lt;div class=&quot;video-wrapper&quot;&gt;&lt;iframe class=&quot;pswp__video&quot; width=&quot;960&quot; height=&quot;640&quot; src=&quot;//www.youtube.com/embed/c8ijHgTvv9I?rel=0&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;&lt;/div&gt;&lt;/div&gt;" title="Watch video"></a></div>
</div>

<div class="product-carousel owl-carousel gallery-wrapper">

<div class="gallery-item" data-hash="one">
<a href="<?=base_url('library/img/Categorias/A0/1.jpg')?>" data-size="1000x667">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Product"></a></div>
</div>

</div>
</div>

<!-- Product Info-->
<div class="col-md-6">
<div class="padding-top-2x mt-2 hidden-md-up"></div>
<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i>
</div><span class="text-muted align-middle">&nbsp;&nbsp;4.2 | 3 Consumidores</span>

<h2 class="padding-top-1x text-normal">A1</h2>
<span class="h2 d-block">$00.00</span>

<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam eligendi accusamus aut nemo nostrum, eveniet aperiam adipisci, nisi temporibus, sapiente error debitis. Corporis eligendi praesentium mollitia ullam quas consequuntur voluptate.</p>

<!-- Option Row -->

<!-- Option Row -->


<div class="pt-1 mb-2"><span class="text-medium">SKU:</span> #001</div>

<div class="padding-bottom-1x mb-2"><span class="text-medium">Categorias:&nbsp;</span><a class="navi-link" href="#">A0,</a><a class="navi-link" href="#"> A1</a>
</div>

<hr class="mb-3">

<div class="d-flex flex-wrap justify-content-between">
<div class="entry-share mt-2 mb-2"><span class="text-muted">Compartir:</span>
<div class="share-links"><a class="social-button shape-circle sb-facebook" href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a><a class="social-button shape-circle sb-instagram" href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="socicon-instagram"></i></a><a class="social-button shape-circle sb-google-plus" href="#" data-toggle="tooltip" data-placement="top" title="Google +"><i class="socicon-googleplus"></i></a></div>
</div>

<div class="sp-buttons mt-2 mb-2">

<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>

<button class="btn btn-primary" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!"><i class="icon-bag"></i>Añadir al Carrito</button>
</div>

</div>
</div>
</div>

<!-- Product Tabs-->
<div class="row padding-top-3x mb-3">
<div class="col-lg-10 offset-lg-1">
<ul class="nav nav-tabs" role="tablist">

<li class="nav-item">
<a class="nav-link active" href="#description" data-toggle="tab" role="tab">Descripcion</a>
</li>

<li class="nav-item">
<a class="nav-link" href="#reviews" data-toggle="tab" role="tab">Reseñas (3)</a>
</li>
</ul>

<div class="tab-content">
<div class="tab-pane fade show active" id="description" role="tabpanel">

<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores consectetur, quaerat beatae possimus sed laborum illo numquam earum optio ex enim! Ipsum aliquam, ullam mollitia similique vitae sequi beatae aspernatur?</p>

</div>

<div class="tab-pane fade" id="reviews" role="tabpanel">

<!-- Review 1-->
<div class="comment">
<div class="comment-author-ava">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Review author"></div>
<div class="comment-body">

<div class="comment-header d-flex flex-wrap justify-content-between">
<h4 class="comment-title">Titulo 1</h4>
<div class="mb-2">

<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i><i class="icon-star"></i>
</div>

</div>
</div>

<p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<div class="comment-footer"><span class="comment-meta">Mike</span></div>
</div>
</div>
<!-- Review 1-->

<!-- Review 2-->
<div class="comment">
<div class="comment-author-ava">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Review author"></div>
<div class="comment-body">

<div class="comment-header d-flex flex-wrap justify-content-between">
<h4 class="comment-title">Titulo 1</h4>
<div class="mb-2">

<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i><i class="icon-star"></i>
</div>

</div>
</div>

<p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<div class="comment-footer"><span class="comment-meta">Gil</span></div>
</div>
</div>
<!-- Review 2-->

<!-- Review 3-->
<div class="comment">
<div class="comment-author-ava">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Review author"></div>
<div class="comment-body">

<div class="comment-header d-flex flex-wrap justify-content-between">
<h4 class="comment-title">Titulo 1</h4>
<div class="mb-2">

<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i><i class="icon-star"></i>
</div>

</div>
</div>

<p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<div class="comment-footer"><span class="comment-meta">Mike Y Gil</span></div>
</div>
</div>
<!-- Review 3-->


<!-- Review Form -->
<h5 class="mb-30 padding-top-1x">Deje su opinión</h5>
<form class="row" method="post">

<div class="col-sm-6">
<div class="form-group">
<label for="review_name">Nombre</label>
<input class="form-control form-control-rounded" type="text" id="review_name" required>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="review_email">Email</label>
<input class="form-control form-control-rounded" type="email" id="review_email" required>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="review_subject">Titulo</label>
<input class="form-control form-control-rounded" type="text" id="review_subject" required>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="review_rating">Puntuacion</label>
<select class="form-control form-control-rounded" id="review_rating">
<option>5 Estrellas</option>
<option>4 Estrellas</option>
<option>3 Estrellas</option>
<option>2 Estrellas</option>
<option>1 Estrella</option>
</select>
</div>
</div>

<div class="col-12">
<div class="form-group">
<label for="review_text">Comentarios</label>
<textarea class="form-control form-control-rounded" id="review_text" rows="8" required></textarea>
</div>
</div>

<div class="col-12 text-right">
<button class="btn btn-outline-primary" type="submit">Enviar Comentarios</button>
</div>

</form><!-- Review Form -->

</div>
</div>
</div>
</div>

</div>



<!-- Page Content-->



    <!-- Photoswipe container-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>


<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>