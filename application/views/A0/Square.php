<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<div class="site-branding">
<div class="inner">
<!-- Off-Canvas Toggle (#shop-categories)-->
<a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>
<!-- Off-Canvas Toggle (#mobile-menu)-->
<a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
<!-- Site Logo-->
<a class="site-logo" href="index.html">
<img src="<?=base_url('library/img/Logo/Principal.png')?>" id="logo2" alt="Unishop"></a>
</div>
</div>

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column">

<h1>A0</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="index.html">Home</a>
</li>

<li class="separator">&nbsp;</li>
<li>A0</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view active" href="<?=base_url('A0')?>"><span></span><span></span><span></span></a>
<a class="list-view" href="<?=base_url('A0/show_list')?>"><span></span><span></span><span></span></a></div>
</div>

</div>

<div class="p-3 mb-4 bg-secondary text-dark text-center rounded"><b>CATEGORIA 1</b></div>

<!-- Products Grid 1-->
<div class="isotope-grid cols-3 mb-0 justify-content-center">
<div class="gutter-sizer"></div>
<div class="grid-sizer"></div>

<!-- Product 1-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('A0/A1')?>">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Product"></a>
<h3 class="product-title"><b>A1</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">
<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i>
</button>

<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Añadir</button>
</div>

</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('A0/A2')?>">
<img src="<?=base_url('library/img/Categorias/A0/2.jpg')?>" alt="Product"></a>
<h3 class="product-title"><b>A2</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">
<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i>
</button>

<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Añadir</button>
</div>

</div>
</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('A0/A3')?>">
<img src="<?=base_url('library/img/Categorias/A0/3.jpg')?>" alt="Product"></a>
<h3 class="product-title"><b>A3</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">
<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i>
</button>

<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Añadir</button>
</div>

</div>
</div>
<!-- Product 3-->

</div>
<!-- Products Grid 1-->

<div class="p-3 mb-2 bg-secondary text-dark text-center rounded"><b>CATEGORIA 2</b></div>

<!-- Products Grid 2-->
<div class="isotope-grid cols-3 mb-2 mt-4 justify-content-center">
<div class="gutter-sizer"></div>
<div class="grid-sizer"></div>

<!-- Product 1-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('A0/A4')?>">
<img src="<?=base_url('library/img/Categorias/A0/4.jpg')?>" alt="Product"></a>
<h3 class="product-title"><b>A4</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">
<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i>
</button>

<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Añadir</button>
</div>

</div>
</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('A0/A5')?>">
<img src="<?=base_url('library/img/Categorias/A0/5.jpg')?>" alt="Product"></a>
<h3 class="product-title"><b>A5</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">
<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i>
</button>

<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Añadir</button>
</div>

</div>
</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('A0/A6')?>">
<img src="<?=base_url('library/img/Categorias/A0/6.jpg')?>" alt="Product"></a>
<h3 class="product-title"><b>A6</b></h3>
<h4 class="product-price">$00.00</h4>

<div class="product-buttons">
<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i>
</button>

<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Añadir</button>
</div>

</div>
</div>
<!-- Product 3-->

</div>
<!-- Products Grid 2-->



</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

