<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<div class="page-title">
<div class="container">
<div class="column">
<h1 class="text-dark text-center">
Inicio de sesión / Registro </h1>
</div>
</div>
</div>
<!-- Page Content-->
<div class="container padding-bottom-3x mb-2">
<div class="row">

<!-- Columna 1-->
<div class="col-md-6">
<form class="login-box" id="fomulario1" method="post" action="<?php echo site_url('Login/Enter')?>">
<h3 class="margin-bottom-1x text-center">Inicio de sesión</h3>

<div class="form-group input-group">
<input class="form-control" type="email" placeholder="Correo" name="Email" id="Email" required=""><span class="input-group-addon"><i class="icon-mail"></i></span>
</div>

<div class="form-group input-group">
<input class="form-control" type="password" placeholder="Contraseña" name="Password_Main" id="Password_Main" required=""><span class="input-group-addon"><i class="icon-lock"></i></span>
</div>

<div class="d-flex flex-wrap justify-content-between padding-bottom-1x">
<div class="custom-control custom-checkbox">
<input class="custom-control-input" type="checkbox" id="remember_me" checked="">
<label class="custom-control-label" for="remember_me">Recuerdame</label>
</div><a class="navi-link" href="<?=base_url('Login/Recovery')?>">¿Se te olvidó tu contraseña?</a>
</div>

<div class="text-center text-sm-right">
<button class="btn btn-primary margin-bottom-none" type="submit">Iniciar</button>
</div>

</form>
</div>
<!-- Columna 1-->

<!-- Columna 2-->
<div class="col-md-6">
<div class="padding-top-3x hidden-md-up"></div>
<h3 class="margin-bottom-1x text-center">¿No tiene cuenta? Registrarse</h3>

<form class="row " id="formulario2" method="post" action="<?php echo site_url('Login/New')?>" >

<div class="col-sm-6">
<div class="form-group">
<label for="reg-fn">Nombre(s)</label>
<input class="form-control" type="text" name="Nombre" id="Nombre">
<div class="invalid-feedback">El Nombre(s) no tiene que tener caracteres especiales.</div>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="reg-ln">Apellidos</label>
<input class="form-control" type="text" name="Apellidos" id="Apellidos">
<div class="invalid-feedback">Los apellidos no tiene que tener caracteres especiales.</div>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="reg-email">Dirección de correo electrónico</label>
<input class="form-control" type="email" name="Correo" id="Correo">
<div class="invalid-feedback">El Correo no tiene que tener caracteres especiales.</div>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="reg-phone">Número de teléfono</label>
<input class="form-control" type="text" name="Telefono" id="Telefono">
<div class="invalid-feedback">El Telefono no tiene que tener caracteres especiales </div>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="reg-pass">Contraseña</label>
<input class="form-control" type="password" name="Password" id="Password">
<div class="invalid-feedback">La contraseña debe ser mayor a 4 y menor que 12 caracteres.</div>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="reg-pass-confirm">Confirmar Contraseña</label>
<input class="form-control" type="password" name="Password2" id="Password2">
<div class="invalid-feedback">Las contraseñas deben sen iguales.</div>
</div>
</div>

<div class="col-12 text-center text-sm-right">
<button class="btn btn-primary margin-bottom-none" type="submit">Registrarse</button>
</div>

</form>
</div>
</div>
</div>
<!-- Page Content-->
</div>
<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>
<script src="<?=base_url('library/js/Formulario.js')?>"></script>
</body>
</html>

