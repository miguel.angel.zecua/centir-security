<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<div class="site-branding" >
<div class="inner" >
<!-- Off-Canvas Toggle (#shop-categories)-->
<a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>
<!-- Off-Canvas Toggle (#mobile-menu)-->
<a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
<!-- Site Logo-->
<a class="site-logo" href="<?=base_url('Home')?>">
	
<img src="<?=base_url('library/img/Logo/Icono.svg')?>"   alt="Unishop"></a>	
<br>
</div>
</div>

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<div class="page-title">
<div class="container">
<div class="column">
<h1 class="text-danger text-center">
Recuperación de contraseña </h1>
</div>
</div>
</div>
<!-- Page Content-->
<div class="container padding-bottom-3x mb-2">

<div class="row justify-content-center">

<div class="col-lg-8 col-md-10">
<h2>¿Olvidaste tu contraseña?</h2>
<p>Cambie su contraseña en tres sencillos pasos. Esto ayuda a mantener segura su nueva contraseña.</p>

<ol class="list-unstyled">
<li><span class="text-primary text-medium">1. </span>Complete su dirección de correo electrónico abajo.</li>
<li><span class="text-primary text-medium">2. </span>Le enviaremos un código temporal por correo electrónico.</li>
<li><span class="text-primary text-medium">3. </span>Utilice el código para cambiar su contraseña en nuestro sitio web seguro.</li>
</ol>

<form class="card mt-4 rounded">
<div class="card-body rounded">

<div class="form-group">
<label for="email-for-pass">Ingrese su dirección de correo electrónico</label>
<input class="form-control" type="text" id="email-for-pass" required=""><small class="form-text text-muted">Escriba la dirección de correo electrónico que utilizó cuando se registró en Unishop. Luego, le enviaremos un código por correo electrónico a esta dirección.</small>
</div>

</div>
<div class="card-footer">
<button class="btn btn-primary" type="submit">Obtener nueva contraseña</button>
</div>
</form>
</div>
</div>


<!-- Page Content-->
</div>
<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

