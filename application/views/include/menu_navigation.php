<nav class="site-menu">
<ul>

<!-- MENU CONSULTAS-->
<li ><a href="<?=base_url('Consultas/Inicio')?>"><span>Consultas</span></a>
<ul class="sub-menu">
<li><a href="<?=base_url('Consultas/Inicio')?>">Individual</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Familiar</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">De pareja</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Infantil</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Adicciones</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Tanatología</a></li>
</ul>
</li>
<!-- MENU CONSULTAS-->

<!-- MENU CONFERENCIAS-->
<li class="has-megamenu"><a href="<?=base_url('Conferencias')?>"><span>Transmisiones </span></a>
<ul class="mega-menu">

<li><span class="mega-menu-title">Forense</span>
<ul class="sub-menu">
<li><a href="<?=base_url('Conferencias/Funcion_criminalista')?>">La función del criminalista en la necropsia</a></li>
<li><a href="<?=base_url('Conferencias/Investigacion_forense')?>">Investigación forense de artefactos explosivos y peligrosos</a></li>
<li><a href="<?=base_url('Conferencias/Investigacion_criminal')?>">Como inicia una investigación criminal en México</a></li>
<li><a href="<?=base_url('Conferencias/Trabajo_interdisciplinario')?>">Soñar no cuesta nada: “El trabajo interdisciplinario con adolescentes en internamiento"</a></li>
<li><a href="<?=base_url('Conferencias/Delincuentes_infancia')?>">¿Cómo formamos delincuentes desde la infancia?</a></li>
<li><a href="<?=base_url('Conferencias/Apoyo_balistica')?>">Apoyo a la balística forense en el apoyo criminal</a></li>
</ul>
</li>

<!-- -->
<li><span class="mega-menu-title">Jurídicos</span>
<ul class="sub-menu">
<li><a href="<?=base_url('Conferencias/Abogado_Perito')?>">Abogado vs Perito en el juicio oral</a></li>
</ul>
</li>
<!-- -->

<!-- -->
<li><span class="mega-menu-title">Psicología</span>
<ul class="sub-menu">
<li><a href="<?=base_url('Conferencias/Defensa_dolor')?>">Defensa ante el dolor</a></li>
</ul>
</li>
<!-- -->

<!-- -->
<li><span class="mega-menu-title">Terapias Alternativas</span>
<ul class="sub-menu">
<li><a href="<?=base_url('Conferencias/Influencia_emociones')?>">La influencia de las emociones en la salud</a></li>
<li><a href="<?=base_url('Conferencias/Terapia_dolor')?>">Terapia neural: “La terapia contra el dolor”</a></li>
<li><a href="<?=base_url('Conferencias/Alimentos_funcionales_enfermedades')?>">Alimentos funcionales: Promueven el riesgo a presentar enfermedades</a></li>
<li><a href="<?=base_url('Conferencias/Agua_Magnetizada')?>">Agua Magnetizada</a></li>
</ul>
</li>
<!-- -->

</ul>
</li>
<!-- MENU CONFERENCIAS-->

<!-- MENU CURSOS-->
<li class="has-megamenu"><a href="<?=base_url('Cursos')?>"><span>Cursos y Diplomados</span></a>
<ul class="mega-menu ">

<!-- 
<li ><span class="mega-menu-title">Terapias Integrativas</span>
<ul class="sub-menu">
<li class="d-none"><a href="<?=base_url('Cursos/Terapia_Imanes')?>">Terapia de Imanes para las Emociones</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Medicina_Cannabica')?>">Medicina Cannábica</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Auriculoterapia')?>">Auriculoterapia para control de peso</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Dejar_amar')?>">Dejar ir es amar</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Terapia_neural')?>">Terapia neural</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Alineacion_chakras')?>">Alineación de chakras con imanes</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Alimentos_funcionales')?>">Alimentos funcionales</a></li>
</ul>
</li>
 -->

<!--
<li class="d-none"><span class="mega-menu-title">Psicología</span>
<ul class="sub-menu">
<li class="d-none"><a href="<?=base_url('Cursos/Dinero_espiritu')?>">Dinero y espíritu</a></li>
</ul>
</li>
-->

<!-- -->
<li><span class="mega-menu-title">Pedagogía</span>
<ul class="sub-menu">
<li class="d-none"><a href="<?=base_url('Cursos/Estimulacion_temprana')?>">Estimulación temprana</a></li>
<li><a href="<?=base_url('Cursos/Lectoescritura_sensorial')?>">Lectoescritura sensorial</a></li>
</ul>
</li>
<!-- -->

<!-- -->
<li><span class="mega-menu-title">Forenses</span>
<ul class="sub-menu">
<li class="d-none"><a href="<?=base_url('Cursos/Balistica_Forense')?>">Balística Forense e identificación de Armas de Fuego</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Introduccion_Estudio')?>">Introducción Al Estudio De La Grafoscopía Y Documentoscopía</a></li>
<li><a href="<?=base_url('Cursos/Investigacion_criminal')?>">Investigación criminal</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Introduccion_dactiloscopia')?>">Introducción a la dactiloscopía</a></li>
</ul>
</li>
<!-- -->

<!-- 
<li><span class="mega-menu-title">Jurídicos</span>
<ul class="sub-menu">
<li class="d-none"><a href="<?=base_url('Cursos/Lectura_redaccion')?>">Lectura, redacción y argumentación jurídica</a></li>
</ul>
</li>
-->

<!-- -->
<li><span class="mega-menu-title">Otros</span>
<ul class="sub-menu">
<li><a href="<?=base_url('Cursos/Mercados_financieros')?>">¿Cómo invertir en los mercados financieros?</a></li>
<li><a href="<?=base_url('Cursos/Plantas_medicinales')?>">Plantas Medicinales</a></li>
</ul>
</li>
<!-- -->

<!-- -->
<li><span class="mega-menu-title">Diplomados</span>
<ul class="sub-menu">
<li><a href="<?=base_url('Diplomados/Biomagnetismo')?>">Biomagnetismo</a></li>
<li><a href="<?=base_url('Diplomados/S_penal_acusatorio')?>">Sistema penal acusatorio y sus etapas procesales</a></li>

<li><a href="<?=base_url('Diplomados/Masajes_Terapeuticos')?>">Masajes Terapeúticos</a></li>

<li><a href="<?=base_url('Diplomados/Acupuntura_Terapias_Alternativas')?>">Acupuntura y Terapias Alternativas</a></li>

</ul>
</li>
<!-- -->

</ul>
</li>
<!-- MENU CURSOS-->

<!-- MENU DIPLOMADOS-->
<li class="d-none"><a href="<?=base_url('Cursos')?>"><span>Diplomados</span></a>
<ul class="sub-menu">
<li><a href="<?=base_url('Diplomados/Biomagnetismo')?>">Biomagnetismo</a></li>
<li><a href="<?=base_url('Diplomados/S_penal_acusatorio')?>">Sistema penal acusatorio y sus etapas procesales</a></li>
</ul>
</li>
<!-- MENU DIPLOMADOS-->

<!-- MENU ALTERNATIVAS-->
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>"><span>Alternativas</span></a>
<ul class="sub-menu">
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Acupuntura</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Ventosas</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Auriculoterapia</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Conoterapia</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Terapia de imanes</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Flores mexicanas</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Terapia neural</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Alineación de chakras</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Masaje terapéutico</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Masaje reductivo</a></li>
</ul>
</li>
<!-- MENU ALTERNATIVAS-->

<li class="has-megamenu"><a href="<?=base_url('Tienda')?>"><span>Tienda</span></a>
</li>

<li class="has-megamenu"><a href="<?=base_url('Blog')?>"><span>Blog</span></a>
</li>

</ul>
</nav>