<meta charset="utf-8">
<!-- SEO Meta Tags-->
<meta name="description" content="CENTI-R">
<!-- Mobile Specific Meta Tag-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="google" value="notranslate">
<!-- Favicon and Apple Icons-->
<link rel="icon" type="image/x-icon" href="<?=base_url('library/img/Icon/favicon.ico')?>">
<link rel="icon" type="image/png" href="<?=base_url('library/img/Icon/favicon.png')?>">
<link rel="apple-touch-icon" href="touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="180x180" href="touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="167x167" href="touch-icon-ipad-retina.png">

<link rel="stylesheet" href="<?=base_url('library/Icons/css/all.css')?>">
<!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
<link rel="stylesheet" media="screen" href="<?=base_url('library/css/vendor.min.css')?>">
<!-- Main Template Styles-->
<link id="mainStyles" rel="stylesheet" media="screen" href="<?=base_url('library/css/styles.min.css')?>">
<!-- Customizer Styles-->
<link rel="stylesheet" media="screen" href="<?=base_url('library/css/customizer.min.css')?>">
<!-- Google Tag Manager-->

<!-- Modernizr-->
<script src="<?=base_url('library/js/modernizr.min.js')?>"></script>