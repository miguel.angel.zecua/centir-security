<footer class="site-footer">
<div class="container">

<div class="row">

<div class="col-lg-4 col-md-6">
<!-- Contact Info-->
<section class="widget widget-light-skin">
<h3 class="widget-title">CONTACTANOS</h3>
<p class="text-white">Telefono: 246 248 6999</p>

<ul class="list-unstyled text-sm text-white">
<li><span class="opacity-50">Lunes-Viernes:</span>9.00 am - 4.00 pm</li>
<li><span class="opacity-50">Sabados:</span>10.00 am - 4.00 pm</li>
</ul>
<p>
<a class="navi-link-light" href="mailto:centir2020@gmail.com" target="_blank">centir2020@gmail.com</a></p>

<a class="social-button shape-circle sb-facebook sb-light-skin" href="https://www.facebook.com/TecnicasIntegrativasYDeRenovacion/
" target="_blank">

<i class="socicon-facebook"></i></a>

<a class="social-button shape-circle sb-instagram sb-light-skin" href="https://www.instagram.com/centi_r_/
" target="_blank">

<i class="socicon-instagram"></i></a>

<a class="social-button shape-circle sb-google-plus sb-light-skin" href="mailto:centir2020@gmail.com" target="_blank">
<i class="socicon-googleplus"></i></a>

</section>
</div>

<!-- About Us-->
<div class="col-lg-4 col-md-6">
<section class="widget widget-links widget-light-skin">
<h3 class="widget-title">Sobre nosotros</h3>

<ul>
<li><a href="<?=base_url('About')?>">Acerca de CENTI-R</a></li>
<li><a href="<?=base_url('Home')?>">Servicios</a></li>
<li><a href="#">Tu Cuenta</a></li>
<li><a href="#">Políticas</a></li>
</ul>

</section>
</div>

<!-- Account / Shipping Info-->
<div class="col-lg-4 col-md-6">
<section class="widget widget-links widget-light-skin">

<img src="<?=base_url('library/img/Footer/Base.png')?>" style="margin-top: -25px;">


</section>
</div>
</div>

<hr class="hr-light mt-2 margin-bottom-1x">

<div class="row">
<!-- Payment Methods-->


<!--Subscription-->

</div>

<!-- Copyright-->
</div>
</footer>
