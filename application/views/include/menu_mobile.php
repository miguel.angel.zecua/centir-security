<div class="offcanvas-container" id="mobile-menu">
<a class="account-link" href="
<?php if($this->session->userdata('Login')==true)
{echo base_url('User/Perfil');}
else{echo base_url('Login');}
?>
"
<?=base_url('User/Perfil')?>">
<div class="user-ava"><img src="<?=base_url('library/img/Users/user-ava-md.jpg')?>" alt="Daniel Adams"></div>
<div class="user-info">

<h6 class="user-name" id="name_user">
<?php if($this->session->userdata('Login')==true)
{echo $this->session->userdata('Nombre');}
else{echo ('USER CENTI-R');}
?></h6>

</div>
</a>

<nav class="offcanvas-menu">
<ul class="menu">


<li class="has-children active"><span><a><span>Tu Cuenta</span></a>
<span class="sub-menu-toggle"></span></span>
<ul class="offcanvas-submenu">
<?php 
if($this->session->userdata('Login')==true)
{
echo '
<li><a href="'.base_url('User/Perfil').'">Mi Perfil</a></li>
<li><a href="'.base_url('User/Cursos').'">Cursos</a></li>
<li><a href="'.base_url('User/Diplomados').'">Diplomados</a></li>
<li class="sub-menu-separator"></li>
<li><a href="'.base_url('Login/End').'"> <i class=""></i>Salir</a></li>';
}

else
{
echo '
<li class="sub-menu-separator"></li>
<li class=""><a href="'.base_url('Login').'">INICIAR SESIÓN</a></li>';
}
?>

</ul>
</li>

<li class="has-children"><span><a href="<?=base_url('Consultas')?>">Consultas</a><span class="sub-menu-toggle"></span></span>
<ul class="offcanvas-submenu">
<li><a href="<?=base_url('Consultas/Inicio')?>">Individual</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Familiar</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">De pareja</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Infantil</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Adicciones</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Tanatología</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">Educación especial</a></li>
<li><a href="<?=base_url('Consultas/Inicio')?>">De lenguaje</a></li>
</ul>
</li>

<li class="has-children"><span><a href="<?=base_url('Conferencias')?>">Conferencias</a><span class="sub-menu-toggle"></span></span>
<ul class="offcanvas-submenu">
<li><a href="<?=base_url('Conferencias/Prevencion_suicidio')?>">Prevención del suicidio</a></li>
<li><a href="<?=base_url('Conferencias/Perfil_criminal')?>">Perfil criminal en juicios orales</a></li>
<li><a href="<?=base_url('Conferencias/Conductas_destructivas')?>">Conductas destructivas en la niñez y adolescencia</a></li>
<li><a href="<?=base_url('Conferencias/Funcion_criminalista')?>">La función del criminalista en la necropsia</a></li>
<li><a href="<?=base_url('Conferencias/Investigacion_forense')?>">Investigación forense de artefacto explosivos y peligrosos</a></li>

<li><a href="<?=base_url('Conferencias/Factor_criminologico')?>">La familia como factor criminológico</a></li>
<li><a href="<?=base_url('Conferencias/Investigacion_criminal')?>">Como inicia una investigación criminal en México</a></li>
<li><a href="<?=base_url('Conferencias/Trabajo_interdisciplinario')?>">Soñar no cuesta nada: “El trabajo interdisciplinario con adolescentes en internamiento</a></li>
<li><a href="<?=base_url('Conferencias/Delincuentes_infancia')?>">¿Cómo formamos delincuentes desde la infancia?</a></li>
<li><a href="<?=base_url('Conferencias/Apoyo_balistica')?>">Apoyo a la balística forense en el apoyo criminal</a></li>

<li><a href="<?=base_url('Conferencias/Abogado_Perito')?>">Abogado vs Perito en el juicio oral</a></li>
<li><a href="<?=base_url('Conferencias/Importancia_ortografia')?>">Importancia de la ortografía, redacción y argumentación en el ámbito Jurídico</a></li>
<li><a href="<?=base_url('Conferencias/Soluciones_alternas')?>">Soluciones alternas del procedimiento penal</a></li>
<li><a href="<?=base_url('Conferencias/Relacion_historia_familiar')?>">¿Qué relación hay entre mi historia familiar y mis problemas con el dinero?</a></li>
<li><a href="<?=base_url('Conferencias/Perdida_perdon')?>">La perdida y el perdón</a></li>

<li><a href="<?=base_url('Conferencias/Defensa_dolor')?>">Defensa ante el dolor</a></li>
<li><a href="<?=base_url('Conferencias/Emociones_vida')?>">¿Qué papel juegan las emociones dentro de mi vida?</a></li>
<li><a href="<?=base_url('Conferencias/Conociendo_construyendo')?>">Conociendo y construyendo mi masculinidad</a></li>
<li><a href="<?=base_url('Conferencias/Duelo_Covid_19')?>">Duelo ante el Covid-19</a></li>
<li><a href="<?=base_url('Conferencias/Defensa_dolor')?>">Defensa ante el dolor</a></li>

<li><a href="<?=base_url('Conferencias/Circulo_vicioso')?>">Circulo vicioso del amor inmaduro</a></li>
<li><a href="<?=base_url('Conferencias/Liderazgo_aplicado')?>">Liderazgo aplicado al desarrollo personal</a></li>
<li><a href="<?=base_url('Conferencias/Errores_lectoescritura')?>">Errores más comunes en la lectoescritura: Estrategias de prevención y corrección</a></li>
<li><a href="<?=base_url('Conferencias/Retos_educacion')?>">Retos de la educación ante la nueva normalidad</a></li>
<li><a href="<?=base_url('Conferencias/Cerebro_confinamiento')?>">¿Cómo aprende el cerebro en tiempos de confinamiento?</a></li>

<li><a href="<?=base_url('Conferencias/Influencia_emociones')?>">La influencia de las emociones en la salud</a></li>
<li><a href="<?=base_url('Conferencias/Terapia_dolor')?>">Terapia neural: “La terapia contra el dolor”</a></li>
<li><a href="<?=base_url('Conferencias/Alimentos_funcionales_enfermedades')?>">Alimentos funcionales: Promueven el riesgo a presentar enfermedades</a></li>
<li><a href="<?=base_url('Conferencias/Agua_Magnetizada')?>">Agua Magnetizada</a></li>

</ul>
</li>

<li class="has-children"><span><a href="<?=base_url('Cursos')?>">Cursos</a><span class="sub-menu-toggle"></span></span>
<ul class="offcanvas-submenu">
<li class="d-none"><a href="<?=base_url('Cursos/Terapia_Imanes')?>">Terapia de Imanes para las Emociones</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Medicina_Cannabica')?>">Medicina Cannábica</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Auriculoterapia')?>">Auriculoterapia para control de peso</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Estimulacion_temprana')?>">Estimulación temprana</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Balistica_Forense')?>">Balística Forense e identificación de Armas de Fuego</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Introduccion_Estudio')?>">Introducción Al Estudio De La Grafoscopía Y Documentoscopía</a></li>
<li><a href="<?=base_url('Cursos/Investigacion_criminal')?>">Investigación criminal</a></li>
<li class="d-none"><a href="<?=base_url('Cursos/Introduccion_dactiloscopia')?>">Introducción a la dactiloscopía</a></li>
<li><a href="<?=base_url('Cursos/Lectura_redaccion')?>">Lectura, redacción y argumentación jurídica</a></li>
<li><a href="<?=base_url('Cursos/Mercados_financieros')?>">¿Cómo invertir en los mercados financieros?</a></li>
</ul>
</li>

<li class="has-children"><span><a href="<?=base_url('Cursos')?>">Diplomados</a><span class="sub-menu-toggle"></span></span>
<ul class="offcanvas-submenu">
<li><a href="<?=base_url('Diplomados/Acupuntura')?>">Acupuntura</a></li>
<li><a href="<?=base_url('Diplomados/Biomagnetismo')?>">Biomagnetismo</a></li>
<li><a href="<?=base_url('Diplomados/S_penal_acusatorio')?>">Sistema penal acusatorio y sus etapas procesales</a></li>
</ul>
</li>


<li class="has-children"><span><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Terapias alternativas</a><span class="sub-menu-toggle"></span></span>
<ul class="offcanvas-submenu">
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Acupuntura</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Ventosas</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Auriculoterapia</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Conoterapia</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Terapia de imanes</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Flores mexicanas</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Terapia neural</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Alineación de chakras</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Masaje terapéutico</a></li>
<li><a href="<?=base_url('Terapias_alternativas/Inicio')?>">Masaje reductivo</a></li>
</ul>
</li>
 
<li class="has-children"><span><a href="<?=base_url('Tienda')?>">Tienda</a></span>
</li>

<li class="has-children"><span><a href="<?=base_url('Blog')?>">Blog</a></span>
</li>


<li class="has-children"><span><a href="<?=base_url('About')?>">Acerca de</a></span>
</li>

<li class="has-children"><span><a href="<?=base_url('Contact')?>">Contacto</a></span>
</li>
 
</ul>
</nav> 
</div>