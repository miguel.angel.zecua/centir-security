<div class="site-backdrop"></div>
 
<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="<?=base_url('library/js/jQuery.js')?>"></script>

<!-- Customizer scripts-->
<script src="<?=base_url('library/js/vendor.min.js')?>"></script>
<script src="<?=base_url('library/js/scripts.min.js')?>"></script>
<script src="<?=base_url('library/js/customizer.min.js')?>"></script>
<script src="<?=base_url('library/js/verify.js')?>"></script>
<script src="<?=base_url('library/js/scroll.js')?>"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>