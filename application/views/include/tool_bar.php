<div class="toolbar">
<div class="inner">
<div class="tools">
<div class="search invisible"><i class="icon-search"></i></div>

<div class="account mr-5"><a href="#"></a><i class="icon-head"></i>
<ul class="toolbar-dropdown">

<li class="sub-menu-user">       
<div class="user-ava"><img src="<?=base_url('library/img/Users/user-ava-sm.jpg')?>" alt="Daniel Adams">
</div>
<div class="user-info">
<h6 class="user-name" id="name_user_1"><?php if($this->session->userdata('Login')==true){echo $this->session->userdata('Nombre');}else{echo "USER CENTI-R";}?></h6>
</div>
</li>
<?php 
if($this->session->userdata('Login')==true)
{
echo '
<li><a href="'.base_url('User/Perfil').'">Mi Perfil</a></li>
<li><a href="'.base_url('User/Cursos').'">Cursos</a></li>
<li class="sub-menu-separator"></li>
<li><a href="'.base_url('User/Diplomados').'">Diplomados</a></li>
<li class="sub-menu-separator"></li>
<li><a href="'.base_url('Login/End').'"> <i class="icon-unlock"></i>Salir</a></li>';
}
else
{
echo '
<li class="sub-menu-separator"></li>
<li class="text-center"><a href="'.base_url('Login').'">INICIAR SESIÓN</a></li>';
}

?>


</ul>


</div>

<div class="cart d-none"><a href=""></a>
<i class="icon-bag"></i>
<span class="count"><!--- Numero ---></span>
<span class="subtotal"><!--- Numero ---></span>
              
</div>
</div>
</div>