<!-- Introduccion-->
<div class="col-md-12 col-sm-12 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">1.0 INTRODUCCIÓN.</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Introduccion/Introduccion.MOV')?>" type="video/mp4">	
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Introduccion-->

<!-- Modulo 1.1-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 1.1</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls autoplay>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Modulo-1/M-1.1.MOV')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 1.1-->

<!-- Modulo 1.2-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 1.2</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls autoplay>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Modulo-1/M-1.2.MOV')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 1.2-->

<!-- Modulo 1.3-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 1.3</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls autoplay>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Modulo-1/M-1.3.MOV')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 1.3-->

<!-- Introduccion Modulo 2-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">INTRODUCCIÓN MODULO 2</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls autoplay>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Modulo-2/Introducción.MOV')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Introduccion Modulo 2-->

<!-- Modulo 2.1-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_6" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 2.1</b></a></h6>
</div>

<div class="collapse" id="temario_6" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls autoplay>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Modulo-2/M-2.1.MOV')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 2.1-->

<!-- Modulo 3.1-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_7" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.1</b></a></h6>
</div>

<div class="collapse" id="temario_7" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls autoplay>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Modulo-3/M-3.1.MOV')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.1-->

<!-- Modulo 3.2-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_8" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.2</b></a></h6>
</div>

<div class="collapse" id="temario_8" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls autoplay>
<source src="<?=base_url('Material/Videos/Investigacion-Criminal/Modulo-3/M-3.2.MOV')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.2-->






