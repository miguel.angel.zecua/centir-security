<!-- Introduccion-Nombre-->
<div class="col-md-12 col-sm-12 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_0" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">INTRODUCCION</b></a></h6>
</div>

<div class="collapse" id="temario_0" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Presentacion-Nombre/Nombre2.mp4')?>" type="video/mp4">	
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Introduccion-Nombre-->


<!-- Introduccion-->
<div class="col-md-12 col-sm-12 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 1.1</b></a></h6>
</div>

<div class="collapse" id="temario_1" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-1/C1.1.mp4')?>" type="video/mp4">	
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Introduccion-->

<!-- Modulo 1.1-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 1.2</b></a></h6>
</div>

<div class="collapse" id="temario_2" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-1/C1.2.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 1.1-->

<!-- Modulo 1.2-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_3" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 1.3</b></a></h6>
</div>

<div class="collapse" id="temario_3" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-1/C1.3.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 1.2-->

<!-- Modulo 1.3-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_4" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 2.1</b></a></h6>
</div>

<div class="collapse" id="temario_4" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-2/C2.1.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 1.3-->

<!-- Introduccion Modulo 2-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_5" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 2.2</b></a></h6>
</div>

<div class="collapse" id="temario_5" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-2/C2.2.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Introduccion Modulo 2-->

<!-- Modulo 2.1-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_6" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 2.3</b></a></h6>
</div>

<div class="collapse" id="temario_6" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-2/C2.3.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 2.1-->

<!-- Modulo 3.1-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_7" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.1</b></a></h6>
</div>

<div class="collapse" id="temario_7" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-3/C3.1.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.1-->

<!-- Modulo 3.2.1-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_8" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.2.1</b></a></h6>
</div>

<div class="collapse" id="temario_8" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-3/C3.2.1.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.2.1-->

<!-- Modulo 3.2.2-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_9" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.2.2</b></a></h6>
</div>

<div class="collapse" id="temario_9" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-3/C3.2.2.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.2.2-->

<!-- Modulo 3.2.3-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_10" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.2.3</b></a></h6>
</div>

<div class="collapse" id="temario_10" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-3/C3.2.3.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.2.3-->

<!-- Modulo 3.2.4-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_11" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.2.4</b></a></h6>
</div>

<div class="collapse" id="temario_11" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-3/C3.2.4.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.2.4-->

<!-- Modulo 3.2.5-->
<div class="col-md-12 col-sm-12 mt-1">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#temario_12" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">MODULO 3.2.5</b></a></h6>
</div>

<div class="collapse" id="temario_12" role="tabpanel">
<div class="card-body">
<!-- Body-->

<div class="embed-responsive embed-responsive-16by9" style="border-radius: 15px;">
<video class="embed-responsive-item" controls>
<source src="<?=base_url('Material/Videos/Lectoescritura-sensorial/Modulo-3/C3.2.5.mp4')?>" type="video/mp4"> 
</video>
</div>

<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Modulo 3.2.5-->

<!-- Anexo 1-->
<div class="col-md-12 col-sm-12 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Anexo_1" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">ANEXO-1</b></a></h6>
</div>

<div class="collapse" id="Anexo_1" role="tabpanel">
<div class="card-body">
<!-- Body-->


<object data="<?=base_url('Material/Videos/Lectoescritura-sensorial/Anexo-1.pdf')?>" type="application/pdf" width="100%" height="800px"> 
  <p>It appears you don't have a PDF plugin for this browser.
   No biggie... you can <a href="resume.pdf">click here to
  download the PDF file.</a></p>  
</object>


<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Anexo 1-->

<!-- Anexo 2-->
<div class="col-md-12 col-sm-12 ">

<div class="accordion"  role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Anexo_2" data-toggle="collapse" style="font-size: 16px"><b class="text-danger">ANEXO-2</b></a></h6>
</div>

<div class="collapse" id="Anexo_2" role="tabpanel">
<div class="card-body">
<!-- Body-->


<object data="<?=base_url('Material/Videos/Lectoescritura-sensorial/Anexo-2.pdf')?>" type="application/pdf" width="100%" height="800px"> 
  <p>It appears you don't have a PDF plugin for this browser.
   No biggie... you can <a href="resume.pdf">click here to
  download the PDF file.</a></p>  
</object>


<!-- Body-->
</div>
</div>
</div>
</div>

</div>
<!-- Anexo 2-->




