<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>

<link rel="stylesheet" media="screen" href="<?=base_url('library/css/tablas/datatables.css')?>">
<link rel="stylesheet" media="screen" href="<?=base_url('library/css/iconos/all.css')?>">
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">



<div class="container padding-bottom-3x">
<!-- Shop Toolbar-->


<h4 class="text-muted text-normal text-uppercase margin-top-1x text-center">Datos Generales</h4>
<hr class="padding-bottom-2x">

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Nombre Completo:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->nombre_usuario; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 17px">Edad:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->edad_usuario; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Correo:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->correo; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>


<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Telefono:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->telefono; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>

<hr class="mb-4">

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Consulta:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->Consulta; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Modalidad:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->Modalidad; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Sexo Psicologo:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->Sexo_Psicologo; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Horario:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->horario; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Fecha:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" name="birthdate" value="<?php echo $row[0]->fecha; ?>" aria-describedby="emailHelp" readonly >
</div>
</div>


</div>
</div>


<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>


</body>
</html>

