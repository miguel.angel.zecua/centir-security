<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>

<link rel="stylesheet" media="screen" href="<?=base_url('library/css/tablas/datatables.css')?>">
<link rel="stylesheet" media="screen" href="<?=base_url('library/css/iconos/all.css')?>">
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">



<div class="container padding-bottom-3x">
<!-- Shop Toolbar-->


<ul class="nav nav-pills nav-justified margin-top-2x">
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin')?>">Consultas</a></li>
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin/Conferencias')?>">Conferencias</a></li>
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin/Cursos')?>">Cursos</a></li>
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin/Diplomados')?>">Diplomados</a></li>
<li class="nav-item"><a class="nav-link active" href="<?=base_url('Admin/Terapias')?>">Terapias</a></li>
</ul>

<hr class="mt-3 mb-2">

<div class="row margin-top-1x justify-content-center">

<div class="col-sm-6 ">
</div>

<div class="col-sm-6 ">
<div class="shop-view">
<a class="list-view active" href="<?=base_url('Admin/Terapias')?>"><span></span><span></span><span></span></a>    
<a class="grid-view" href="<?=base_url('Admin/Calendar_Terapia')?>"><span></span><span></span><span></span></a>
</div>
</div>

<div class="col-sm-6">
</div>

</div>


<div class="row margin-top-1x">

<div class="col-sm-12">

<div class="table-responsive">

<table id="example" class="display table table-hover text-nowrap table-bordered rounded" style="font-size: 12px">

<thead class="thead-dark">
<tr>
<th scope="col">Id</th>
<th scope="col">Nombre Usuario</th>
<th scope="col">Edad</th>
<th scope="col">Correo</th>
<th scope="col">Telefono</th>
<th scope="col">Fecha</th>
<th scope="col">Horario</th>
<th scope="col">Terapia</th>
<th scope="col">Ver</th>
</tr>
</thead>

<tbody>
<?php foreach($result as $row) {?>
<tr>
<td scope="row"><?php echo $row->Id_Terapia_Alternativa; ?></td>
<td><?php echo $row->nombre_usuario; ?></td>
<td><?php echo $row->edad; ?></td>
<td><?php echo $row->correo_usuario; ?></td>
<td><?php echo $row->telefono_usuario; ?></td>
<td><?php echo $row->fecha; ?></td>
<td><?php echo $row->horario; ?></td>
<td><?php echo $row->Terapia; ?></td>
<td><a href="<?php echo site_url('Admin/Vista_Terapia');?>/<?php echo $row->Id_Terapia_Alternativa;?>"><i class="fas fa-eye d-flex align-items-center ml-3"></i></a></td>
</tr>
<?php } ?>
</tbody>
</table>


</div>
	
</div>

</div>
</div>

<style type="text/css">
.material-icons.md-18 { font-size: 8px; }
.pagination{
    display:-webkit-box;
    display:-ms-flexbox;
    display:flex;
    padding-left:0;
    list-style:none;
    border-radius:10px;
}
.page-link{
    text-decoration: none;
    margin-top: 10px;
    position:relative;
    display:block;
    padding:.75rem 1rem;
    margin-left:5px;
    line-height:1.25;
    color:#007bff;
    background-color:#fff;
    border:1px solid #dee2e6;
    border-radius:30px;
}
.page-link:hover{
    color:#0056b3;
    text-decoration:none;
    background-color:#e9ecef;
    border-color:#dee2e6
}




.page-item.active .page-link{
    z-index:1;
    color:#fff;
    background-color:#007bff;
    border-color:#007bff
}




</style>


<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>
<script src="<?=base_url('library/js/tablas/datatables.min.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable({
        "language": espanol
    });
} );

let espanol = {
    "aria": {
        "sortAscending": "Activar para ordenar la columna de manera ascendente",
        "sortDescending": "Activar para ordenar la columna de manera descendente"
    },
    "autoFill": {
        "cancel": "Cancelar",
        "fill": "Rellene todas las celdas con <i>%d&lt;\\\/i&gt;<\/i>",
        "fillHorizontal": "Rellenar celdas horizontalmente",
        "fillVertical": "Rellenar celdas verticalmentemente"
    },
    "buttons": {
        "collection": "Colección",
        "colvis": "Visibilidad",
        "colvisRestore": "Restaurar visibilidad",
        "copy": "Copiar",
        "copyKeys": "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
        "copySuccess": {
            "1": "Copiada 1 fila al portapapeles",
            "_": "Copiadas %d fila al portapapeles"
        },
        "copyTitle": "Copiar al portapapeles",
        "csv": "CSV",
        "excel": "Excel",
        "pageLength": {
            "-1": "Mostrar todas las filas",
            "1": "Mostrar 1 fila",
            "_": "Mostrar %d filas"
        },
        "pdf": "PDF",
        "print": "Imprimir"
    },
    "decimal": ",",
    "emptyTable": "No se encontraron resultados",
    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
    "infoThousands": ",",
    "lengthMenu": "Mostrar _MENU_ registros",
    "loadingRecords": "Cargando...",
    "paginate": {
        "first": "Primero",
        "last": "Último",
        "next": "Siguiente",
        "previous": "Anterior"
    },
    "processing": "Procesando...",
    "search": "Buscar:",
    "searchBuilder": {
        "add": "Añadir condición",
        "button": {
            "0": "Constructor de búsqueda",
            "_": "Constructor de búsqueda (%d)"
        },
        "clearAll": "Borrar todo",
        "condition": "Condición",
        "data": "Data",
        "deleteTitle": "Eliminar regla de filtrado",
        "leftTitle": "Criterios anulados",
        "logicAnd": "Y",
        "logicOr": "O",
        "rightTitle": "Criterios de sangría",
        "title": {
            "0": "Constructor de búsqueda",
            "_": "Constructor de búsqueda (%d)"
        },
        "value": "Valor"
    },
    "searchPanes": {
        "clearMessage": "Borrar todo",
        "collapse": {
            "0": "Paneles de búsqueda",
            "_": "Paneles de búsqueda (%d)"
        },
        "count": "{total}",
        "countFiltered": "{shown} ({total}",
        "emptyPanes": "Sin paneles de búsqueda",
        "loadMessage": "Cargando paneles de búsqueda",
        "title": "Filtros Activos - %d"
    },
    "select": {
        "1": "%d fila seleccionada",
        "_": "%d filas seleccionadas",
        "cells": {
            "1": "1 celda seleccionada",
            "_": "$d celdas seleccionadas"
        },
        "columns": {
            "1": "1 columna seleccionada",
            "_": "%d columnas seleccionadas"
        }
    },
    "thousands": ",",
    "zeroRecords": "No se encontraron resultados",
    "datetime": {
        "previous": "Anterior",
        "next": "Proximo",
        "hours": "Horas",
        "minutes": "Minutos",
        "seconds": "Segundos",
        "unknown": "-",
        "amPm": [
            "am",
            "pm"
        ]
    },
    "editor": {
        "close": "Cerrar",
        "create": {
            "button": "Nuevo",
            "title": "Crear Nuevo Registro",
            "submit": "Crear"
        },
        "edit": {
            "button": "Editar",
            "title": "Editar Registro",
            "submit": "Actualizar"
        },
        "remove": {
            "button": "Eliminar",
            "title": "Eliminar Registro",
            "submit": "Eliminar",
            "confirm": {
                "_": "¿Está seguro que desea eliminar %d filas?",
                "1": "¿Está seguro que desea eliminar 1 fila?"
            }
        },
        "error": {
            "system": "Ha ocurrido un error en el sistema (<a target=\"\\\" rel=\"\\ nofollow\" href=\"\\\">Más información&lt;\\\\\\\/a&gt;).&lt;\\\/a&gt;<\/a>"
        },
        "multi": {
            "title": "Múltiples Valores",
            "info": "Los elementos seleccionados contienen diferentes valores para este registro. Para editar y establecer todos los elementos de este registro con el mismo valor, hacer click o tap aquí, de lo contrario conservarán sus valores individuales.",
            "restore": "Deshacer Cambios",
            "noMulti": "Este registro puede ser editado individualmente, pero no como parte de un grupo."
        }
    }
} ;
</script>
</body>
</html>

