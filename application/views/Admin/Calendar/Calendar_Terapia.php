<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
<link href="<?=base_url('library/calendar/main.css')?>" rel='stylesheet' />
<script src="<?=base_url('library/calendar/main.js')?>"></script>
<script src="<?=base_url('library/calendar/locales/es.js')?>"></script>
<script src="<?=base_url('library/js/jQuery.js')?>"></script>

<script>

document.addEventListener('DOMContentLoaded', function() 
{

var calendarEl = document.getElementById('calendar');

var calendar = new FullCalendar.Calendar(calendarEl, 
{
eventClick: function(info) 
{       
$('#nombre').val(info.event.title);
$('#edad').val(info.event._def.extendedProps.edad);
$('#correo').val(info.event._def.extendedProps.correo);
$('#telefono').val(info.event._def.extendedProps.telefono);
$('#fecha').val(info.event.start);
$('#motivos').val(info.event._def.extendedProps.motivo);
$('#terapia').val(info.event._def.extendedProps.terapia);


$('#exampleModal').modal('show')
},




selectable: true,

dayMaxEvents: true,

initialView: 'dayGridMonth',

locale: 'es',

headerToolbar: {
left: 'dayGridMonth,timeGridWeek,timeGridDay',
      center: 'title',
      right: 'prevYear,prev,next,nextYear'
},

footerToolbar: {
center: '',
right: 'prev,next'
},

events: 
[
<?php foreach($result as $row) {
?>

{
title  : '<?php echo $row->nombre_usuario; ?>',
start  : '<?php echo $row->fecha; ?>T<?php echo $row->horario; ?>',
edad : '<?php echo $row->edad; ?>',
correo : '<?php echo $row->correo_usuario; ?>',
telefono : '<?php echo $row->telefono_usuario; ?>',
motivo : '<?php echo $row->Motivos; ?>',
terapia : '<?php echo $row->Terapia; ?>',
},
<?php } ?>

]

});
calendar.render();
});

</script>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">

<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel"></h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">

<h4 class="text-muted text-normal text-uppercase text-center">Datos Generales</h4>
<hr class="padding-bottom-2x">

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Nombre Completo:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" id="nombre" name="birthdate" value="" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 17px">Edad:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" id="edad" name="birthdate" value="" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Correo:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" id="correo" name="birthdate" value="" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Teléfono:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" id="telefono" name="birthdate" value="" aria-describedby="emailHelp" readonly >
</div>
</div>

<hr class="mb-4">

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Fecha:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" id="fecha" name="birthdate" value="" aria-describedby="emailHelp" readonly >
</div>
</div>


<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Motivos:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" id="motivos" name="birthdate" value="" aria-describedby="emailHelp" readonly >
</div>
</div>

<div class="form-group row">
<label class="col-3 col-form-label" for="text-input" style="font-size: 16px">Terapia:</label>
<div class="col-9">
<input type="text" class="form-control text-danger" style="font-size: 16px" id="terapia" name="birthdate" value="" aria-describedby="emailHelp" readonly >
</div>
</div>


</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

</div>
</div>
</div>
</div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">


<div class="container padding-bottom-3x">
<!-- Shop Toolbar-->


<ul class="nav nav-pills nav-justified margin-top-2x">
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin')?>">Consultas</a></li>
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin/Conferencias')?>">Conferencias</a></li>
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin/Cursos')?>">Cursos</a></li>
<li class="nav-item"><a class="nav-link" href="<?=base_url('Admin/Diplomados')?>">Diplomados</a></li>
<li class="nav-item"><a class="nav-link active" href="<?=base_url('Admin/Terapias')?>">Terapias</a></li>
</ul>

<hr class="mt-3 mb-2">

<div class="row margin-top-1x justify-content-center">

<div class="col-sm-6 ">
</div>

<div class="col-sm-6 ">
<div class="shop-view">
<a class="list-view" href="<?=base_url('Admin/Terapias')?>"><span></span><span></span><span></span></a>    
<a class="grid-view active" href="<?=base_url('Admin/Calendar_Terapia')?>"><span></span><span></span><span></span></a>
</div>
</div>

<div class="col-sm-6">
</div>

</div>



<div id='calendar'></div>

</div>
</div>

    
  

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

