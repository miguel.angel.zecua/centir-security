<div class="p-3 mb-2 bg-secondary text-dark rounded text-center"><b>CONFIGURE SU CITA</b></div>

<form id="formulario">

<div class="row" id="fila_1">
<div class="col-sm-6">
<div class="form-group">

<label for="size">Nombre del paciente</label>
<input class="form-control" type="text" placeholder="Nombre Completo" id="nombre" name="nombre" onkeyup="mayus(this);">
</div>
</div>

<div class="col-sm-6">
<div class="form-group">

<label for="size">Edad</label>
<input class="form-control " type="number" placeholder="Edad" name="edad" id="edad">
</div>
</div>


</div>

<div class="row">

<div class="col-sm-6">
<div class="form-group">

<label for="size">Correo</label>
<input class="form-control" type="email" placeholder="Correo Electrónico" name="correo" id="correo">
</div>
</div>

<div class="col-sm-6">
<div class="form-group">

<label for="size">Teléfono </label>
<input class="form-control" type="text"  placeholder="Teléfono" name="telefono" id="telefono">
</div>
</div>
</div>

<div class="row">


<div class="col-sm-6">
<div class="form-group">
<label for="color">Fecha</label>
<input type='date' class="form-control" id="fecha" value="<?php echo date('Y-m-d')?>" min="<?php echo date('Y-m-d')?>">
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="color">Horario</label>
<select class="form-control" name="horario" id="horario" >
<option selected></option>
<option value="09:00 AM">09:00 AM-10:00 AM</option>
<option value="10:00 AM">10:00 AM-11:00 AM</option>
<option value="11:00 AM">11:00 AM-12:00 PM</option>
<option value="12:00 PM">12:00 PM-01:00 PM</option>
<option value="01:00 PM">01:00 PM-02:00 PM</option>
<option value="02:00 PM">02:00 PM-03:00 PM</option>
<option value="03:00 PM">03:00 PM-04:00 PM</option>
</select>
</div>
</div>


</div>

<div class="row">

<div class="col-sm-12">	
<div class="form-group">
<label for="color">Motivos de su consulta:</label>
<textarea class="form-control" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 107px;" id="motivos"></textarea>
</div>

</div>
</div>


<!-- Option Row -->


<hr class="mb-3">

<div class="d-flex flex-wrap justify-content-between">
<div class="entry-share mt-2 mb-2 invisible"><span class="text-muted">Compartir:</span>
<div class="share-links"><a class="social-button shape-circle sb-facebook" href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a><a class="social-button shape-circle sb-instagram" href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="socicon-instagram"></i></a></div>
</div>

<div class="sp-buttons mt-2 mb-2">


<button class="btn btn-primary disabled" type="button" data-toggle="modal" data-target="#modal" id="main" >
<i class="icon-bag mr-2 mb-1"></i>Crear cita</button>
</div>

</form>
</div>
</div>

</div>