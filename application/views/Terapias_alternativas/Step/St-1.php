<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
<script src="<?=base_url('library/js/jQuery.js')?>"></script>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #FF3DD2;">
<div class="container">
<div class="column">

<h1 style="color: white;">Terapias alternativas</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?= base_url('Home') ?>" style="color: white;">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li style="color: white;">Terapias alternativas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->

<!-- CUADRO 1-->
<div class="row">
<!-- Checkout Adress-->
<div class="col-xl-12 col-lg-12">

<!-- Direcciones-->
<div class="checkout-steps">
<a class="disable_link" id="resumen" href="<?=base_url('Terapias_alternativas/St4')?>">4. Resumen</a>
<a class="disable_link" id="datos_consulta" href="<?=base_url('Terapias_alternativas/St3')?>"><span class="angle"></span>3. Datos de la consulta</a>

<a class="disable_link" id="datos_personales" href="<?=base_url('Terapias_alternativas/St2')?>"><span class="angle disable-links"></span>2. Datos Personales</a>

<a class="active" href="<?=base_url('Terapias_alternativas/Inicio')?>"><span class="angle"></span>1. Consulta</a>
</div>
<!-- Direcciones-->

<h4>Tipo de consulta:</h4>

<hr class="padding-bottom-1x">


<div class="row mt-2">
<div class="col-12">
<form>
<div class="form-group row justify-content-center">

<div class="card-deck">

<!-- ///////////////////////////////////////////// -->
<div id="elephant-card" class="card mb-4">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="elephant" name="consulta" type="radio" value="Acupuntura" class="d-none">
<label for="elephant" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Acupuntura</span></label>
</h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$250.00</span>
</div>
<p class="card-text text-center">Técnica terapéutica milenaria originaria de la Medicina Tradicional China, la cual consiste en la aplicación de agujas en puntos específicos del cuerpo para tratar enfermedades y promover la salud.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////// -->
<div id="lion-card" class="card mb-4">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="lion" name="consulta" type="radio" value="Ventosas" class="d-none"> 
<label for="lion" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Ventosas</span></label>
</h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; font-weight: 400;">$300.00</span>
</div>
<p class="card-text text-center">Masaje con ventosas consiste en aplicar ventosas sobre el cuerpo, a las que se les extrae el aire, y que se manipulan con el fin de eliminar toxinas de la sangre y la linfa, o tratar diversas afecciones.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->


<!-- ///////////////////////////////////////////// -->
<div id="zebra-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="zebra" name="consulta" type="radio" value="Auriculoterapia" class="d-none"> 
<label for="zebra" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Auriculoterapia</span></label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$250.00</span>
</div>
<p class="card-text text-center">Proviene etimológicamente del griego terapia que significa curar y del latín aurícula que quiere decir oreja, se presenta como una rama de la acupuntura por ser practicada por medio de las agujas chinas.</p>
</div>
</div>

</div>
<!-- ///////////////////////////////////////////// -->
<div class="w-100"></div>

<div class="card-deck">
<!-- ///////////////////////////////////////////// -->
<div id="giraffe-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="giraffe" name="consulta" type="radio" value="Conoterapia" class="d-none"> 
<label for="giraffe" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Conoterapia</span></label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$250.00</span>
</div>
<p class="card-text text-center">Técnica para la desintoxicación y limpieza de oídos. Se utilizan conos de cera que se colocan en el interior de los oídos y ayudan a desincrustar, drenar sustancias y extraer microrganismos de las cavidades auditivas.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->


<!-- ///////////////////////////////////////////// -->
<div id="hyena-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="hyena" name="consulta" type="radio" value="Terapia de imanes" class="d-none"> 
<label for="hyena" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Terapia de imanes</span></label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$250.00</span>
</div>
<p class="card-text text-center">Consiste en el reconocimiento de puntos de energía alterados en nuestro organismo que en conjunto están dando origen a una enfermedad o malestar de curso agudo o crónico.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////// -->
<div id="meerkat-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="meerkat"name="consulta"  type="radio" value="Flores mexicanas" class="d-none"> 
<label for="meerkat" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Flores mexicanas</span>
</label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$150.00</span>
</div>
<p class="card-text">Terapia floral parte de la idea de que los desequilibrios emocionales son el origen de la enfermedades físicas y mentales, por lo que "promueve un método de tratamiento capaz de armonizarlos.</p>
</div>
</div>

</div>
<!-- ///////////////////////////////////////////// -->


<div class="card-deck">
<!-- ///////////////////////////////////////////// -->
<div id="meerkat_1-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="meerkat_1"name="consulta"  type="radio" value="Terapia neural" class="d-none"> 
<label for="meerkat_1" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Terapia neural</span>
</label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$300 X Sesión</span>
</div>
<p class="card-text">Disciplina que se encarga de proporcionar un acompañamiento emocional para trabajar el proceso de duelo, principalmente de una muerte. También se encarga de los duelos derivados de pérdidas significativas</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////// -->
<div id="meerkat_2-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="meerkat_2"name="consulta"  type="radio" value="Alineacion de chakras" class="d-none"> 
<label for="meerkat_2" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Alineación de chakras</span>
</label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$450.00</span>
</div>
<p class="card-text">Alinear los chakras o centros energéticos, es devolver la vitalidad, equilibrio y armonía al cuerpo físico, emocional, mental y espiritual. Cuando la energía vital circula de forma positiva nos sentimos felices y sanos.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->

<!-- ///////////////////////////////////////////// -->
<div id="meerkat_3-card" class="card mb-4 ">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="meerkat_3"name="consulta"  type="radio" value="Masaje terapeutico" class="d-none"> 
<label for="meerkat_3" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Masaje terapéutico</span>
</label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$500.00</span>
</div>
<p class="card-text">El masaje es una herramienta poderosa para el beneficio del complejo psicofisiológico del paciente. He aquí los efectos positivos generales de la aplicación de masajes</p>
</div>
</div>

</div>
<!-- ///////////////////////////////////////////// -->

<
<div class="card-deck">
<!-- ///////////////////////////////////////////// -->
<div id="meerkat_4-card" class="card mb-4 col-4">
<div class="card-body" role="button">
<h3 class="card-title">
<input id="meerkat_4"name="consulta"  type="radio" value="Masaje reductivo" class="d-none"> 
<label for="meerkat_4" style="font-size: 20px;" class="control-label col-sm-12 d-flex justify-content-center align-items-center"><span class="badge badge-warning text-center" style="font-size: 20px;color: black; ">Masaje reductivo</span>
</label></h3>
<div class="row justify-content-center mb-2">
<span class="text-center" style="font-size: 15px;color: black; ">$450.00</span>
</div>
<p class="card-text">Se realiza con una presión fuerte y una velocidad más rápida que un masaje relajante. Se puede hacer en cualquier parte del cuerpo: cintura, caderas, piernas, abdomen y debajo de la barbilla.</p>
</div>
</div>
<!-- ///////////////////////////////////////////// -->

</div>

</div>
</div>
</div>
</form>
</div>
</div>


<div class="checkout-footer">
<div class="column"><a class="btn btn-primary invisible" id="boton_siguiente" href="<?=base_url('Terapias_alternativas/St2')?>"><span class="hidden-xs-down">Siguiente&nbsp;</span><i class="icon-arrow-right"></i></a></div>
</div>
</div>
<!-- Sidebar          -->

</div>
<!-- CUADRO 1-->



</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>


<style type="text/css">

.active {
border-color: #FF3DD2 !important;
border-width: 4px;
background-color: white;
box-shadow: 0 3px 6px rgba(0, 0, 0, 0.18), 0 3px 6px rgba(0, 0, 0, 0.23);
}

.card {
box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15), 0 2px 5px rgba(0, 0, 0, 0.2);
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
}


</style>

<script type="text/javascript">

$(document).ready(function () {
$("#Tipo_consulta").text(sessionStorage.getItem('Consulta'));
$('input:radio').change(function () {//Clicking input radio
var radioClicked = $(this).attr('id');
unclickRadio();
removeActive();
clickRadio(radioClicked);
makeActive(radioClicked);
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
});
$(".card").click(function () {//Clicking the card
var inputElement = $(this).find('input[type=radio]').attr('id');
unclickRadio();
removeActive();
makeActive(inputElement);
clickRadio(inputElement);
var consulta = $("input[name='consulta']:checked").val();
sessionStorage.setItem('Consulta',consulta);
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
});
});


function unclickRadio() {
$("input:radio").attr("checked", false);
}

function clickRadio(inputElement) {
$("#" + inputElement).attr("checked", true);
}

function removeActive() {
$(".card").removeClass("active");
}

function makeActive(element) {
$("#" + element + "-card").addClass("active");
}

</script>


<style type="text/css">

.card:hover {
border-color: transparent;
transition: 0.5s; 
border-width: 5px;
}
</style>


<script type="text/javascript">
  
$(document).ready(function () 
{
var valor = sessionStorage.getItem('Consulta');
if(valor != '')
{

switch(valor)
{
case 'Acupuntura':
$("#elephant-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Ventosas':
$("#lion-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Auriculoterapia':
$("#zebra-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Conoterapia':
$("#giraffe-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Terapia de imanes':
$("#hyena-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("invisible");
break;
case 'Flores mexicanas':
$("#meerkat-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Terapia neural':
$("#meerkat_1-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Alineacion de chakras':
$("#meerkat_2-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Masaje terapeutico':
$("#meerkat_3-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
case 'Masaje reductivo':
$("#meerkat_4-card").addClass("active");
$("#boton_siguiente").removeClass("invisible");
$("#datos_personales").removeClass("disable_link");
break;
}

}

});

</script>

<style type="text/css">

.disable_link
{
pointer-events: none; 
display: inline-block;
}

.checkout-steps>a.active>.angle::after {
border-left-color: #FF3DD2;
}


.checkout-steps>a.active {
background-color: #FF3DD2;
color: #fff;
cursor: default;
pointer-events: none;
}

</style>



</body>
</html>

