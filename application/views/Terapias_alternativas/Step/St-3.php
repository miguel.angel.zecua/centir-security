<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #FF3DD2;">
<div class="container">
<div class="column">

<h1 style="color: white;">Terapias alternativas</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?= base_url('Home') ?>" style="color: white;">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li style="color: white;">Terapias alternativas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->


<!-- CUADRO 1-->
<div class="row">
<!-- Checkout Adress-->
<div class="col-xl-9 col-lg-8">

<!-- Direcciones-->
<div class="checkout-steps">
<a class="invisible" id="resumen" href="<?=base_url('Terapias_alternativas/St4')?>">4. Resumen</a><a class="active" href="checkout-payment.html">
<span class="angle"></span>3. Datos de la consulta</a><a class="completed" href="<?=base_url('Terapias_alternativas/St2')?>"><span class="step-indicator icon-circle-check"></span>
<span class="angle"></span>2. Datos Personales</a><a class="completed" href="<?=base_url('Terapias_alternativas/Inicio')?>">
<span class="step-indicator icon-circle-check"></span>
<span class="angle"></span>1. Consulta</a></div>           
<!-- Direcciones-->


<h4>Selecione sus datos de consulta</h4>
<hr class="padding-bottom-1x">
<form id="formulario_2">

<div class="row">

<div class="col-sm-6">
<div class="form-group">
<label for="color">Fecha</label>
<input type='date' class="form-control" id="fecha" value="<?php echo date('Y-m-d')?>" min="<?php echo date('Y-m-d')?>">
</div>
</div>


<div class="col-sm-6">
<div class="form-group">
<label for="color">Horario</label>
<select class="form-control" name="horario" id="horario" >
<option value=""></option>
<option value="09-00-AM">09:00 AM-10:00 AM</option>
<option value="10-00-AM">10:00 AM-11:00 AM</option>
<option value="11-00-AM">11:00 AM-12:00 PM</option>
<option value="12-00-PM">12:00 PM-01:00 PM</option>
<option value="01-00-PM">01:00 PM-02:00 PM</option>
<option value="02-00-PM">02:00 PM-03:00 PM</option>
<option value="03-00-PM">03:00 PM-04:00 PM</option>
</select>
</div>
</div>

</div>



<div class="row">
	<div class="col-sm-12 col-md-12">
 <div class="form-group">
    <label for="exampleFormControlTextarea1">Motivos de su consulta:</label>
    <textarea class="form-control" id="Motivos" name="Motivos" rows="4"></textarea>
  </div>
	</div>
</div>
</form>

<div class="checkout-footer margin-top-1x">

<div class="column"><a class="btn btn-outline-secondary" href="<?=base_url('Terapias_alternativas/St2')?>"><i class="icon-arrow-left"></i><span class="hidden-xs-down">&nbsp;Regresar</span></a></div>

<div class="column"><a class="btn btn-primary invisible" id="siguiente"><span class="hidden-xs-down">Siguiente&nbsp;</span><i class="icon-arrow-right"></i></a></div>

</div>
</div>

<!-- Sidebar          -->
<div class="col-xl-3 col-lg-4">
<aside class="sidebar">
<div class="padding-top-2x hidden-lg-up"></div>

<!-- Orden-->
<section class="widget widget-order-summary">
<h3 class="widget-title">Su consulta:</h3>
<table class="table">
<tbody>
<tr>
<td>Tipo:</td>
<td class="text-lg"  id="Tipo_consulta" style="font-weight: bold;"></td>
</tr>

<tr>
<td>Nombre:</td>
<td class="text-lg"  id="Nombre_main" style="font-size: 14px;"></td>
</tr>

<tr>
<td>Edad:</td>
<td class="text-lg"  id="Edad_main" style="font-size: 14px;"></td>
</tr>

<tr>
<td>Correo:</td>
<td class="text-lg"  id="Correo_main" style="font-size: 14px;"></td>
</tr>

<tr>
<td>Télefono:</td>
<td class="text-lg"  id="Telefono_main" style="font-size: 14px;"></td>
</tr>

</tbody></table>
</section>
<!-- Orden-->

</aside>
</div>
</div>
<!-- CUADRO 1-->



</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>


<script type="text/javascript">
$( document ).ready(function() 
{
$("#Tipo_consulta").text(sessionStorage.getItem('Consulta'));

$("#Nombre_main").text(sessionStorage.getItem('Nombre_main'));
$("#Edad_main").text(sessionStorage.getItem('Edad_main'));
$("#Correo_main").text(sessionStorage.getItem('Correo_main'));
$("#Telefono_main").text(sessionStorage.getItem('Telefono_main'));
});

</script>


<script type="text/javascript">


const campos = 
{
horario: false
}
 

//////////////////////////////////////    
$('#horario').change(function(e) 
{
$('#horario').find("option:selected").each(function() 
{
if ($(this).val().trim() == '') 
{
document.getElementById('horario').classList.remove('is-valid');
document.getElementById('horario').classList.add('is-invalid');
campos['horario'] = false;   
}

else
{
document.getElementById('horario').classList.remove('is-invalid');
document.getElementById('horario').classList.add('is-valid');
campos['horario'] = true;
}
});
})
////////////////////////////////////// 
$('#formulario_2').change(function(e) 
{

if(campos.horario == true)
{
$("#siguiente").removeClass("invisible");
$("#resumen").removeClass("invisible");
}

else
{
$("#siguiente").addClass("invisible");
$("#resumen").addClass("invisible");
}

})
////////////////////////////////////// 


$( "#siguiente" ).click(function() 
{
sessionStorage.setItem('Horario_main',$("#horario").val());
sessionStorage.setItem('Fecha_main',$("#fecha").val());
sessionStorage.setItem('Motivos',$("#Motivos").val());
window.location.href = 'St4';
});

$( "#resumen" ).click(function() 
{
sessionStorage.setItem('Horario_main',$("#horario").val());
sessionStorage.setItem('Fecha_main',$("#fecha").val());
sessionStorage.setItem('Motivos',$("#Motivos").val());
window.location.href = 'St4';
});

</script>

<script type="text/javascript">
    
$( document ).ready(function() {

if(sessionStorage.getItem('Horario_main') != "")
{

$("#horario option[value="+ sessionStorage.getItem('Horario_main') +"]").attr("selected",true);
$("#fecha").val(sessionStorage.getItem('Fecha_main'));
$("#siguiente").removeClass("invisible");
$("#resumen").removeClass("invisible");
}

});
</script>


<style type="text/css">
	
.disable_link
{
pointer-events: none; 
display: inline-block;
}

.checkout-steps>a.active>.angle::after {
    border-left-color: #FF3DD2;
}

.checkout-steps>a.active+a>.angle {
    background-color: #FF3DD2;
}


.checkout-steps>a.active {
    background-color: #FF3DD2;
    color: #fff;
    cursor: default;
    pointer-events: none;
}

</style>
</body>
</html>
