<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #FF3DD2;">
<div class="container">
<div class="column">

<h1 style="color: white;">Terapias alternativas</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?= base_url('Home') ?>" style="color: white;">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li style="color: white;">Terapias alternativas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->


<!-- CUADRO 1-->
<div class="row">
<!-- Checkout Adress-->
<div class="col-xl-9 col-lg-8">

<!-- Direcciones-->
<div class="checkout-steps">
<a class="disable_link" href="checkout-review.html" id="resumen">4. Resumen</a>
<a class="disable_link" id="datos_consulta" href="<?=base_url('Terapias_alternativas/St3')?>">
<span class="angle"></span>3. Datos de la consulta</a>
<a class="active" href="">
<span class="angle"></span>2. Datos Personales</a>
<a class="completed" href="<?=base_url('Terapias_alternativas/Inicio')?>">
<span class="angle"></span>
<span class="step-indicator icon-circle-check"></span>1. Consulta</a></div>            
<!-- Direcciones-->


<h4>Escriba sus datos personales </h4>
<hr class="padding-bottom-1x">

<form id="formulario">
<div class="row" id="fila_1">
<div class="col-sm-6">
<div class="form-group">

<label for="size" style="font-size: 16px;">Nombre del paciente</label>
<input class="form-control" type="text" placeholder="Nombre Completo" id="nombre" name="nombre" onkeyup="mayus(this);">
</div>
</div>

<div class="col-sm-6">
<div class="form-group">

<label for="size" style="font-size: 16px;">Edad</label>
<input class="form-control " type="number" placeholder="Edad" name="edad" id="edad">
</div>
</div>

</div>


<div class="row">

<div class="col-sm-6">
<div class="form-group">

<label for="size" style="font-size: 16px;">Correo</label>
<input class="form-control" type="email" placeholder="Correo Electrónico" name="correo" id="correo">
</div>
</div>

<div class="col-sm-6">
<div class="form-group">

<label for="size" style="font-size: 16px;">Teléfono </label>
<input class="form-control" type="text"  placeholder="Teléfono" name="telefono" id="telefono">
</div>
</div>
</div>


<div class="checkout-footer margin-top-1x">

<div class="column"><a class="btn btn-outline-secondary" href="<?=base_url('Terapias_alternativas/Inicio')?>"><i class="icon-arrow-left"></i><span class="hidden-xs-down">&nbsp;Regresar</span></a></div>

<div class="column"><a class="btn btn-primary invisible" id="siguiente" href="<?=base_url('Terapias_alternativas/St3')?>"><span class="hidden-xs-down">Siguiente&nbsp;</span><i class="icon-arrow-right"></i></a></div>

</form>

</div>
</div>

<!-- Sidebar          -->
<div class="col-xl-3 col-lg-4">
<aside class="sidebar">
<div class="padding-top-2x hidden-lg-up"></div>

<!-- Orden-->
<section class="widget widget-order-summary">
<h3 class="widget-title">Su consulta:</h3>
<table class="table">
<tbody><tr>
<td>Tipo:</td>
<td class="text-lg"  id="Tipo_consulta" style="font-weight: bold;"></td>
</tr>
</tbody></table>
</section>
<!-- Orden-->

</aside>
</div>
</div>
<!-- CUADRO 1-->



</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

<script src="<?=base_url('library/js/Consultas/St-2.js')?>"></script>

<script type="text/javascript">
    
$( document ).ready(function() 
{
$("#Tipo_consulta").text(sessionStorage.getItem('Consulta'));
if(sessionStorage.getItem('Nombre_main') == " ")
{

}

else
{
$("#nombre").val(sessionStorage.getItem('Nombre_main'));
$("#edad").val(sessionStorage.getItem('Edad_main'));
$("#correo").val(sessionStorage.getItem('Correo_main'));
$("#telefono").val(sessionStorage.getItem('Telefono_main'));
$("#datos_consulta").removeClass("disable_link");
$("#siguiente").removeClass("invisible");
}

});



</script>


<style type="text/css">
	
.disable_link
{
pointer-events: none; 
display: inline-block;
}

.checkout-steps>a.active>.angle::after {
    border-left-color: #FF3DD2;
}

.checkout-steps>a.active+a>.angle {
    background-color: #FF3DD2;
}


.checkout-steps>a.active {
    background-color: #FF3DD2;
    color: #fff;
    cursor: default;
    pointer-events: none;
}

</style>



</body>
</html>
