<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title">
<div class="container">
<div class="column">

<h1>Terapias alternativas</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li>Terapias alternativas</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view " href="<?=base_url('Terapias_alternativas')?>"><span></span><span></span><span></span></a>
<a class="list-view active" href="<?=base_url('Terapias_alternativas/List')?>"><span></span><span></span><span></span></a></div>
</div>

</div>

<!-- Fila-->
<div class="row justify-content-center">

<div class="col-lg-11">


<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Acupuntura')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Acupuntura-min.png')?>" alt="Product"></a>
<div class="product-info">Acupuntura</h3>

<h4 class="product-price">$250.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Acupuntura'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Ventosas')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Ventosas-min.png')?>" alt="Product"></a>
<div class="product-info">Ventosas</h3>

<h4 class="product-price">$300.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Ventosas'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Auriculoterapia')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Auriculoterapia-min.png')?>" alt="Product"></a>
<div class="product-info">Auriculoterapia</h3>

<h4 class="product-price">$250.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Auriculoterapia'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Conoterapia')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Conoterapia-min.png')?>" alt="Product"></a>
<div class="product-info">Conoterapia</h3>

<h4 class="product-price">$250.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conoterapia'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Terapia_imanes')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Imanes-min.png')?>" alt="Product"></a>
<div class="product-info">Terapia de imanes</h3>

<h4 class="product-price">$250.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapia_imanes'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Flores_mexicanas')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Flores-min.png')?>" alt="Product"></a>
<div class="product-info">Flores mexicanas</h3>

<h4 class="product-price">$150.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Flores_mexicanas'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Terapia_neural')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Neural-min.png')?>" alt="Product"></a>
<div class="product-info">Terapia neural</h3>

<h4 class="product-price">$350.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapia_neural'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Alineacion_chakras')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Chakras-min.png')?>" alt="Product"></a>
<div class="product-info">Alineación de chakras</h3>

<h4 class="product-price">$450.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Alineacion_chakras'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Masaje_terapeutico')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Terapeutico-min.png')?>" alt="Product"></a>
<div class="product-info">Masaje terapéutico</h3>

<h4 class="product-price">$500.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Masaje_terapeutico'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Terapias_alternativas/Masaje_reductivo')?>">
<img src="<?=base_url('library/img/Categorias/Alternativas/Reductivo-min.png')?>" alt="Product"></a>
<div class="product-info">Masaje reductivo</h3>
<h4 class="product-price">$450.00</h4>

<p class="hidden-xs-down">Su objetivo consiste en desarrollar conocimientos acerca de la acupuntura; técnica milenaria de la MTC, con el propósito de desarrollar habilidades para el tratamiento de diversos síndromes y así propiciar armonía en el plano físico, mental y espiritual de las personas que gocen de sus beneficios.</p>
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Masaje_reductivo'">Agendar</button>
</div>
</div>
</div>
<!-- Product-->


</div>
       
</div>
<!-- Fila-->

</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>

