<div class="accordion mb-4" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Cat_4" data-toggle="collapse" style="font-size: 18px">Pedagogía</a></h6>
</div>

<div class="collapse" id="Cat_4"  role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">


<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2 d-none">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Cerebro_confinamiento')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Pedagogia/Cerebro-confinamiento-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Cerebro_confinamiento'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->
	
</div>
<!-- Fila 1-->

<!-- Body-->
</div>
</div>
</div>
</div>