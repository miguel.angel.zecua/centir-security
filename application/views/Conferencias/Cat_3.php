<div class="accordion mb-4" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Cat_3" data-toggle="collapse" style="font-size: 18px">Psicología</a></h6>
</div>

<div class="collapse" id="Cat_3"  role="tabpanel">
<div class="card-body">	
<!-- Body-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">



<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-2 d-none">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Perdida_perdon')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Perdida-perdon-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Perdida_perdon'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Defensa_dolor')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Defensa-dolor-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Defensa_dolor'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->

	
</div>
<!-- Fila 1-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3 mt-2">


<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-2 d-none">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Conociendo_construyendo')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Conociendo-construyendo-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Conociendo_construyendo'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2 d-none">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Duelo_Covid_19')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Duelo-Covid-19-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Duelo_Covid-19'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->
	
</div>
<!-- Fila 1-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 m-2 d-none">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Circulo_vicioso')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Circulo-vicioso-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Circulo_vicioso'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

	
</div>
<!-- Fila 1-->

<!-- Body-->
</div>
</div>
</div>
</div>