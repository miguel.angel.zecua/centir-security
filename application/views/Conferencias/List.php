<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->
<div class="page-title" style="background-color: #4A46B3;">
<div class="container">
<div class="column">

<h1 style="color: white;">Transmisiones</h1>
</div>
<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li style="color: white;">Transmisiones</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<!-- Shop Toolbar-->
<div class="shop-toolbar padding-bottom-1x mb-2">
<div class="column">

<div class="shop-sorting invisible">
<label for="sorting">Ordenar por:</label>
<select class="form-control" id="sorting">
<option>Popularidad</option>
<option>Bajo - Alto Precio</option>
<option>Alto - Bajo Precio</option>
<option>Puntuacion</option>
<option>A - Z </option>
<option>Z - A </option>
</select><span class="text-muted">Mostrando:&nbsp;</span><span>1 - 3 Categorias</span>
</div>
</div>

<div class="column">
<div class="shop-view">
<a class="grid-view d-none " href="<?=base_url('Conferencias')?>"><span></span><span></span><span></span></a>
<a class="list-view active" href="<?=base_url('Conferencias/List')?>"><span></span><span></span><span></span></a></div>
</div>

</div>

<!-- Fila-->
<div class="row justify-content-center">

<div class="col-lg-11">

<div class="p-3 mb-2 bg-info text-dark text-center rounded mb-3"><b style="font-size: 18px">Forense</b></div>

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Conferencias/Prevencion_suicidio')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Prevencion_suicidio-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Prevención del suicidio</h3>

<p>
El suicidio es un grave problema de salud pública rodeado de estigmas, mitos y tabúes. Cada año cerca de 800.000 personas se quitan la vida a nivel mundial, lo que equivale a que a una persona muere por suicidio cada 40 segundos.
</p>

<p>En esta conferencia abordaremos los factores relacionados con el suicidio desde una perspectiva criminológica enfocándonos en la prevención del suicidio con la Lic. Melissa Moreno González.
</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Prevencion_suicidio'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Conferencias/Conductas_destructivas')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Conductas_destructivas-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Conductas destructivas en la niñez y adolescencia</h3>

<p>La adolescencia es una etapa complicada de cambio donde, muchas veces, el cuerpo se desarrolla antes que la mente esté preparada para ello. Esto provoca una acumulación de emociones y sentimientos que al ser retenidos pueden provocar problemas para el adolescente.
</p>

<p>Descubre a través de esta conferencia como es que los problemas para manejar las emociones pueden llevar a cometer conductas autodestructivas en la adolescencia con la Lic. Melissa Moreno González.
</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conductas_destructivas'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Funcion_criminalista')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Funcion_criminalista-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">La función del criminalista en la necropsia</h3>

<p>La Necropsia es aquel estudio realizado a un cadáver con la finalidad de investigar y determinar las causas de su muerte, Cabe mencionar que los criminalistas no realizarán jamás una necropsia, esta tarea es exclusivamente del médico forense, sin embargo, los criminalistas intervienen con sus aportaciones y hallazgos en la escena de intervención y en sus diferentes áreas para coadyuvar al médico forense en la investigación.
</p>

<p>Descubre más sobre la importancia del criminalista en la investigación médico forense con la Lic. Cynthia Coronas Moreno.
</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Funcion_criminalista'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Investigacion_forense')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Investigacion_forense-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Investigación forense de artefacto explosivos y peligrosos</h3>

<p>En la actualidad cada vez son más usados los artefactos explosivos hechos de forma improvisada, por lo que dentro de la investigación forense es necesario entender como funcionan estos explosivos, y de que manera poder reducir los daños o desactivar dichos artefactos.
</p>
<p></p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Investigacion_forense'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Conferencias/Factor_criminologico')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Factor_criminologico-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">La familia como factor criminológico</h3>

<p>La familia como factor criminógeno es “la unidad básica de las conductas antisociales, la cual formará la personalidad violenta y agresiva, así como la manera incorrecta de relacionarse con el medio o la sociedad, agrediendo a los demás por las causas de sus conflictos internos.</p>

<p>Aprende de qué manera influye el ambiente familiar en el desarrollo de conductas antisociales con la Lic. Rosalba Flores Gaspar.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Factor_criminologico'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Investigacion_criminal')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Investigacion_criminal-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Como inicia una investigación criminal en México</h3>

<p>La investigación criminal es una ciencia aplicada que implica el estudio de hechos que luego se utilizan para informar los juicios penales. Una investigación criminal completa puede incluir búsquedas, entrevistas, interrogatorios, recolección y preservación de evidencia, y varios métodos de investigación.</p>

<p>Descubre cuales son los momentos y autoridades que conforman una Investigación Criminal en México a través de esta conferencia con la Lic. Daniela Zúñiga Pérez.
</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Investigacion_criminal'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Trabajo_interdisciplinario')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Trabajo_interdisciplinario-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Soñar no cuesta nada: “El trabajo interdisciplinario con adolescentes en internamiento"</h3>

<p>Con el incremento de la inseguridad y la criminalidad, cada vez son más los jóvenes que se ven involucrados en actos delictivos, dentro del sistema de justicia se les define como "Menores Infractores" pero, ¿Cuáles son las pautas para trabajar con Menores Infractores? ¿De que manera se puede lograr la reinserción de un Menor Infractor</p>
<p></p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Trabajo_interdisciplinario'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Delincuentes_infancia')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Delincuentes_infancia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">¿Cómo formamos delincuentes desde la infancia?</h3>

<p>Alguna vez te has preguntado ¿Cómo influye la familia, el entorno social y las instituciones Educativas en el desarrollo de conductas dañinas en niños y adolescentes? ¿Qué responsabilidad tenemos como padres, tutores en la formación de delincuentes? O ¿Qué acciones pueden realizar las Instituciones educativas para prevenir conductas delictivas en menores?
</p>

<p>Descubre la respuesta a estas preguntas con la Lic. Daniela Zúñiga Pérez.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Delincuentes_infancia'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Apoyo_balistica')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Apoyo_balistica-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Apoyo a la balística forense en el apoyo criminal</h3>

<p>Una de las aportaciones de balística dentro de la investigación criminal se encuentra presente desde su parte experimental y práctica, ayuda a investigar la realización de un hecho punible donde han sido empleadas armas de fuego, mediante los diferentes elementos como pueden ser proyectiles, fragmentos, vainillas, municiones, para establecer factores como la procedencia entre los elementos materiales probatorios recolectados y las evidencias físicas encontradas en las victimas.
</p>

<p>En esta conferencia aprenderás como participa la Balística y sus diferentes áreas en una Investigación Criminal con el Lic. Joel Sosa Domínguez.
</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Apoyo_balistica'">Unirme</button>
</div>
</div>
<!-- Product-->

<div class="p-3 mb-2 bg-info text-dark text-center rounded  mt-2 mb-3"><b style="font-size: 18px">Jurídicos</b></div>

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Abogado_Perito')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Abogado_Perito-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Abogado vs Perito en el juicio oral</h3>

<p>Con la introducción del Nuevo Sistema Penal Acusatorio, los roles dentro de dicho sistema se vieron modificados, por un lado, El perito en el juicio oral debe, primeramente, conocer todos los aspectos teóricos y prácticos de su disciplina, y saber comunicar de manera clara, objetiva y convincente los elementos probatorios puestos a su experticia tanto por la vía escrita como oral, Mientras que el Abogado, deberá cuestionar como estos elementos desde la perspectiva del cómo son presentados.
</p>

<p>Descubre más sobre el rol del Perito y Abogado en esta conferencia impartida por el Mtro. Arturo H. Sánchez George.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Abogado_Perito'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Conferencias/Importancia_ortografia')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Importancia_ortografia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Importancia de la ortografía, redacción y argumentación en el ámbito Jurídico</h3>

<p>Una de los aspectos más importantes y uno de los más descuidados dentro de la Argumentación Jurídica es la Ortografía y la forma en la realizamos un escrito, para cualquier persona es indispensable saber comunicarse, pero dentro del ámbito jurídico es crucial y estrictamente necesario pues es a través de esta habilidad que se pueden ganar o perder asuntos legales de cualquier tipo.</p>

<p>En esta conferencia aprenderás como identificar problemas de ortografía y como desarrollar las habilidades necesarias para la argumentación Jurídica con la Lic. María Soledad Esquivel Romero.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Importancia_ortografia'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Conferencias/Soluciones_alternas')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Soluciones_alternas-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Soluciones alternas del procedimiento penal</h3>

<p>Dentro del Nuevo Sistema de Justicia Penal existen diferentes alternativas que permiten finalizar el proceso sin la necesidad de tener que llegar a un juicio, estas alternativas permiten que la victima u ofendido tenga la posibilidad de que le sea reparado el daño, en medida de lo posible, así como permiten ahorrar tiempo y recursos de ambas partes.</p>
<p></p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Soluciones_alternas'">Unirme</button>
</div>
</div>
<!-- Product-->

<div class="p-3 mb-2 bg-info text-dark text-center rounded  mt-2 mb-3"><b style="font-size: 18px">Psicología</b></div>


<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Conferencias/Perdida_perdon')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Perdida_perdon-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">La perdida y el perdón</h3>

<p>El Duelo incluye todo el proceso emocional de enfrentarse a una pérdida, y su duración puede ser prolongada. Este proceso involucra emociones, acciones y expresiones diferentes, y todas ellas ayudan a la persona a aceptar la pérdida de un ser amado.</p>

<p>Descubre más del Proceso de Duelo, sus implicaciones, sus características y cómo pasar por este proceso de una manera sana con la licenciada en Psicología María Magdalena Ochoa Medina y la Licenciada en Antropología Forense Lisa Edith Velázquez Peláez.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Perdida_perdon'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Defensa_dolor')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Defensa_dolor-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Defensa ante el dolor</h3>

<p>¿Sabías que los seres humanos estamos programados para evitar el dolor? A través de mecanismos de defensa el ser humano evita situaciones que le generan malestar. </p>

<p>Descubre más del como usamos los mecanismos de defensa para evadir el dolor, sus implicaciones, sus características con el licenciado Víctor Hugo Murgo Tiscareño.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Defensa_dolor'">Unirme</button>
</div>
</div>
<!-- Product-->


<div class="p-3 mb-2 bg-info text-dark text-center rounded  mt-2 mb-3"><b style="font-size: 18px">Pedagogía</b></div>




<!-- Product-->
<div class="product-card product-list d-none">
<a class="product-thumb" href="<?=base_url('Conferencias/Cerebro_confinamiento')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Pedagogia/Cerebro_confinamiento-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">¿Cómo aprende el cerebro en tiempos de confinamiento?</h3>

<p>El ser humano, junto con otros seres vivos, tiene la capacidad de aprender gracias a los estímulos que hay en su entorno, gracias al sistema nervioso, Para favorecer el aprendizaje, nuestro cerebro es capaz de formar más sinapsis para responder mejor a los estímulos, mecanismo que llamamos aprendizaje.
</p>

<p>En esta conferencia descubriremos cuales son los procesos que favorecen el aprendizaje y cómo influye el confinamiento en el proceso con la Mtra. Jenne Hernández Romero.
</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cerebro_confinamiento'">Unirme</button>
</div>
</div>
<!-- Product-->

<div class="p-3 mb-2 bg-info text-dark text-center rounded  mt-2 mb-3"><b style="font-size: 18px">Terapias alternativas</b></div>

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Influencia_emociones')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Influencia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">La influencia de las emociones en la salud</h3>

<p>Las emociones que experimentamos día a día tienen un gran impacto sobre nuestro estado de salud e influyen tanto en la manera en la que nos enfermemos como en el proceso de sanación, por lo que es importante saber identificar las emociones para lograr sanar de forma más rápida y eficaz.
</p>

<p>En esta conferencia aprenderás como identificar las emociones que causan conflicto con tu estado de salud y que hacer para empezar a sanar con el Lic. Víctor Hugo Murgo Tiscareño.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Influencia_emociones'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Terapia_dolor')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Terapia-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Terapia neural: “La terapia contra el dolor”</h3>

<p>La Terapia Neural es una forma de medicina alternativa con la que se estimulan las acciones que el cuerpo posee para auto curarse, en otras palabras: el cuerpo de cada paciente tiene la capacidad de auto defenderse, auto curarse y mantenerse sano.</p>

<p>Descubre más sobre la Terapia Neural y sus Beneficios en el tratamiento de dolores y enfermedades con la Mtra. Mónica Espíndola Buendía.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Terapia_dolor'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Alimentos_funcionales_enfermedades')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Alimentos-min.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Alimentos funcionales: Promueven el riesgo a presentar enfermedades</h3>

<p>Los alimentos funcionales son aquellos que tienen un efecto potencialmente positivo en la salud más allá de la nutrición básica, debido a que promueven una salud óptima y ayudan a reducir el riesgo de padecer enfermedades.</p>

<p>Descubre en esta conferencia que son los Alimentos Funcionales, como podemos integrarlos a nuestra alimentación diaria y de los beneficios a la salud con la Mtra. en Salud Publica Libia Darina Dosamantes Carrasco.</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Alimentos_funcionales_enfermedades'">Unirme</button>
</div>
</div>
<!-- Product-->

<!-- Product-->
<div class="product-card product-list">
<a class="product-thumb" href="<?=base_url('Conferencias/Agua_Magnetizada')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Agua-Magnetizada.png')?>" alt="Product"></a>
<div class="product-info">
<h3 class="product-title">Agua Magnetizada</h3>

<p>El uso de Agua Magnetizada para la Salud.
Presentado por el Lic. Victor Hugo Murgo Tiscareño</p>

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Agua_Magnetizada'">Unirme</button>
</div>
</div>
<!-- Product-->

<div class="alert alert-primary text-center" role="alert" style="border-radius: 25px; font-size: 25px; color: black; font-weight: bold; padding: 10px;">
<a href="https://open.spotify.com/show/3rhYRZwad79IYWDh83zvxm" style="text-decoration: none; color: black;">SPOTIFY</a>
</div>


</div>
       
</div>
<!-- Fila-->

</div>

<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>

</body>
</html>


