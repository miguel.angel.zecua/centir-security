<div class="accordion mb-4" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Cat_5" data-toggle="collapse" style="font-size: 18px">Terapias Alternativas</a></h6>
</div>

<div class="collapse" id="Cat_5"  role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">   
<a class="product-thumb" href="<?=base_url('Conferencias/Influencia_emociones')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Influencia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Terapia_Imanes'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Terapia_dolor')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Terapia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Medicina_Cannabica'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Alimentos_funcionales_enfermedades')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Alternativas/Alimentos-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Cursos/Auriculoterapia'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->
	
</div>
<!-- Fila 1-->

<!-- Body-->	
</div>
</div>
</div>
</div>