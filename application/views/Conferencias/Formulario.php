<div class="p-3 mb-2 bg-secondary text-dark rounded text-center mt-0"><b>CONFIGURE SUS DATOS PARA CONSTANCIA</b></div>
<form id="formulario">
<div class="row">
<div class="col-sm-6">
<div class="form-group">

<label for="size">Nombre Completo</label>
<input class="form-control" type="text" id="nombre" name="nombre" placeholder="Nombre Completo" onkeyup="mayus(this);">
</div>
</div>

<div class="col-sm-6">
<div class="form-group">

<label for="size">Teléfono</label>
<input class="form-control" type="text" id="telefono" name="telefono" placeholder="Telefono">
</div>
</div>


</div>

<div class="row">
  <div class="col-sm-12">
<div class="form-group">

<label for="size">Correo</label>
<input class="form-control" type="text" id="correo" name="correo" placeholder="Correo Electrónico">
</div>
</div>
</div>

<div class="row justify-content-center">

<div class="col-sm-6 justify-content-center">
<h6 class=" text-normal text-uppercase"><b>¿Requiere constancia?</b></h6>

<div class="form-group">

<div class="custom-control custom-radio custom-control-inline">
<input class="custom-control-input" type="radio" id="ex-radio-4" name="radio2" value="Si">
<label class="custom-control-label" for="ex-radio-4">SI</label>
</div>

<div class="custom-control custom-radio custom-control-inline">
<input class="custom-control-input" type="radio" id="ex-radio-5" name="radio2" checked="" value="No">
<label class="custom-control-label" for="ex-radio-5">NO</label>
</div>
              
</div>
</div>

<div class="col-sm-6 justify-content-center">
<span class="h2 d-block mt-3 invisible" id="precio">$<?php echo $row->Precio; ?></span>
</div>

</div>



<div class="row mt-0 d-none" id="titulo" >
<div class="col-sm-12 form-grou mb-2">
<label for="validationCustom03">Titulo</label>

<select class="form-control" id="validationCustom03" required="" id="titulo_name" name="titulo_name">
<option value="Ninguno" selected>Ninguno</option>
<option value="Lic.">Lic.</option>
<option value="Ing.">Ing.</option>
<option value="Mtro.">Mtro.</option>
<option value="Dr.">Dr.</option>
</select>

</div>
</div>


<!-- Option Row -->


<hr class="mb-3">

<div class="d-flex flex-wrap justify-content-between">
<div class="entry-share mt-2 mb-2 invisible"><span class="text-muted">Compartir:</span>
<div class="share-links"><a class="social-button shape-circle sb-facebook" href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a><a class="social-button shape-circle sb-instagram" href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="socicon-instagram"></i></a></div>
</div>

<div class="sp-buttons mt-2 mb-2">


<button class="btn btn-primary disabled" type="button" data-toggle="modal" data-target="#modal" id="main" >
<i class="icon-bag mr-2 mb-1"></i>Solicitar Constancia</button>
</div>
</div>
</form>
</div>
</div>
</div>