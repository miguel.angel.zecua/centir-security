<div class="accordion mb-4" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Cat_2" data-toggle="collapse" style="font-size: 18px">Jurídicos</a></h6>
</div>

<div class="collapse" id="Cat_2"  role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card"> 
<a class="product-thumb" href="<?=base_url('Conferencias/Abogado_Perito')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Abogado-Perito-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Abogado_Perito'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-2 d-none">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Importancia_ortografia')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Importancia-ortografia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Importancia_ortografia'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2 d-none">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Soluciones_alternas')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Soluciones-alternas-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Soluciones_alternas'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->
	
</div>
<!-- Fila 1-->

<!-- Body-->
</div>
</div>
</div>
</div>