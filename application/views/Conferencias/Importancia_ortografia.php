
<!DOCTYPE html>
<html lang="es">
<head>
<title>CENTI-R</title>
<?=$this->load->view('include/head','',TRUE);?>
</head>

<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;">
</iframe>
</noscript>
<!-- Template Customizer-->
<div class="customizer-backdrop"></div>

<!-- Modal-->
<div class="modal fade" id="modal" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">

<div class="modal-header">
<h4 class="modal-title">Importancia de la ortografía, redacción y argumentación en el ámbito Jurídico</h4>
<button class="close" type="button" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
</div>

<div class="modal-body">
<!-- BODY-->
<div class="alert alert-danger rounded text-center text-dark" role="alert">
Revise que sus datos sean correctos  
</div>
<form method="post" action="<?php echo site_url('Conferencias/create')?>">
<div class="form-group row">

<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Nombre</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="nombre_2" id="nombre_2" required readonly>
</div>

<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Telefono</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="telefono_2" id="telefono_2" required readonly>
</div>

<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Correo</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="correo_2" id="correo_2" required readonly>
</div>

<label for="inputPassword" class="col-sm-2 col-form-label mt-3">Constancia</label>
<div class="col-sm-10 mt-3">
<input type="text" class="form-control" name="constancia" id="constancia" required readonly>
</div>

<input type="text" class="form-control d-none" name="titulo_name" id="titulo_name2">

<input type="text" class="form-control d-none" name="id_conferencia" value="8">


</div>
<!-- BODY-->

</div>
<div class="modal-footer">
<button class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Editar</button>
<button class="btn btn-primary btn-sm " type="submit" value="save">Confirmar</button>
</div>
</form>
</div>
</div>
</div>
</div>
<!-- Modal-->

<!-- Off-Canvas Category Menu-->
<?=$this->load->view('include/slider_left','',TRUE);?>
<!-- Off-Canvas Category Menu-->

<!-- Off-Canvas Mobile Menu-->
<?=$this->load->view('include/menu_mobile','',TRUE);?>
<!-- Off-Canvas Mobile Menu-->

<!-- Topbar-->
<?=$this->load->view('include/top_bar.php','',TRUE);?>
<!-- Topbar-->

<!-- Navbar-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">

<!-- Search-->
<form class="site-search" method="get">
<input type="text" name="site_search" placeholder="Type to search...">
<div class="search-tools">
<span class="clear-search">Limpiar</span>
<span class="close-search"><i class="icon-cross"></i>
</span>
</div>
</form>

<!-- Logo-->
<?=$this->load->view('include/logo','',TRUE);?>
<!-- Logo-->

<!-- Main Navigation-->
<?=$this->load->view('include/menu_navigation','',TRUE);?>
<!-- Main Navigation-->

<!-- Toolbar-->
<?=$this->load->view('include/tool_bar','',TRUE);?>
<!-- Toolbar-->

</header>

<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">

<!-- Page Content-->

<!-- Page Title-->
<div class="page-title">
<div class="container">

<div class="column">
<h1>Importancia de la ortografía, redacción y argumentación en el ámbito Jurídico</h1>
</div>

<div class="column">
<ul class="breadcrumbs">
<li><a href="<?=base_url('Home')?>">Inicio</a>
</li>

<li class="separator">&nbsp;</li>
<li><a href="<?=base_url('Conferencias')?>">Conferencias</a>
</li>

<li class="separator">&nbsp;</li>
<li>Importancia de la ortografía, redacción y argumentación en el ámbito Jurídico</li>
</ul>
</div>
</div>
</div>

<div class="container padding-bottom-3x mb-1">
<div class="row">

<!-- Poduct Gallery-->
<div class="col-md-6">
<div class="product-gallery">

<div class="gallery-wrapper">
<div class="gallery-item video-btn text-center">
<a href="#" data-toggle="tooltip" data-type="video" data-video="&lt;div class=&quot;wrapper&quot;&gt;&lt;div class=&quot;video-wrapper&quot;&gt;&lt;iframe class=&quot;pswp__video&quot; width=&quot;960&quot; height=&quot;640&quot; src=&quot;//www.youtube.com/embed/FKiFsHXHe4M&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;&lt;/div&gt;&lt;/div&gt;" title="Watch video"></a></div>
</div>

<div class="product-carousel owl-carousel gallery-wrapper">

<div class="gallery-item" data-hash="one">
<a href="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Importancia-ortografia-min.png')?>" data-size="1000x667">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Importancia-ortografia-min.png')?>" alt="Product"></a></div>
</div>

</div>
</div>

<!-- Product Info-->
<div class="col-md-6">
<div class="padding-top-2x mt-2 hidden-md-up"></div>
<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i>
</div><span class="text-muted align-middle">&nbsp;&nbsp;4.2 | 3 Consumidores</span>

<h2 class="padding-top-1x text-normal"><?php echo $row->Nombre; ?></h2>

<div class="alert alert-info alert-dismissible fade show text-center margin-bottom-1x" style="border-radius: 50px; padding: 13px;
"><h4><strong style="color: black;">Constancia $50 Pesos:</strong></h4></div>


<p><?php echo $row->Descripcion_1; ?></p>
<p><?php echo $row->Descripcion_2; ?></p>

<!-- Option Row -->
<?=$this->load->view('Conferencias/Formulario','',TRUE);?>
<!-- Option Row -->

<!-- Product Tabs-->
<div class="row padding-top-3x mb-3">
<div class="col-lg-10 offset-lg-1">
<ul class="nav nav-tabs" role="tablist">

<li class="nav-item">
<a class="nav-link active" href="#description" data-toggle="tab" role="tab">Descripcion</a>
</li>

<li class="nav-item invisible">
<a class="nav-link" href="#reviews" data-toggle="tab" role="tab">Reseñas (3)</a>
</li>
</ul>

<div class="tab-content">
<div class="tab-pane fade show active" id="description" role="tabpanel">

<p><?php echo $row->Descripcion_1; ?></p>
<p><?php echo $row->Descripcion_2; ?></p>


</div>

<div class="tab-pane fade" id="reviews" role="tabpanel">

<!-- Review 1-->
<div class="comment">
<div class="comment-author-ava">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Review author"></div>
<div class="comment-body">

<div class="comment-header d-flex flex-wrap justify-content-between">
<h4 class="comment-title">Titulo 1</h4>
<div class="mb-2">

<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i><i class="icon-star"></i>
</div>

</div>
</div>

<p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<div class="comment-footer"><span class="comment-meta">Mike</span></div>
</div>
</div>
<!-- Review 1-->

<!-- Review 2-->
<div class="comment">
<div class="comment-author-ava">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Review author"></div>
<div class="comment-body">

<div class="comment-header d-flex flex-wrap justify-content-between">
<h4 class="comment-title">Titulo 1</h4>
<div class="mb-2">

<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i><i class="icon-star"></i>
</div>

</div>
</div>

<p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<div class="comment-footer"><span class="comment-meta">Gil</span></div>
</div>
</div>
<!-- Review 2-->

<!-- Review 3-->
<div class="comment">
<div class="comment-author-ava">
<img src="<?=base_url('library/img/Categorias/A0/1.jpg')?>" alt="Review author"></div>
<div class="comment-body">

<div class="comment-header d-flex flex-wrap justify-content-between">
<h4 class="comment-title">Titulo 1</h4>
<div class="mb-2">

<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i><i class="icon-star"></i>
</div>

</div>
</div>

<p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<div class="comment-footer"><span class="comment-meta">Mike Y Gil</span></div>
</div>
</div>
<!-- Review 3-->


<!-- Review Form -->
<h5 class="mb-30 padding-top-1x">Deje su opinión</h5>
<form class="row" method="post">

<div class="col-sm-6">
<div class="form-group">
<label for="review_name">Nombre</label>
<input class="form-control form-control-rounded" type="text" id="review_name" required>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="review_email">Email</label>
<input class="form-control form-control-rounded" type="email" id="review_email" required>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="review_subject">Titulo</label>
<input class="form-control form-control-rounded" type="text" id="review_subject" required>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="review_rating">Puntuacion</label>
<select class="form-control form-control-rounded" id="review_rating">
<option>5 Estrellas</option>
<option>4 Estrellas</option>
<option>3 Estrellas</option>
<option>2 Estrellas</option>
<option>1 Estrella</option>
</select>
</div>
</div>

<div class="col-12">
<div class="form-group">
<label for="review_text">Comentarios</label>
<textarea class="form-control form-control-rounded" id="review_text" rows="8" required></textarea>
</div>
</div>

<div class="col-12 text-right">
<button class="btn btn-outline-primary" type="submit">Enviar Comentarios</button>
</div>

</form><!-- Review Form -->

</div>
</div>
</div>
</div>

</div>



<!-- Page Content-->



    <!-- Photoswipe container-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>


<!-- Featured Products Carousel-->
      
<section class="container padding-top-3x padding-bottom-2x"></section>
<!-- Site Footer-->
<?=$this->load->view('include/footer','',TRUE);?>
<!-- Site Footer-->
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Back To Top Button-->

<?=$this->load->view('include/js','',TRUE);?>
<script src="<?=base_url('library/js/Especials/Conferencias.js')?>"></script>
</body>
</html>