<div class="accordion mb-4" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Cat_0" data-toggle="collapse" style="font-size: 18px">"Lo mas visto"</a></h6>
</div>

<div class="collapse" id="Cat_0"  role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card"> 
<a class="product-thumb" href="<?=base_url('Conferencias/Apoyo_balistica')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Apoyo-balistica-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Apoyo_balistica'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-2 d-none">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Importancia_ortografia')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Importancia-ortografia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Importancia_ortografia'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">     
<a class="product-thumb" href="<?=base_url('Conferencias/Emociones_vida')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Emociones-vida-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Emociones_vida'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->
	
</div>
<!-- Fila 1-->

<!-- Fila 2-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Liderazgo_aplicado')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Psicologia/Liderazgo-aplicado-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Liderazgo_aplicado'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product -->
<div class="col-md-3 col-sm-4 mt-2">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Delincuentes_infancia')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Delincuentes-infancia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Delincuentes_infancia'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">
<a class="product-thumb" href="<?=base_url('Conferencias/Funcion_criminalista')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Funcion-criminalista-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>   
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Funcion_criminalista'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->

	
</div>
<!-- Fila 2-->

<!-- Fila 3-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card"> 
<a class="product-thumb" href="<?=base_url('Conferencias/Abogado_Perito')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Juridicos/Abogado-Perito-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Abogado_Perito'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2 d-none">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Cerebro_confinamiento')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Pedagogia/Cerebro-confinamiento-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Cerebro_confinamiento'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->




	
</div>
<!-- Fila 3-->

<!-- Fila 4-->
<div class="row justify-content-center mt-3">

	
</div>
<!-- Fila 4-->

<!-- Body-->
</div>
</div>
</div>
</div>