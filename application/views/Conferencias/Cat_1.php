<div class="accordion mb-4" role="tablist">
<div class="card">

<div class="card-header" role="tab">
<h6 class="text-center">
<a class="collapsed" href="#Cat_1" data-toggle="collapse" style="font-size: 18px">Forense</a></h6>
</div>

<div class="collapse" id="Cat_1"  role="tabpanel">
<div class="card-body">
<!-- Body-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2 d-none">

<div class="grid-item">
<div class="product-card">   
<a class="product-thumb" href="<?=base_url('Conferencias/Prevencion_suicidio')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Prevencion-suicidio-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Prevencion_suicidio'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2 d-none">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Conductas_destructivas')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Conductas-destructivas-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Conductas_destructivas'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">
<a class="product-thumb" href="<?=base_url('Conferencias/Funcion_criminalista')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Funcion-criminalista-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Funcion_criminalista'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Investigacion_forense')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Investigacion-forense-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Investigacion_forense'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->
	
</div>
<!-- Fila 1-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3 d-none">


<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Factor_criminologico')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Factor-criminologico-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Factor_criminologico'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->
	
</div>
<!-- Fila 1-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">    
<a class="product-thumb" href="<?=base_url('Conferencias/Investigacion_criminal')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Investigacion-criminal-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Investigacion_criminal'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->

<!-- Product 2-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Trabajo_interdisciplinario')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Trabajo-interdisciplinario-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Trabajo_interdisciplinario'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 2-->

<!-- Product 3-->
<div class="col-md-3 col-sm-4 mt-2">
	
<div class="grid-item">
<div class="product-card">

<a class="product-thumb" href="<?=base_url('Conferencias/Delincuentes_infancia')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Delincuentes-infancia-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>

<div class="product-buttons">
	
<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Delincuentes_infancia'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 3-->
	
</div>
<!-- Fila 1-->

<!-- Fila 1-->
<div class="row justify-content-center mt-3">

<!-- Product 1-->
<div class="col-md-3 col-sm-4 mt-2">

<div class="grid-item">
<div class="product-card"> 
<a class="product-thumb" href="<?=base_url('Conferencias/Apoyo_balistica')?>">
<img src="<?=base_url('library/img/Categorias/Conferencias/Forense/Apoyo-balistica-min.png')?>" alt="Product"></a>
<h4 class="product-price invisible">$00.00</h4>
          
<div class="product-buttons">

<button class="btn btn-outline-primary btn-sm" onclick="location.href='Conferencias/Apoyo_balistica'">Unirme</button>
</div>

</div>
</div>

</div>
<!-- Product 1-->
	
</div>
<!-- Fila 1-->

<!-- Body-->
</div>
</div>
</div>
</div>