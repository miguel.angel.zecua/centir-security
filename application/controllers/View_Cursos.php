<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View_Cursos extends CI_Controller 
{


public function Plantas_medicinales()
{
$this->load->view('View_Cursos/Plantas_medicinales');
}



public function Mercados_financieros()
{
$this->load->view('View_Cursos/Mercados_financieros');
}



public function Terapia_Imanes()
{
$this->load->view('View_Cursos/Terapia_Imanes');
}


public function Medicina_Cannabica()
{
$this->load->view('View_Cursos/Medicina_Cannabica');
}

public function Auriculoterapia()
{
$this->load->view('View_Cursos/Auriculoterapia');
}

public function Dejar_amar()
{
$this->load->view('View_Cursos/Dejar_amar');
}

public function Terapia_neural()
{
$this->load->view('View_Cursos/Terapia_neural');
}

public function Alineacion_chakras()
{
$this->load->view('View_Cursos/Alineacion_chakras');
}

public function Alimentos_funcionales()
{
$this->load->view('View_Cursos/Alimentos_funcionales');
}


//----------------------------------------------

public function Dinero_espiritu()
{
$this->load->view('View_Cursos/Dinero_espiritu');
}

//----------------------------------------------


//----------------------------------------------

public function Estimulacion_temprana()
{
$this->load->view('View_Cursos/Estimulacion_temprana');
}


public function Lectoescritura_sensorial()
{
$this->load->view('View_Cursos/Lectoescritura_sensorial');
}

//----------------------------------------------

public function Balistica_Forense()
{
$this->load->view('View_Cursos/Balistica_Forense');
}

public function Introduccion_Estudio()
{
$this->load->view('View_Cursos/Introduccion_Estudio');
}

public function Investigacion_criminal()
{
$this->load->view('View_Cursos/Investigacion_criminal');
}

public function Introduccion_dactiloscopia()
{
$this->load->view('View_Cursos/Introduccion_dactiloscopia');
}

//----------------------------------------------

public function Lectura_redaccion()
{
$this->load->view('View_Cursos/Lectura_redaccion');
}

//----------------------------------------------

//FIN DE FUNCION
}
