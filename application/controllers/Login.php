<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

public function __construct() 
{
parent:: __construct();
$this->load->model('Model');
}

public function index()
{
$this->session->sess_destroy();
$this->load->view('Login/Login');
}

public function Recovery()
{
$this->load->view('Login/Recovery');
}


public function End()
{
$this->session->sess_destroy();
redirect("Home");
}

public function New()
{
$this->Model->New();
echo "<script> swal({
   title: '¡ERROR!',
   text: 'Esto es un mensaje de error',
   type: 'error',
 });</script>";
redirect("Login");

}

public function Enter()
{
$email = $this->input->post('Email');
$password = $this->input->post('Password_Main');

$fila = $this->Model->Main($email);

if($fila !=null)
{
if($fila->Contraseña==$password){
$data = array(
'Id' => $fila->Id_Usuario,
'Nombre'=> $fila->Nombre_Usuario,
'Login' => true
);
$this->session->set_userdata($data);

redirect("Cursos");
}
else
{
redirect("Login");
}

}

else
{
redirect("Login");
}

}

///FIN DE CLASE
}
