<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultas extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Data');
$this->load->model('Model');
}

public function index()
{
$this->load->view('Consultas/Inicio');
}

public function List()
{
$this->load->view('Consultas/List');
}

//ESCLUSIVAS DE CONSULTAS
public function Inicio()
{
$this->load->view('Consultas/Step/St-1');
}

public function St2()
{
$this->load->view('Consultas/Step/St-2');
}

public function St3()
{
$this->load->view('Consultas/Step/St-3');
}

public function St4()
{
$this->load->view('Consultas/Step/St-4');
}
//ESCLUSIVAS DE CONSULTAS

public function Adicciones()
{
$Tabla='consultas';
$Nombre='Adicciones';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Adicciones',$data);
}

public function Especial()
{
$Tabla='consultas';
$Nombre='Educación especial';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Especial',$data);
}

public function Familiar()
{
$Tabla='consultas';
$Nombre='Familiar';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Familiar',$data);
}

public function Individual()
{
$Tabla='consultas';
$Nombre='Individual';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Individual',$data);
}

public function Infantil()
{
$Tabla='consultas';
$Nombre='Infantil';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Infantil',$data);
}

public function Lenguaje()
{
$Tabla='consultas';
$Nombre='De Lenguaje';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Lenguaje',$data);
}

public function Pareja()
{
$Tabla='consultas';
$Nombre='De pareja';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Pareja',$data);
}

public function Tanatologia()
{
$Tabla='consultas';
$Nombre='Tanatologia';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Consultas/Tanatologia',$data);
}

public function create() 
{
$this->Model->createConsulta();
}

//FIN DE CLASE
}
