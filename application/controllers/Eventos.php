<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Event');
}

public function index()
{
$data = $this->Event->getAllData();
echo json_encode($data);
}

}
