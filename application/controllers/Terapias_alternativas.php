<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terapias_alternativas extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Data');
$this->load->model('Model');
}

public function index()
{
$this->load->view('Terapias_alternativas/Inicio');
}
////////////////////////////////////////////////////////////
//ETAPA DE STEPS
public function Inicio()
{
$this->load->view('Terapias_alternativas/Step/St-1');
}

public function St2()
{
$this->load->view('Terapias_alternativas/Step/St-2');
}

public function St3()
{
$this->load->view('Terapias_alternativas/Step/St-3');
}

public function St4()
{
$this->load->view('Terapias_alternativas/Step/St-4');
}
//ETAPA DE STEPS
/////////////////////////////////////////////////////////////
public function List()
{
$this->load->view('Terapias_alternativas/List');
}

public function Acupuntura()
{
$Tabla='terapias_alternativas';
$Nombre='Acupuntura';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Acupuntura',$data);
}

public function Ventosas()
{
$Tabla='terapias_alternativas';
$Nombre='Ventosas';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Ventosas',$data);
}

public function Auriculoterapia()
{
$Tabla='terapias_alternativas';
$Nombre='Auriculoterapia';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Auriculoterapia',$data);
}

public function Conoterapia()
{
$Tabla='terapias_alternativas';
$Nombre='Conoterapia';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Conoterapia',$data);
}

public function Terapia_imanes()
{
$Tabla='terapias_alternativas';
$Nombre='Terapia de Imanes';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Terapia_imanes',$data);
}

public function Flores_mexicanas()
{
$Tabla='Terapias_alternativas';
$Nombre='Flores mexicanas';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Flores_mexicanas',$data);
}

public function Terapia_neural()
{
$Tabla='terapias_alternativas';
$Nombre='Terapia neural';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Terapia_neural',$data);
}

public function Alineacion_chakras()
{
$Tabla='terapias_alternativas';
$Nombre='Alineación de chakras';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Alineacion_chakras',$data);
}

public function Masaje_terapeutico()
{
$Tabla='terapias_alternativas';
$Nombre='Masaje terapéutico';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Masaje_terapeutico',$data);
}

public function Masaje_reductivo()
{
$Tabla='terapias_alternativas';
$Nombre='Masaje reductivo';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Terapias_alternativas/Masaje_reductivo',$data);
}

public function create() 
{
$this->Model->createTerapias_alternativa();
redirect("Terapias_alternativas");
}
//FIN DE CLASE
}
