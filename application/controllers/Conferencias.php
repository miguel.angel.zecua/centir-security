<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conferencias extends CI_Controller 
{
public function __construct() 
{
parent:: __construct();
$this->load->model('Data');
$this->load->model('Model');
}

public function index()
{
$this->load->view('Conferencias/List');
}

public function List()
{
$this->load->view('Conferencias/List');
}

public function Forense()
{
$Tabla='conferencias';
$Nombre='Forense';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Forense',$data);
}

public function Agua_Magnetizada()
{
$this->load->view('Conferencias/Agua_Magnetizada');
}



public function Juridica()
{
$Tabla='conferencias';
$Nombre='Juridica';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Juridica',$data);
}

public function Psicologica()
{
$Tabla='conferencias';
$Nombre='Psicologica';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Psicologica',$data);
}

public function Pedagogicas()
{
$Tabla='conferencias';
$Nombre='Pedagogicas';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Pedagogicas',$data);
}

public function Terapias_alternativas()
{
$Tabla='conferencias';
$Nombre='Terapias alternativas';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Terapias_alternativas',$data);
}

public function create() 
{
$this->Model->createConferencia();
redirect("Conferencias");
}
//--------------------------
public function Liderazgo_aplicado()
{
$Tabla='conferencias';
$Nombre='Liderazgo aplicado al desarrollo personal';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Liderazgo_aplicado',$data);
}

public function Apoyo_balistica()
{
$Tabla='conferencias';
$Nombre='Apoyo a la balística forense en el apoyo criminal';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Apoyo_balistica',$data);
}

public function Circulo_vicioso()
{
$Tabla='conferencias';
$Nombre='Circulo vicioso del amor inmaduro';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Circulo_vicioso',$data);
}

public function Cerebro_confinamiento()
{
$Tabla='conferencias';
$Nombre='¿Cómo aprende el cerebro en tiempos de confinamiento?';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Cerebro_confinamiento',$data);
}

public function Delincuentes_infancia()
{
$Tabla='conferencias';
$Nombre='¿Cómo formamos delincuentes desde la infancia?';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Delincuentes_infancia',$data);
}

public function Soluciones_alternas()
{
$Tabla='conferencias';
$Nombre='Soluciones alternas del procedimiento penal';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Soluciones_alternas',$data);
}

public function Trabajo_interdisciplinario()
{
$Tabla='conferencias';
$Nombre='Soñar no cuesta nada: El trabajo interdisciplinario con adolescentes en internamiento';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Trabajo_interdisciplinario',$data);
}

public function Importancia_ortografia()
{
$Tabla='conferencias';
$Nombre='Importancia de la ortografía, redacción y argumentación en el ámbito Jurídico';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Importancia_ortografia',$data);
}

public function Defensa_dolor()
{
$Tabla='conferencias';
$Nombre='Defensa ante el dolor';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Defensa_dolor',$data);
}

public function Investigacion_criminal()
{
$Tabla='conferencias';
$Nombre='Como inicia una investigación criminal en México';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Investigacion_criminal',$data);
}

public function Factor_criminologico()
{
$Tabla='conferencias';
$Nombre='La familia como factor criminológico';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Factor_criminologico',$data);
}

public function Investigacion_forense()
{
$Tabla='conferencias';
$Nombre='Investigación forense de artefacto explosivos y peligrosos';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Investigacion_forense',$data);
}

public function Duelo_Covid_19()
{
$Tabla='conferencias';
$Nombre='Duelo ante el Covid-19';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Duelo_Covid_19',$data);
}

public function Funcion_criminalista()
{
$Tabla='conferencias';
$Nombre='La función del criminalista en la necropsia';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Funcion_criminalista',$data);
}

public function Abogado_Perito()
{
$Tabla='conferencias';
$Nombre='Abogado vs Perito en el juicio oral';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Abogado_Perito',$data);
}

public function Conductas_destructivas()
{
$Tabla='conferencias';
$Nombre='Conductas destructivas en la niñez y adolescencia';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Conductas_destructivas',$data);
}

public function Retos_educacion()
{
$Tabla='conferencias';
$Nombre='Retos de la educación ante la nueva normalidad';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Retos_educacion',$data);
}

public function Conociendo_construyendo()
{
$Tabla='conferencias';
$Nombre='Conociendo y construyendo mi masculinidad';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Conociendo_construyendo',$data);
}

public function Perfil_criminal()
{
$Tabla='conferencias';
$Nombre='Perfil criminal en juicios orales';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Perfil_criminal',$data);
}

public function Emociones_vida()
{
$Tabla='conferencias';
$Nombre='¿Qué papel juegan las emociones dentro de mi vida?';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Emociones_vida',$data);
}

public function Prevencion_suicidio()
{
$Tabla='conferencias';
$Nombre='Prevención del suicidio';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Prevencion_suicidio',$data);
}

public function Defensa_dolor_2()
{
$Tabla='conferencias';
$Nombre='Defensa ante el dolor';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Defensa_dolor_2',$data);
}

public function Perdida_perdon()
{
$Tabla='conferencias';
$Nombre='La perdida y el perdón';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Perdida_perdon',$data);
}

public function Alimentos_funcionales_enfermedades()
{
$Tabla='conferencias';
$Nombre='Alimentos funcionales: Promueven el riesgo a presentar enfermedades ';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Alimentos_funcionales_enfermedades',$data);
}

public function Terapia_dolor()
{
$Tabla='conferencias';
$Nombre='Terapia neural: La terapia contra el dolor';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Terapia_dolor',$data);
}

public function Errores_lectoescritura()
{
$Tabla='conferencias';
$Nombre='Errores más comunes en la lectoescritura: Estrategias de prevención y corrección';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Errores_lectoescritura',$data);
}

public function Relacion_historia_familiar()
{
$Tabla='conferencias';
$Nombre='¿Qué relación hay entre mi historia familiar y mis problemas con el dinero?';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Relacion_historia_familiar',$data);
}

public function Influencia_emociones()
{
$Tabla='conferencias';
$Nombre='La influencia de las emociones en la salud';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Conferencias/Influencia_emociones',$data);
}
//--------------------------

//FIN DE CLASE
}
