<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Tablas');
}

public function index()
{
$data['result'] = $this->Tablas->getAllData();
$this->load->view('Admin/Inicio', $data);
}

public function Conferencias()
{
$data['result'] = $this->Tablas->getAllData2();
$this->load->view('Admin/Conferencias', $data);
}

public function Cursos()
{
$data['result'] = $this->Tablas->getAllData3();
$this->load->view('Admin/Cursos', $data);
}

public function Diplomados()
{
$data['result'] = $this->Tablas->getAllData4();
$this->load->view('Admin/Diplomados', $data);
}

public function Terapias()
{
$data['result'] = $this->Tablas->getAllData5();
$this->load->view('Admin/Terapias', $data);
}
//-------------------------------------------
public function Calendar_Inicio()
{
$data['result'] = $this->Tablas->getAllData();
$this->load->view('Admin/Calendar/Calendar_Inicio', $data);
}

public function Calendar_Terapia()
{
$data['result'] = $this->Tablas->getAllData6();
$this->load->view('Admin/Calendar/Calendar_Terapia', $data);
}
//-------------------------------------------
function Vista_Consulta($id)
{
$data['row'] = $this->Tablas->getDataConsulta($id);
$this->load->view('Admin/Vista_Consulta', $data);
}

function Vista_Conferencia($id)
{
$data['row'] = $this->Tablas->getDataConferencia($id);
$this->load->view('Admin/Vista_Conferencia', $data);
}

function Vista_Diplomado($id)
{
$data['row'] = $this->Tablas->getDataDiplomado($id);
$this->load->view('Admin/Vista_Diplomado', $data);
}

function Vista_Terapia($id)
{
$data['row'] = $this->Tablas->getDataTerapia($id);
$this->load->view('Admin/Vista_Terapia', $data);
}

function Vista_Curso($id)
{
$data['row'] = $this->Tablas->getDataCurso($id);
$this->load->view('Admin/Vista_Curso', $data);
}

}