
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Usuario');
}

public function Perfil()
{
$verify=$_SESSION['Login']; 

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');	
$data['row'] = $this->Usuario->Datos($Id);
$this->load->view('Usuario/Perfil',$data);
}

else
{
$this->load->view('Login/Login');	
}



}

public function Cursos()
{
if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');

$data = array('row' => $this->Usuario->Datos($Id));
//$data['row'] = $this->Usuario->Datos($Id);
$data = array('tests' => $this->Usuario->Cursos($Id));

$this->load->view('Usuario/Cursos',$data);
}

else
{
$this->load->view('Login/Login');		
}

}

public function Diplomados()
{
if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');

$data = array('row' => $this->Usuario->Datos($Id));
//$data['row'] = $this->Usuario->Datos($Id);
$data = array('tests' => $this->Usuario->Diplomados($Id));

$this->load->view('Usuario/Diplomados',$data);
}

else
{
$this->load->view('Login/Login');		
}

}




///FIN DE CLASE
}
