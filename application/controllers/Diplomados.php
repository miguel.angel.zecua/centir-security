<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diplomados extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Data');
$this->load->model('Model');
}

public function index()
{
$this->load->view('Diplomados/Inicio');
}

public function List()
{
$this->load->view('Diplomados/List');
}


public function Acupuntura()
{
$Tabla='diplomados';
$Nombre='Acupuntura y Terapias Alternativas';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Diplomados/Acupuntura',$data);
}

public function create() 
{
$this->Model->createDiplomado();
redirect("Diplomados");
}

public function Biomagnetismo()
{
$Tabla='diplomados';
$Nombre='Biomagnetismo';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Diplomados/Biomagnetismo',$data);
}


public function S_penal_acusatorio()
{
$Tabla='diplomados';
$Nombre='Sistema penal acusatorio y sus etapas procesales';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Diplomados/S_penal_acusatorio',$data);
}

public function Masajes_Terapeuticos()
{
$Tabla='diplomados';
$Nombre='Masajes Terapeúticos';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Diplomados/Masajes_Terapeuticos',$data);
}


public function Acupuntura_Terapias_Alternativas()
{
$Tabla='diplomados';
$Nombre='Acupuntura y Terapias Alternativas';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Diplomados/Acupuntura_Terapias_Alternativas',$data);
}


//FIN DE CLASE
}
