<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Data');
$this->load->model('Model');
}

public function index()
{
$this->load->view('Cursos/Inicio');
}

public function List()
{
$this->load->view('Cursos/List');
}
//----------------------------------------------
public function Plantas_medicinales()
{
$Tabla='cursos';
$Nombre='Plantas Medicinales';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Cursos/Otros/Plantas_medicinales',$data);
}
//----------------------------------------------
public function Mercados_financieros()
{
$Tabla='cursos';
$Nombre='¿Cómo invertir en los mercados financieros?';
$data['row'] = $this->Data->getData($Tabla,$Nombre);
$this->load->view('Cursos/Otros/Mercados_financieros',$data);
}
//----------------------------------------------
public function Terapia_Imanes()
{
$Tabla='cursos';
$Nombre='Terapia de Imanes para las Emociones';
$data['row'] = $this->Data->getData($Tabla,$Nombre);


if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Terapias_Integrativas/Terapia_Imanes',$data);
}
else
{
$this->load->view('Cursos/Terapias_Integrativas/Terapia_Imanes',$data);
}

}

public function Medicina_Cannabica()
{
$Tabla='cursos';
$Nombre='Medicina Cannábica';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Terapias_Integrativas/Medicina_Cannabica',$data);
}
else
{
$this->load->view('Cursos/Terapias_Integrativas/Medicina_Cannabica',$data);
}


}

public function Auriculoterapia()
{
$Tabla='cursos';
$Nombre='Auriculoterapia para control de peso';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Terapias_Integrativas/Auriculoterapia',$data);
}
else
{
$this->load->view('Cursos/Terapias_Integrativas/Auriculoterapia',$data);
}

}


public function Dejar_amar()
{
$Tabla='cursos';
$Nombre='Dejar ir es amar';
$data['row'] = $this->Data->getData($Tabla,$Nombre);


if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Terapias_Integrativas/Dejar_amar',$data);
}
else
{
$this->load->view('Cursos/Terapias_Integrativas/Dejar_amar',$data);
}

}

public function Terapia_neural()
{
$Tabla='cursos';
$Nombre='Terapia neural';
$data['row'] = $this->Data->getData($Tabla,$Nombre);


if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Terapias_Integrativas/Terapia_neural',$data);
}
else
{
$this->load->view('Cursos/Terapias_Integrativas/Terapia_neural',$data);
}

}

public function Alineacion_chakras()
{
$Tabla='cursos';
$Nombre='Alineación de chakras con imanes';
$data['row'] = $this->Data->getData($Tabla,$Nombre);


if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Terapias_Integrativas/Alineacion_chakras',$data);
}
else
{
$this->load->view('Cursos/Terapias_Integrativas/Alineacion_chakras',$data);
}

}

public function Alimentos_funcionales()
{
$Tabla='cursos';
$Nombre='Alimentos funcionales';
$data['row'] = $this->Data->getData($Tabla,$Nombre);


if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Terapias_Integrativas/Alimentos_funcionaless',$data);
}
else
{
$this->load->view('Cursos/Terapias_Integrativas/Alimentos_funcionales',$data);
}

}


//----------------------------------------------

public function Dinero_espiritu()
{
$Tabla='cursos';
$Nombre='Dinero y espíritu';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Psicologia/Dinero_espiritu',$data);
}
else
{
$this->load->view('Cursos/Psicologia/Dinero_espiritu',$data);
}

}

//----------------------------------------------


//----------------------------------------------

public function Estimulacion_temprana()
{
$Tabla='cursos';
$Nombre='Estimulación temprana';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Pedagogia/Estimulacion_temprana',$data);
}
else
{
$this->load->view('Cursos/Pedagogia/Estimulacion_temprana',$data);
}

}


public function Lectoescritura_sensorial()
{
$Tabla='cursos';
$Nombre='Lectoescritura sensorial';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Pedagogia/Lectoescritura_sensorial',$data);
}
else
{
$this->load->view('Cursos/Pedagogia/Lectoescritura_sensorial',$data);
}

}

//----------------------------------------------

public function Balistica_Forense()
{
$Tabla='cursos';
$Nombre='Balística Forense e identificación de Armas de Fuego';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Forenses/Balistica_Forense',$data);
}
else
{
$this->load->view('Cursos/Forenses/Balistica_Forense',$data);
}

}

public function Introduccion_Estudio()
{
$Tabla='cursos';
$Nombre='Introducción Al Estudio De La Grafoscopía Y Documentoscopía';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Forenses/Introduccion_Estudio',$data);
}
else
{
$this->load->view('Cursos/Forenses/Introduccion_Estudio',$data);
}

}

public function Investigacion_criminal()
{
$Tabla='cursos';
$Nombre='Investigación criminal';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Forenses/Investigacion_criminal',$data);
}
else
{
$this->load->view('Cursos/Forenses/Investigacion_criminal',$data);
}

}

public function Introduccion_dactiloscopia()
{
$Tabla='cursos';
$Nombre='Introducción a la dactiloscopía';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Forenses/Introduccion_dactiloscopia',$data);
}
else
{
$this->load->view('Cursos/Forenses/Introduccion_dactiloscopia',$data);
}

}

//----------------------------------------------

public function Lectura_redaccion()
{
$Tabla='cursos';
$Nombre='Lectura, redacción y argumentación jurídica';
$data['row'] = $this->Data->getData($Tabla,$Nombre);

if($this->session->userdata('Login')==true)
{
$Id = $this->session->userdata('Id');
$data['usuario'] = $this->Data->getUser($Id);
$this->load->view('Cursos/Juridicos/Lectura_redaccion',$data);
}
else
{
$this->load->view('Cursos/Juridicos/Lectura_redaccion',$data);
}

}

//----------------------------------------------

public function create() 
{
$this->Model->createCurso();
redirect("Cursos");
}

//FIN DE CLASE
}
