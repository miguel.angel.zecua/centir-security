<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model 
{

public function __construct() 
{
$this->load->database();
$this->load->library('pdf');
}

function New() 
{
$data = array (
'Nombre_Usuario' => $this->input->post('Nombre'),
'Apellidos' => $this->input->post('Apellidos'),
'Correo' => $this->input->post('Correo'),
'Telefono' => $this->input->post('Telefono'),
'Contraseña' => $this->input->post('Password')
);
$this->db->insert('usuario', $data);
}

public function createConsulta()
{
$traveltime = $this->input->post('horario');
$contraveltime = date('H:i', strtotime($traveltime));

$data = array (
'Modalidad' =>  $this->input->post('modalidad'),    
'Sexo_Psicologo' => $this->input->post('sexo'), 
'Fecha' =>  $this->input->post('fecha'),        
'Horario' => $contraveltime,     
'Nombre_Usuario' => $this->input->post('nombre'), 
'Edad_Usuario' => $this->input->post('edad'),  
'Correo' => $this->input->post('correo'),         
'Telefono' => $this->input->post('telefono'),       
'Id_Consultas' => $this->input->post('consulta') 

);


$this->db->insert('consulta',$data);



$Modalidad = $this->input->post('modalidad');    
$Sexo_Psicologo = $this->input->post('sexo'); 
$Fecha = $this->input->post('fecha');        
$Horario = $contraveltime;     
$Nombre_Usuario =$this->input->post('nombre'); 
$Edad_Usuario = $this->input->post('edad');           
$Telefono = $this->input->post('telefono');       
$Id_Consultas = $this->input->post('consulta_name'); 


$token = "5555310294:AAEAp4IPM5dece_mw2cPPC9iufs_vY5htR0";
$id = "5577046431";
$urlMsg = "https://api.telegram.org/bot{$token}/sendMessage";
$msg ="NUEVA CONSULTA PSICOLOGICA!"."\n"."Consulta: ".$Id_Consultas."\n"."Nombre: ". $Nombre_Usuario."\n"."Edad: ".$Edad_Usuario."\n"."Telefono: ".$Telefono."\n"."Sexo del Psicologo: ".$Sexo_Psicologo."\n"."Modalidad: ".$Modalidad."\n"."Fecha de Consulta: ".$Fecha."\n"."Horario: ".$Horario;
 
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $urlMsg);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "chat_id={$id}&parse_mode=HTML&text=$msg");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
$server_output = curl_exec($ch);
curl_close($ch);



//$html = $this->load->view('Pdf/Consultas',$data, true);
//$this->pdf->createPDF($html, 'Consulta');
//$this->load->view('Pdf/Consultas',$data);
//redirect('Home');
}

public function createConferencia()
{
$data = array (
'Nombre_Usuario' =>  $this->input->post('nombre_2'),
'Correo_Usuario' =>  $this->input->post('correo_2'),
'Telefono' =>  $this->input->post('telefono_2'),
'Constancia' =>  $this->input->post('constancia'),
'Grado_Estudios' =>  $this->input->post('titulo_name'),
'Id_Conferencias' =>  $this->input->post('id_conferencia')
);
$this->db->insert('conferencia',$data);
$html = $this->load->view('Pdf/Conferencias',$data, true);
$this->pdf->createPDF($html, 'Conferencia');
}

public function createCurso()
{
$data = array (

'Nombre_Usuario' =>  $this->input->post('nombre_2'),
'Telefono_Usuario' =>  $this->input->post('telefono_2'),
'Correo_Usuario' =>  $this->input->post('correo_2'),
'Id_Cursos' =>  $this->input->post('id_curso')
//'Id_Usuario' =>  $this->input->post('id_usuario')
);
$this->db->insert('curso',$data);
$html = $this->load->view('Pdf/Cursos',$data, true);
$this->pdf->createPDF($html, 'Cursos');
}

public function createDiplomado()
{
$data = array (
'Nombre_Usuario' =>  $this->input->post('nombre_2'),
'Telefono_Usuario' =>  $this->input->post('telefono_2'),
'Correo_Usuario' =>  $this->input->post('correo_2'),
'Certificado' =>  $this->input->post('constancia'),
'Id_Diplomados' =>  $this->input->post('id_diplomado')
);
$this->db->insert('diplomado',$data);
$html = $this->load->view('Pdf/Diplomados',$data, true);
$this->pdf->createPDF($html, 'Diplomados');
}

public function createTerapias_alternativa()
{
$data = array (
'Nombre_Usuario' =>  $this->input->post('nombre_2'),
'Edad' =>  $this->input->post('edad_2'),
'Correo_Usuario' =>  $this->input->post('correo_2'),
'Telefono_Usuario' =>  $this->input->post('telefono_2'),
'Fecha' =>  $this->input->post('fecha_2'),
'Horario' =>  $this->input->post('horario_2'),
'Motivos' =>  $this->input->post('motivos_2'),
'Id_Terapias_Alternativas' =>  $this->input->post('terapia_alternativa')
);
$this->db->insert('terapia_alternativa',$data);

$Nombre_Usuario =  $this->input->post('nombre_2');
$Edad =  $this->input->post('edad_2');
$Correo_Usuario =  $this->input->post('correo_2');
$Telefono_Usuario =  $this->input->post('telefono_2');
$Fecha =  $this->input->post('fecha_2');
$Horario =  $this->input->post('horario_2');
$Motivos =  $this->input->post('motivos_2');
$Id_Terapias_Alternativas =  $this->input->post('consulta_name');




$token = "5555310294:AAEAp4IPM5dece_mw2cPPC9iufs_vY5htR0";
$id = "5577046431";
$urlMsg = "https://api.telegram.org/bot{$token}/sendMessage";
$msg ="NUEVA TERAPIA ALTERNATIVA"."\n"."Terapia: ".$Id_Terapias_Alternativas."\n"."Nombre: ". $Nombre_Usuario."\n"."Edad: ".$Edad."\n"."Telefono: ".$Telefono_Usuario."\n"."Fecha de Consulta: ".$Fecha."\n"."Horario: ".$Horario."\n\n"."Motivos: ".$Motivos;
 
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $urlMsg);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "chat_id={$id}&parse_mode=HTML&text=$msg");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
$server_output = curl_exec($ch);
curl_close($ch);
}

public function Main($email )
{
$result = $this->db->query("SELECT * FROM usuario WHERE correo ='" .$email ."' LIMIT 1");

if($result->num_rows() > 0){
	return $result->row();
}else{
return null;
}

}

//FIN DE CLASE
}