<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tablas extends CI_Model {

public function __construct() 
{
$this->load->database();
}

function getAllData() 
{
$query = $this->db->query('SELECT consulta.Id_Consulta, consulta.Modalidad, consulta.Sexo_Psicologo, consulta.fecha, consulta.horario, consulta.nombre_usuario, consulta.edad_usuario, consulta.correo, consulta.telefono, consultas.nombre as Consulta FROM consulta inner join consultas where consulta.Id_consultas=consultas.Id_consultas ORDER BY fECHA DESC');
return $query->result();
}

function getAllData2() 
{
$query = $this->db->query('SELECT conferencia.Id_Conferencia,conferencia.Nombre_Usuario,conferencia.Correo_Usuario,conferencia.Telefono,conferencia.Constancia,conferencia.Grado_Estudios,conferencias.nombre as Nombre_Conferencia FROM conferencia inner join conferencias where conferencia.Id_Conferencias=conferencias.Id_Conferencias');
return $query->result();
}

function getAllData3() 
{
$query = $this->db->query('SELECT curso.Id_Curso, curso.Nombre_Usuario,curso.Telefono_Usuario,curso.Correo_Usuario ,cursos.Nombre as Nombre_Curso from curso inner join cursos where curso.Id_cursos=cursos.Id_cursos');
return $query->result();
}

function getAllData4() 
{
$query = $this->db->query('SELECT diplomado.Id_diplomado,diplomado.nombre_usuario,diplomado.telefono_usuario,diplomado.correo_usuario,diplomado.certificado,diplomados.nombre as Nombre_Diplomado from diplomado inner join diplomados where diplomado.Id_Diplomados=diplomados.Id_Diplomados');
return $query->result();
}

function getAllData5() 
{
$query = $this->db->query('SELECT terapia_alternativa.Id_Terapia_Alternativa, terapia_alternativa.nombre_usuario,terapia_alternativa.edad,terapia_alternativa.correo_usuario,terapia_alternativa.telefono_usuario,terapia_alternativa.fecha,terapia_alternativa.horario,terapias_alternativas.nombre as Terapia from terapia_alternativa inner join terapias_alternativas where terapia_alternativa.Id_Terapias_Alternativas=terapias_alternativas.Id_Terapias_Alternativas');
return $query->result();
}

function getAllData6() 
{
$query = $this->db->query('SELECT terapia_alternativa.Id_Terapia_Alternativa, terapia_alternativa.nombre_usuario,terapia_alternativa.edad,terapia_alternativa.correo_usuario,terapia_alternativa.telefono_usuario,terapia_alternativa.fecha,terapia_alternativa.horario,terapia_alternativa.Motivos,terapias_alternativas.nombre as Terapia from terapia_alternativa inner join terapias_alternativas where terapia_alternativa.Id_Terapias_Alternativas=terapias_alternativas.Id_Terapias_Alternativas');
return $query->result();
}

//------------------------------------
function getDataConsulta($id) 
{
$query = $this->db->query('SELECT consulta.Id_Consulta, consulta.Modalidad, consulta.Sexo_Psicologo, consulta.fecha, consulta.horario, consulta.nombre_usuario, consulta.edad_usuario, consulta.correo, consulta.telefono, consultas.nombre as Consulta FROM consulta inner join consultas where consulta.Id_consultas=consultas.Id_consultas and consulta.Id_Consulta='.$id);
return $query->result();
}

function getDataConferencia($id) 
{
$query = $this->db->query('SELECT conferencia.Id_Conferencia,conferencia.Nombre_Usuario,conferencia.Correo_Usuario,conferencia.Telefono,conferencia.Constancia,conferencia.Grado_Estudios,conferencias.nombre as Nombre_Conferencia FROM conferencia inner join conferencias where conferencia.Id_Conferencias=conferencias.Id_Conferencias and conferencia.Id_Conferencia='.$id);
return $query->result();
}

function getDataDiplomado($id) 
{
$query = $this->db->query('SELECT diplomado.Id_diplomado,diplomado.nombre_usuario,diplomado.telefono_usuario,diplomado.correo_usuario,diplomado.certificado,diplomados.nombre as Nombre_Diplomado from diplomado inner join diplomados where diplomado.Id_Diplomados=diplomados.Id_Diplomados and diplomado.Id_diplomado='.$id);
return $query->result();
}

function getDataTerapia($id) 
{
$query = $this->db->query('SELECT terapia_alternativa.Id_Terapia_Alternativa, terapia_alternativa.nombre_usuario,terapia_alternativa.edad,terapia_alternativa.correo_usuario,terapia_alternativa.telefono_usuario,terapia_alternativa.fecha,terapia_alternativa.horario,terapia_alternativa.Motivos,terapias_alternativas.nombre as Terapia from terapia_alternativa inner join terapias_alternativas where terapia_alternativa.Id_Terapias_Alternativas=terapias_alternativas.Id_Terapias_Alternativas and terapia_alternativa.Id_Terapia_Alternativa='.$id);
return $query->result();
}

function getDataCurso($id)
{
$query = $this->db->query('SELECT curso.Id_Curso, curso.Nombre_Usuario,curso.Telefono_Usuario,curso.Correo_Usuario ,cursos.Nombre as Nombre_Curso from curso inner join cursos where curso.Id_cursos=cursos.Id_cursos and curso.Id_Curso='.$id);
return $query->result();
}

}