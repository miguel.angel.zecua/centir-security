<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Model {

public function __construct() 
{
$this->load->database();
}

function getAllData() 
{
$query = $this->db->query('SELECT consulta.Id_Consulta, consulta.Modalidad, consulta.Sexo_Psicologo, consulta.fecha, consulta.horario, consulta.nombre_usuario, consulta.edad_usuario, consulta.correo, consulta.telefono, consultas.nombre as Consulta FROM consulta inner join consultas where consulta.Id_consultas=consultas.Id_consultas ORDER BY fECHA DESC');
return $query->result();
}

}