<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Model 
{

public function __construct() 
{
$this->load->database();
}

public function Datos($Id)
{
$query = $this->db->query('SELECT * FROM usuario WHERE `Id_Usuario` =' .$Id);
return $query->row();
}

public function Cursos($Id)
{
$query = $this->db->query('select nombre,imagen,Url from cursos inner join curso_usuario on cursos.Id_cursos=curso_usuario.Id_Curso
inner join usuario on usuario.Id_Usuario=curso_usuario.Id_usuario and curso_usuario.Id_Usuario='.$Id);
return $query->result();
}

//public function Diplomados($Id)
//{
//$query = $this->db->query('select nombre,imagen,Url from cursos inner join curso_usuario on cursos.Id_cursos=curso_usuario.Id_Curso
//inner join usuario on usuario.Id_Usuario=curso_usuario.Id_usuario and curso_usuario.Id_Usuario='.$Id);
//return $query->result();
//}

//Prueba 
public function Diplomados($Id)
{
$query = $this->db->query('select nombre,imagen,Url from diplomados inner join usuarios_diplomados on diplomados.Id_diplomados=usuarios_diplomados.Id_Diplomados
inner join usuario on usuario.Id_Usuario=usuarios_diplomados.Id_usuario and usuarios_diplomados.Id_Usuario='.$Id);
return $query->result();
}

//FIN DE CLASE
}