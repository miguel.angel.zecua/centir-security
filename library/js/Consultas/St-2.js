
//CONSTANTES
const expresiones = 
{
nombre: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
apellidos: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
telefono: /^\d{7,14}$/, // 7 a 14 numeros.
edad: /^\d{1,3}$/, // 7 a 14 numeros.
password: /^.{4,12}$/ // 4 a 12 digitos.
}
//CONSTANTES

const campos = 
{
nombre: false,
edad: false,
correo: false,
telefono: false
}

const main = document.querySelector('#main');
const enviar = document.querySelector('#siguiente');
const enviar_2 = document.querySelector('#datos_consulta');

const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');
var formulario_principal = document.getElementById("form");


enviar.addEventListener('click', (e) =>{
e.preventDefault();

sessionStorage.setItem('Nombre_main',$("#nombre").val());
sessionStorage.setItem('Edad_main',$("#edad").val());
sessionStorage.setItem('Correo_main',$("#correo").val());
sessionStorage.setItem('Telefono_main',$("#telefono").val());

window.location.href = 'St3';
});


enviar_2.addEventListener('click', (e) =>{
e.preventDefault();

sessionStorage.setItem('Nombre_main',$("#nombre").val());
sessionStorage.setItem('Edad_main',$("#edad").val());
sessionStorage.setItem('Correo_main',$("#correo").val());
sessionStorage.setItem('Telefono_main',$("#telefono").val());

window.location.href = 'St3';
});


const validarFormulario = (e)=>{
switch(e.target.name)
{
case  "nombre":
if(expresiones.nombre.test(e.target.value))
{
document.getElementById('nombre').classList.remove('is-invalid');
document.getElementById('nombre').classList.add('is-valid');
campos['nombre'] = true;
}
else
{
document.getElementById('nombre').classList.add('is-invalid');
campos['nombre'] = false;
}
break;

case  "edad":
if(expresiones.edad.test(e.target.value))
{
document.getElementById('edad').classList.remove('is-invalid');
document.getElementById('edad').classList.add('is-valid');
campos['edad'] = true;
}
else
{
document.getElementById('edad').classList.add('is-invalid');
campos['edad'] = false;
}

break;


case  "correo":
if(expresiones.correo.test(e.target.value))
{
document.getElementById('correo').classList.remove('is-invalid');
document.getElementById('correo').classList.add('is-valid');
campos['correo'] = true;
}

else
{
document.getElementById('correo').classList.add('is-invalid');
campos['correo'] = false;
}

break;

case  "telefono":
if(expresiones.telefono.test(e.target.value))
{
document.getElementById('telefono').classList.remove('is-invalid');
document.getElementById('telefono').classList.add('is-valid');
campos['telefono'] = true;
}
else
{
document.getElementById('telefono').classList.add('is-invalid');
campos['telefono'] = false;
}

break;


}

}



inputs.forEach((input)=>{
input.addEventListener('keyup', validarFormulario);
input.addEventListener('blur', validarFormulario);
});



function mayus(e) 
{
e.value = e.value.toUpperCase();
}

formulario.addEventListener('keyup' ,(event) =>{

if(campos.nombre && campos.edad && campos.correo && campos.telefono )
{
$("#siguiente").removeClass("disable_link");
$("#datos_consulta").removeClass("disable_link");
}
else {
$("#siguiente").addClass("disable_link");
$("#datos_consulta").addClass("disable_link");
}



});

