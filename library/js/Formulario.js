//CONSTANTES
const expresiones = 
{
nombre: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
apellidos: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
telefono: /^\d{7,14}$/, // 7 a 14 numeros.
password: /^.{4,12}$/ // 4 a 12 digitos.
}
//CONSTANTES


//FORMULARIO 1
const formulario1 = document.getElementById('formulario1');
const inputs1 = document.querySelectorAll('#formulario1 input');
//FORMULARIO 1

//FORMULARIO 2
const formulario2 = document.getElementById('formulario2');
const inputs = document.querySelectorAll('#formulario2 input');

const campos = 
{
nombre: false,
apellidos: false,
correo: false,
telefono: false,
password: false
}

const validate2 = (e) =>{

switch (e.target.name)
{
case "Nombre":
if(expresiones.nombre.test(e.target.value))
{
document.getElementById('Nombre').classList.remove('is-invalid');
document.getElementById('Nombre').classList.add('is-valid');
campos['nombre'] = true;
} else
{
document.getElementById('Nombre').classList.add('is-invalid');
campos['nombre'] = false;
}
break;

case "Apellidos":
if(expresiones.apellidos.test(e.target.value))
{
document.getElementById('Apellidos').classList.remove('is-invalid');
document.getElementById('Apellidos').classList.add('is-valid');
campos['apellidos'] = true;
} else
{
document.getElementById('Apellidos').classList.add('is-invalid');
campos['apellidos'] = false;
}
break;

case "Correo":
if(expresiones.correo.test(e.target.value))
{
document.getElementById('Correo').classList.remove('is-invalid');
document.getElementById('Correo').classList.add('is-valid');
campos['correo'] = true;
} else
{
document.getElementById('Correo').classList.add('is-invalid');
campos['correo'] = false;
}
break;

case "Telefono":
if(expresiones.telefono.test(e.target.value))
{
document.getElementById('Telefono').classList.remove('is-invalid');
document.getElementById('Telefono').classList.add('is-valid');
campos['telefono'] = true;
} else
{
document.getElementById('Telefono').classList.add('is-invalid');
campos['telefono'] = false;
}
break;

case "Password":
if(expresiones.password.test(e.target.value))
{
document.getElementById('Password').classList.remove('is-invalid');
document.getElementById('Password').classList.add('is-valid');
} else
{
document.getElementById('Password').classList.add('is-invalid');
}
validarPassword2();
break;

case "Password2":
validarPassword2();
break;
}

}

const validarPassword2 = () =>{
const Password = document.getElementById('Password');
const Password2 = document.getElementById('Password2');
if(Password.value != Password2.value)
{
document.getElementById('Password2').classList.add('is-invalid');
campos['password'] = false;
}
else
{
document.getElementById('Password2').classList.remove('is-invalid');
document.getElementById('Password2').classList.add('is-valid');
campos['password'] = true;
}
}

inputs.forEach( (input) => 
{
input.addEventListener('keyup',validate2);
input.addEventListener('blur',validate2);
});

formulario2.addEventListener('submit',(e) =>{
e.preventDefault();

if(campos.nombre && campos.apellidos && campos.correo && campos.telefono && campos.password)
{
formulario2.submit();
}
});
//FORMULARIO 2