
//CONSTANTES
const expresiones = 
{
nombre: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
apellidos: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
telefono: /^\d{7,14}$/, // 7 a 14 numeros.
edad: /^\d{1,3}$/, // 7 a 14 numeros.
password: /^.{4,12}$/ // 4 a 12 digitos.
}
//CONSTANTES

const campos = 
{
nombre: false,
edad: false,
correo: false,
telefono: false
}

const main = document.querySelector('#main');
const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');


const validarFormulario = (e)=>{
switch(e.target.name)
{
case  "nombre":
if(expresiones.nombre.test(e.target.value))
{
document.getElementById('nombre').classList.remove('is-invalid');
document.getElementById('nombre').classList.add('is-valid');
campos['nombre'] = true;
}
else
{
document.getElementById('nombre').classList.add('is-invalid');
campos['nombre'] = false;
}
break;

case  "edad":
if(expresiones.edad.test(e.target.value))
{
document.getElementById('edad').classList.remove('is-invalid');
document.getElementById('edad').classList.add('is-valid');
campos['edad'] = true;
}
else
{
document.getElementById('edad').classList.add('is-invalid');
campos['edad'] = false;
}

break;


case  "correo":
if(expresiones.correo.test(e.target.value))
{
document.getElementById('correo').classList.remove('is-invalid');
document.getElementById('correo').classList.add('is-valid');
campos['correo'] = true;
}

else
{
document.getElementById('correo').classList.add('is-invalid');
campos['correo'] = false;
}

break;

case  "telefono":
if(expresiones.telefono.test(e.target.value))
{
document.getElementById('telefono').classList.remove('is-invalid');
document.getElementById('telefono').classList.add('is-valid');
campos['telefono'] = true;
}
else
{
document.getElementById('telefono').classList.add('is-invalid');
campos['telefono'] = false;
}

break;


}

}


inputs.forEach((input)=>{
input.addEventListener('keyup', validarFormulario);
input.addEventListener('blur', validarFormulario);
});


main.addEventListener('click', (e) =>{

document.getElementById("nombre_2").value = document.getElementById("nombre").value;
document.getElementById("edad_2").value = document.getElementById("edad").value;
document.getElementById("correo_2").value = document.getElementById("correo").value;
document.getElementById("telefono_2").value = document.getElementById("telefono").value;
document.getElementById("fecha_2").value = document.getElementById("fecha").value;
document.getElementById("horario_2").value = document.getElementById("horario").value;
document.getElementById("motivos_2").value = document.getElementById("motivos").value;
});


function mayus(e) 
{
e.value = e.value.toUpperCase();
}

formulario.addEventListener('keyup' ,(event) =>{

if(campos.nombre && campos.edad && campos.correo && campos.telefono )
{
$("#main").removeClass("disabled");
}
else {
$("#main").addClass("disabled");
}



});

