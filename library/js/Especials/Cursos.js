/*//CONSTANTES
const expresiones = 
{
nombre: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
telefono: /^\d{7,14}$/
}
//CONSTANTES

const user_name = document.getElementById('name_user_1').innerHTML;
const main = document.querySelector('#main');

const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');

if(user_name=='USER CENTI-R')
{
document.querySelector("#nombre").setAttribute("disabled", "");
document.querySelector("#correo").setAttribute("disabled", "");
document.querySelector("#telefono").setAttribute("disabled", "");
}
else 
{
document.querySelector("#nombre").removeAttribute("disabled");
document.querySelector("#correo").removeAttribute("disabled");
document.querySelector("#telefono").removeAttribute("disabled");
}

main.addEventListener('click', (e) =>{
if(user_name=='USER CENTI-R')
{
$(window).attr('location','../Login')
}
else 
{
document.getElementById("nombre_2").value = document.getElementById("nombre").value;
document.getElementById("correo_2").value = document.getElementById("correo").value;
document.getElementById("telefono_2").value = document.getElementById("telefono").value;
$('#modal').modal('show'); // abrir
}

});

function mayus(e) 
{
e.value = e.value.toUpperCase();
}

const validarFormulario = (e)=>{
switch(e.target.name)
{
case  "nombre":
if(expresiones.nombre.test(e.target.value))
{
document.getElementById('nombre').classList.remove('is-invalid');
document.getElementById('nombre').classList.add('is-valid');
}
else
{
document.getElementById('nombre').classList.add('is-invalid');
}
break;


case  "correo":
if(expresiones.correo.test(e.target.value))
{
document.getElementById('correo').classList.remove('is-invalid');
document.getElementById('correo').classList.add('is-valid');
}

else
{
document.getElementById('correo').classList.add('is-invalid');
}

break;

case  "telefono":
if(expresiones.telefono.test(e.target.value))
{
document.getElementById('telefono').classList.remove('is-invalid');
document.getElementById('telefono').classList.add('is-valid');
}
else
{
document.getElementById('telefono').classList.add('is-invalid');
}

break;

}

}


inputs.forEach((input)=>{
input.addEventListener('keyup', validarFormulario);
input.addEventListener('blur', validarFormulario);
});
*/
//CONSTANTES
const expresiones = 
{
nombre: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
telefono: /^\d{7,14}$/
}
//CONSTANTES

const campos = 
{
nombre: false,
correo: false,
telefono: false
}

const user_name = document.getElementById('name_user_1').innerHTML;
const main = document.querySelector('#main');

const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');

$("#main").addClass("disabled");

main.addEventListener('click', (e) =>{

document.getElementById("nombre_2").value = document.getElementById("nombre").value;
document.getElementById("correo_2").value = document.getElementById("correo").value;
document.getElementById("telefono_2").value = document.getElementById("telefono").value;
document.getElementById("constancia").value = $('input[name=radio2]:checked').val();
$('#modal').modal('show'); // abrir


});

function mayus(e) 
{
e.value = e.value.toUpperCase();
}

const validarFormulario = (e)=>{
switch(e.target.name)
{
case  "nombre":
if(expresiones.nombre.test(e.target.value))
{
document.getElementById('nombre').classList.remove('is-invalid');
document.getElementById('nombre').classList.add('is-valid');
campos['nombre'] = true;
}
else
{
document.getElementById('nombre').classList.add('is-invalid');
campos['nombre'] = false;
}
break;


case  "correo":
if(expresiones.correo.test(e.target.value))
{
document.getElementById('correo').classList.remove('is-invalid');
document.getElementById('correo').classList.add('is-valid');
campos['correo'] = true;
}

else
{
document.getElementById('correo').classList.add('is-invalid');
campos['correo'] = false;
}

break;

case  "telefono":
if(expresiones.telefono.test(e.target.value))
{
document.getElementById('telefono').classList.remove('is-invalid');
document.getElementById('telefono').classList.add('is-valid');
campos['telefono'] = true;
}
else
{
document.getElementById('telefono').classList.add('is-invalid');
campos['telefono'] = false;
}

break;

}

}


inputs.forEach((input)=>{
input.addEventListener('keyup', validarFormulario);
input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('keyup' ,(event) =>{

if(campos.nombre && campos.correo && campos.telefono )
{
$("#main").removeClass("disabled");
}
else {
$("#main").addClass("disabled");
}



});