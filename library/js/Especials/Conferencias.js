//CONSTANTES
const expresiones = 
{
nombre: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
telefono: /^\d{7,14}$/
}
//CONSTANTES
const main = document.querySelector('#main');
const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');
const radio_1 = document.querySelector('#ex-radio-4');
const radio_2 = document.querySelector('#ex-radio-5');

const campos = 
{
nombre: false,
correo: false,
telefono: false
}

radio_1.addEventListener('change', (event) =>{
 $('#titulo').removeClass("d-none");
 $('#precio').removeClass("invisible");
});

radio_2.addEventListener('change', (event) =>{
$('#titulo').addClass("d-none");
$('#precio').addClass("invisible");

});

const validarFormulario = (e)=>{
switch(e.target.name)
{
case  "nombre":
if(expresiones.nombre.test(e.target.value))
{
document.getElementById('nombre').classList.remove('is-invalid');
document.getElementById('nombre').classList.add('is-valid');
campos['nombre'] = true;
}
else
{
document.getElementById('nombre').classList.add('is-invalid');
campos['nombre'] = false;
}
break;


case  "correo":
if(expresiones.correo.test(e.target.value))
{
document.getElementById('correo').classList.remove('is-invalid');
document.getElementById('correo').classList.add('is-valid');
campos['correo'] = true;
}

else
{
document.getElementById('correo').classList.add('is-invalid');
campos['correo'] = false;
}

break;

case  "telefono":
if(expresiones.telefono.test(e.target.value))
{
document.getElementById('telefono').classList.remove('is-invalid');
document.getElementById('telefono').classList.add('is-valid');
campos['telefono'] = true;
}
else
{
document.getElementById('telefono').classList.add('is-invalid');
campos['telefono'] = false;
}

break;

}

}


inputs.forEach((input)=>{
input.addEventListener('keyup', validarFormulario);
input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('keyup' ,(event) =>{

if(campos.nombre && campos.correo && campos.telefono )
{
$("#main").removeClass("disabled");
}
else {
$("#main").addClass("disabled");
}



});


main.addEventListener('click', (e) =>{

document.getElementById("nombre_2").value = document.getElementById("nombre").value;
document.getElementById("correo_2").value = document.getElementById("correo").value;
document.getElementById("telefono_2").value = document.getElementById("telefono").value;
document.getElementById("constancia").value = $('input[name=radio2]:checked').val();
document.getElementById("titulo_name2").value = $('select[name="titulo_name"] option:selected').text();
});

function mayus(e) 
{
e.value = e.value.toUpperCase();
}