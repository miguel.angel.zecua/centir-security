
//CONSTANTES
const expresiones = 
{
nombre: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
apellidos: /^[a-zA-ZÀ-ÿ\s]{1,100}$/, // Letras y espacios, pueden llevar acentos.
correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
telefono: /^\d{7,14}$/, // 7 a 14 numeros.
edad: /^\d{1,3}$/, // 7 a 14 numeros.
password: /^.{4,12}$/ // 4 a 12 digitos.
}
//CONSTANTES

const campos = 
{
nombre: false,
edad: false,
correo: false,
telefono: false,
modalidad: false
}

const main = document.querySelector('#main');
const enviar = document.querySelector('#enviar');
const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');
var formulario_principal = document.getElementById("form");
const modalidad = document.querySelector('#modalidad');
const horario = document.querySelector('#horario');


enviar.addEventListener('click', (e) =>{
e.preventDefault();
window.location.reload(true);
formulario_principal.submit();
});


modalidad.addEventListener('change',(event)=>{



var operacion = $('#modalidad').val();

if(operacion=='Nothing')
{
document.getElementById('modalidad').classList.remove('is-invalid');
document.getElementById('modalidad').classList.add('is-invalid');
campos['modalidad'] = false;
}
else
{
document.getElementById('modalidad').classList.remove('is-invalid');
document.getElementById('modalidad').classList.add('is-valid');
campos['modalidad'] = true;
}


if(operacion=='Linea')
{
$('#horario').append(new Option('04:00 PM-05:00 PM', '04:00 PM'));
$('#horario').append(new Option('05:00 PM-06:00 PM', '05:00 PM'));
$('#horario').append(new Option('06:00 PM-07:00 PM', '06:00 PM'));
$('#horario').append(new Option('07:00 PM-08:00 PM', '07:00 PM'));
$('#horario').append(new Option('08:00 PM-09:00 PM', '08:00 PM'));

} 
else if(operacion=='Presencial')
{
$("option[value='04:00 PM']").remove(); 
$("option[value='05:00 PM']").remove(); 
$("option[value='06:00 PM']").remove(); 
$("option[value='07:00 PM']").remove(); 
$("option[value='08:00 PM']").remove(); 

}

});

const validarFormulario = (e)=>{
switch(e.target.name)
{
case  "nombre":
if(expresiones.nombre.test(e.target.value))
{
document.getElementById('nombre').classList.remove('is-invalid');
document.getElementById('nombre').classList.add('is-valid');
campos['nombre'] = true;
}
else
{
document.getElementById('nombre').classList.add('is-invalid');
campos['nombre'] = false;
}
break;

case  "edad":
if(expresiones.edad.test(e.target.value))
{
document.getElementById('edad').classList.remove('is-invalid');
document.getElementById('edad').classList.add('is-valid');
campos['edad'] = true;
}
else
{
document.getElementById('edad').classList.add('is-invalid');
campos['edad'] = false;
}

break;


case  "correo":
if(expresiones.correo.test(e.target.value))
{
document.getElementById('correo').classList.remove('is-invalid');
document.getElementById('correo').classList.add('is-valid');
campos['correo'] = true;
}

else
{
document.getElementById('correo').classList.add('is-invalid');
campos['correo'] = false;
}

break;

case  "telefono":
if(expresiones.telefono.test(e.target.value))
{
document.getElementById('telefono').classList.remove('is-invalid');
document.getElementById('telefono').classList.add('is-valid');
campos['telefono'] = true;
}
else
{
document.getElementById('telefono').classList.add('is-invalid');
campos['telefono'] = false;
}

break;


}

}



inputs.forEach((input)=>{
input.addEventListener('keyup', validarFormulario);
input.addEventListener('blur', validarFormulario);
});


main.addEventListener('click', (e) =>{

document.getElementById("nombre_2").value = document.getElementById("nombre").value;
document.getElementById("edad_2").value = document.getElementById("edad").value;
document.getElementById("modalidad_2").value = document.getElementById("modalidad").value;
document.getElementById("correo_2").value = document.getElementById("correo").value;
document.getElementById("telefono_2").value = document.getElementById("telefono").value;
document.getElementById("sexo_2").value = document.getElementById("sexo").value;
document.getElementById("fecha_2").value = document.getElementById("fecha").value;
document.getElementById("horario_2").value = document.getElementById("horario").value;

});


function mayus(e) 
{
e.value = e.value.toUpperCase();
}

formulario.addEventListener('keyup' ,(event) =>{

if(campos.nombre && campos.edad && campos.correo && campos.telefono )
{
$("#main").removeClass("disabled");
}
else {
$("#main").addClass("disabled");
}



});

